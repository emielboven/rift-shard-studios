﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillTrigger : MonoBehaviour {

	void OnTriggerEnter (Collider other){
		if (other.GetComponent<PlayerController>() != null) {
			other.GetComponent<PlayerController>().OnDeath();
		}

	}
}
