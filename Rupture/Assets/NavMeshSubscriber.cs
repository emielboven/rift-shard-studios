﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshSubscriber : MonoBehaviour
{
    void Start()
    {
        NavManager.instance.Subscribe(this.GetComponent<NavMeshSurface>());
    }
    void OnDestroy()
    {
        NavManager.instance.Unsubscribe(this.GetComponent<NavMeshSurface>());
    }
}
