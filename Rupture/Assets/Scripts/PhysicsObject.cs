﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour
{
    private float slomoTime;

	public bool isSlowed;

	private Rigidbody rb;
    // Use this for initialization
    void Start()
    {
		rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isSlowed)
        {
            OnSlowTime();
        }
        else if (slomoTime != 1)
        {
            OnSlowTimeEnd();
        }

    }

    void OnSlowTime()
    {
        slomoTime = 0.3f;
        rb.velocity = rb.velocity * slomoTime;
        rb.angularVelocity = rb.angularVelocity * slomoTime;
		
    }

    void OnSlowTimeEnd()
    {
        slomoTime = 1f;
    }
}
