﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsUpdater : MonoBehaviour {

	public Text health;
	public Text stamina;
	public Text damage;
	public Text damageReduction;
	private PlayerController playerController;
	private PlayerStats stats;

	// Use this for initialization
	void Start () {
		playerController = GameObject.Find("Player").GetComponent<PlayerController>();
		stats = GameObject.Find("Player").GetComponent<PlayerStats>();
	}
	
	// Update is called once per frame
	void Update () {
		health.text = "Health " +stats.health.ToString("F0") + "/" + stats.maxHealth;
		//damage.text = "Damage " +playerController.weapon.weaponDamage + " + " + ((stats.bonusDamage)/100) * playerController.weapon.weaponDamage + " (" + stats.bonusDamage + "%)";
		damageReduction.text = "Dmg. Red. " + stats.dmgReduction + "%";
	}
}
