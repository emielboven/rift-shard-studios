﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class InventoryManager : MonoBehaviour
{

    public List<Item> inventory;
    public Text itemName;
    public Text itemType;
    public Text itemStats;
    public Text itemDescription;
    public Text set;
    public GameObject[] inventoryGrid;
    public Image[] inventoryGridIcons;
    public GameObject[] inventoryGridCounters;
    public ItemDatabase itemDB;
    public EventSystem e;
    private GameObject prevSelect;
    private Item selectedItem;
    private Item maskItem;
    private Item weaponItem;
    private Item shieldItem;
    private Item secondWeaponItem;
    private Item secondShieldItem;
    public Item throwableItem;
    public Item consumableItem;
    private Item amuletItem;

    public bool secondSet;
    public GameObject Set1;
    public GameObject Set2;

    public Image maskIcon;
    public Image weaponIcon;
    public Image shieldIcon;
    public Image secondWeaponIcon;
    public Image secondShieldIcon;

    public Image amuletIcon;
    public Image throwableIcon;
    public Image consumableIcon;

    public Image throwableIconHUD;
    public Image consumableIconHUD;

    public Text throwableText;
    public Text consumableText;
    public Text throwableTextHUD;
    public Text consumableTextHUD;

    private int currentInventoryIndex;
    // Use this for initialization
    void Start()
    {   

        throwableItem = null;
        consumableItem = null;
        Set2.SetActive(false);
        set.text = "Set I ";
        secondSet = false;
        AddItem(GetItemByName("Warhammer"));
        AddItem(GetItemByName("Shatter Dust Bomb"));
        AddItem(GetItemByName("Shatter Dust Bomb"));

        AddItem(GetItemByName("Health Potion"));
        AddItem(GetItemByName("Health Potion"));
        AddItem(GetItemByName("Health Potion"));
        AddItem(GetItemByName("Health Potion"));
        AddItem(GetItemByName("Iron Longsword"));
        AddItem(GetItemByName("Iron Longsword"));
        AddItem(GetItemByName("Iron Longsword"));
        AddItem(GetItemByName("Iron Longsword"));
        AddItem(GetItemByName("Mask of Nimble Feet"));
        AddItem(GetItemByName("Amulet of Azuhl"));
        AddItem(GetItemByName("Templar Shield"));
        AddItem(GetItemByName("Shatter Dust Bomb"));
        // AddItem(200);
        AddItem(GetItemByName("Health Potion"));


        inventoryGridIcons = new Image[inventoryGrid.Length];
        inventoryGridCounters = new GameObject[inventoryGrid.Length];
        for (int i = 0; i < inventoryGrid.Length; i++)
        {
            inventoryGridIcons[i] = inventoryGrid[i].transform.GetChild(0).GetComponent<Image>();
            inventoryGridCounters[i] = inventoryGrid[i].transform.GetChild(1).gameObject;
        }

        UpdateGrid();
    }

    // Update is called once per frame
    void Update()
    {
        if (prevSelect != e.currentSelectedGameObject)
        {
            UpdateSelected();
            prevSelect = e.currentSelectedGameObject;
        }
        if (Input.GetButtonDown("Interact") && selectedItem != null)
        {
            UseItem(selectedItem);
        }

        if (Input.GetButtonDown("Swap"))
        {
            secondSet = !secondSet;
            if (secondSet)
            {
                Set2.SetActive(true);
                Set1.SetActive(false);
                set.text = "Set II";
            }
            if (!secondSet)
            {
                Set2.SetActive(false);
                Set1.SetActive(true);
                set.text = "Set I ";
            }
        }


    }

    void UpdateSelected()
    {
        for (int i = 0; i < inventoryGrid.Length; i++)
        {
            if (inventoryGrid[i] == e.currentSelectedGameObject)
            {
                if (i < inventory.Count)
                {
                    selectedItem = inventory[i];
                    itemName.text = selectedItem.itemName;
                    itemType.text = "" + selectedItem.itemType;
                    itemStats.text = GetItemPower(selectedItem);
                    itemDescription.text = selectedItem.itemDesc;


                }
                else
                {
                    selectedItem = null;
                    itemName.text = "";
                    itemType.text = "";
                    itemStats.text = "";
                    itemDescription.text = "";
                }
                currentInventoryIndex = i;

            }
        }
    }
    public void UpdateThrowableAmount(int amount)
    {

        throwableItem.itemAmount -= amount;

        if (throwableItem.itemAmount <= 0)
        {
            throwableItem = null;
            throwableIcon.sprite = null;
            throwableIcon.color = Color.clear;
            throwableIconHUD.sprite = null;
            throwableIconHUD.color = Color.clear;
            throwableText.text = "-";
            throwableTextHUD.text = "-";
        }
        else
        {
            UpdateThrowableHUD();
        }


    }
    public void UpdateConsumableAmount(int amount)
    {

        consumableItem.itemAmount -= amount;

        if (consumableItem.itemAmount <= 0)
        {
            consumableItem = null;
            consumableIcon.sprite = null;
            consumableIcon.color = Color.clear;
            consumableIconHUD.sprite = null;
            consumableIconHUD.color = Color.clear;
            consumableText.text = "-";
            consumableTextHUD.text = "-";
        }
        else
        {
            UpdateConsumableHUD();
        }


    }

    void UpdateThrowableHUD()
    {

        throwableText.text = "" + throwableItem.itemAmount;
        throwableTextHUD.text = "" + throwableItem.itemAmount;
    }

    void UpdateConsumableHUD()
    {

        consumableText.text = "" + consumableItem.itemAmount;
        consumableTextHUD.text = "" + consumableItem.itemAmount;
    }

    void UpdateGrid()
    {
        for (int i = 0; i < inventory.Count; i++)
        {
            inventoryGridIcons[i].sprite = inventory[i].itemIcon;
            inventoryGridIcons[i].color = Color.white;
            if (inventory[i].itemAmount > 1)
            {
                inventoryGridCounters[i].SetActive(true);
                inventoryGridCounters[i].transform.GetChild(0).GetComponent<Text>().text = "" + inventory[i].itemAmount;
            }
            else
            {
                inventoryGridCounters[i].SetActive(false);
            }
        }
        for (int i = inventory.Count; i < inventoryGrid.Length; i++)
        {
            inventoryGridIcons[i].sprite = null;
            inventoryGridIcons[i].color = Color.clear;
            inventoryGridCounters[i].SetActive(false);


        }
    }

    void UseItem(Item item)
    {

        if (item.itemType == Item.ItemType.Weapon)
        {
            if (!secondSet)
            {
                if (weaponItem == null)
                {
                    weaponItem = item;
                    weaponIcon.sprite = weaponItem.itemIcon;
                    weaponIcon.color = Color.white;
                    DestroyItem(currentInventoryIndex);
                    UpdateGrid();
                    UpdateSelected();

                }
                else
                {
                    inventory[currentInventoryIndex] = weaponItem;
                    weaponItem = item;
                    weaponIcon.sprite = weaponItem.itemIcon;
                    weaponIcon.color = Color.white;
                    UpdateGrid();
                    UpdateSelected();
                }
            }
            else
            {

                if (secondWeaponItem == null)
                {
                    secondWeaponItem = item;
                    secondWeaponIcon.sprite = secondWeaponItem.itemIcon;
                    secondWeaponIcon.color = Color.white;
                    DestroyItem(currentInventoryIndex);
                    UpdateGrid();
                    UpdateSelected();

                }
                else
                {
                    inventory[currentInventoryIndex] = secondWeaponItem;
                    secondWeaponItem = item;
                    secondWeaponIcon.sprite = secondWeaponItem.itemIcon;
                    secondWeaponIcon.color = Color.white;
                    UpdateGrid();
                    UpdateSelected();
                }
            }

        }
        if (item.itemType == Item.ItemType.Shield)
        {
            if (!secondSet)
            {
                if (shieldItem == null)
                {
                    shieldItem = item;
                    shieldIcon.sprite = shieldItem.itemIcon;
                    shieldIcon.color = Color.white;
                    DestroyItem(currentInventoryIndex);
                    UpdateGrid();
                    UpdateSelected();
                }
                else
                {
                    inventory[currentInventoryIndex] = shieldItem;
                    shieldItem = item;
                    shieldIcon.sprite = shieldItem.itemIcon;
                    shieldIcon.color = Color.white;
                    UpdateGrid();
                    UpdateSelected();
                }
            }
            else
            {
                if (secondShieldItem == null)
                {
                    secondShieldItem = item;
                    secondShieldIcon.sprite = secondShieldItem.itemIcon;
                    secondShieldIcon.color = Color.white;
                    DestroyItem(currentInventoryIndex);
                    UpdateGrid();
                    UpdateSelected();
                }
                else
                {
                    inventory[currentInventoryIndex] = secondShieldItem;
                    secondShieldItem = item;
                    secondShieldIcon.sprite = secondShieldItem.itemIcon;
                    secondShieldIcon.color = Color.white;
                    UpdateGrid();
                    UpdateSelected();
                }
            }

        }
        if (item.itemType == Item.ItemType.Mask)
        {
            if (maskItem == null)
            {
                maskItem = item;
                maskIcon.sprite = maskItem.itemIcon;
                maskIcon.color = Color.white;
                DestroyItem(currentInventoryIndex);
                UpdateGrid();
                UpdateSelected();

            }
            else
            {
                inventory[currentInventoryIndex] = maskItem;
                maskItem = item;
                maskIcon.sprite = maskItem.itemIcon;
                maskIcon.color = Color.white;
                UpdateGrid();
                UpdateSelected();
            }

        }
        if (item.itemType == Item.ItemType.Throwable)
        {
            if (throwableItem == null)
            {
                throwableItem = item;
                throwableIcon.sprite = throwableItem.itemIcon;
                throwableIcon.color = Color.white;
                throwableIconHUD.sprite = throwableItem.itemIcon;
                throwableIconHUD.color = Color.white;

                throwableText.text = "" + throwableItem.itemAmount;
                throwableTextHUD.text = "" + throwableItem.itemAmount;

                DestroyItem(currentInventoryIndex);
                UpdateGrid();
                UpdateSelected();

            }
            else
            {
                inventory[currentInventoryIndex] = throwableItem;
                throwableItem = item;
                throwableIcon.sprite = throwableItem.itemIcon;
                throwableIcon.color = Color.white;
                throwableIconHUD.sprite = throwableItem.itemIcon;
                throwableIconHUD.color = Color.white;

                throwableText.text = "" + throwableItem.itemAmount;
                throwableTextHUD.text = "" + throwableItem.itemAmount;
                UpdateGrid();
                
                UpdateSelected();
            }

        }
        if (item.itemType == Item.ItemType.Consumable)
        {
            if (consumableItem == null)
            {
                consumableItem = item;
                consumableIcon.sprite = consumableItem.itemIcon;
                consumableIcon.color = Color.white;
                consumableIconHUD.sprite = consumableItem.itemIcon;
                consumableIconHUD.color = Color.white;

                consumableText.text = "" + consumableItem.itemAmount;
                consumableTextHUD.text = "" + consumableItem.itemAmount;
                DestroyItem(currentInventoryIndex);
                UpdateGrid();
                UpdateSelected();





            }
            else
            {
                inventory[currentInventoryIndex] = consumableItem;
                consumableItem = item;
                consumableIcon.sprite = consumableItem.itemIcon;
                consumableIcon.color = Color.white;
                consumableIconHUD.sprite = consumableItem.itemIcon;
                consumableIconHUD.color = Color.white;

                consumableText.text = "" + consumableItem.itemAmount;
                consumableTextHUD.text = "" + consumableItem.itemAmount;
                UpdateGrid();
                UpdateSelected();
            }
        }
        if (item.itemType == Item.ItemType.Amulet)
        {
            if (amuletItem == null)
            {
                amuletItem = item;
                amuletIcon.sprite = amuletItem.itemIcon;
                amuletIcon.color = Color.white;
                DestroyItem(currentInventoryIndex);
                UpdateGrid();
                UpdateSelected();

            }
            else
            {
                inventory[currentInventoryIndex] = amuletItem;
                amuletItem = item;
                amuletIcon.sprite = amuletItem.itemIcon;
                amuletIcon.color = Color.white;
                UpdateGrid();
                UpdateSelected();
            }
        }


    }
    void AddItem(int ID)
    {
        if (ID == -1)
        {

        }

        for (int i = 0; i < itemDB.items.Count; i++)
        {
            if (itemDB.items[i].itemID == ID)
            {
                if (IsItemAlreadyInInventory(ID) && (itemDB.items[i].itemType == Item.ItemType.Consumable || itemDB.items[i].itemType == Item.ItemType.Throwable))
                {
                    inventory[GetItemInInventory(ID)].itemAmount++;

                }
                else
                {
                    inventory.Add(itemDB.items[i]);

                }

            }
        }
    }





    int GetItemByName(string nameString)
    {

        for (int i = 0; i < itemDB.items.Count; i++)
        {
            if (itemDB.items[i].itemName == nameString)
            {
                return itemDB.items[i].itemID;
            }
        }

        return -1;
    }

    bool IsItemAlreadyInInventory(int ID)
    {
        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].itemID == ID)
            {
                return true;
            }
        }
        return false;
    }

    int GetItemInInventory(int ID)
    {
        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].itemID == ID)
            {
                return i;
            }
        }

        return -1;
    }


    int GetItemByID(int ID)
    {
        for (int i = 0; i < itemDB.items.Count; i++)
        {
            if (itemDB.items[i].itemID == ID)
            {
                return i;
            }
        }

        return -1;
    }

    public string GetItemPower(Item item)
    {

        if (item.itemType == Item.ItemType.Weapon)
        {
            return item.itemPower + " weapon damage";
        }
        else if (item.itemType == Item.ItemType.Shield)
        {
            return item.itemPower + "% damage resist when blocking \n" + item.itemPower2 + "% reduced stamina cost when blocking";
        }
        else if (item.itemType == Item.ItemType.Consumable)
        {
            return "+" + item.itemPower + " health when consumed";
        }
        else
        {
            return "" + item.itemPower + " Power";
        }
    }

    void DestroyItem(int inventoryIndex)
    {
        inventory.RemoveAt(inventoryIndex);
        UpdateGrid();
    }

    public void SetFirstSelectable(){
        e.SetSelectedGameObject(inventoryGrid[0]);
    }
}
