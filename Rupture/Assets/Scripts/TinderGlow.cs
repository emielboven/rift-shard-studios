﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TinderGlow : MonoBehaviour {

    private Renderer rend;
    private Material mat;

	void Start () {
        rend = GetComponent<Renderer>();
        rend.material.shader = Shader.Find("SH_Tinder");
	}
	
	void Update () {
        float emissionStrAdd = Mathf.PingPong(Time.time * 2f, 2f);
        float emissionStr = 3f + emissionStrAdd;
        rend.material.SetFloat("_emissionStrength", emissionStr); 
    }
}
