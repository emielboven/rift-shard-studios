﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowAnimator : MonoBehaviour {

public bool  isSlowed;

private Animator anim;

	void Start () {
		anim = GetComponent<Animator>();
	}
	
	void Update () {
		if (isSlowed && anim.GetFloat("TimeScale") != 0.3f){
			anim.SetFloat("TimeScale",0.3f);
		} 
		if (!isSlowed && anim.GetFloat("TimeScale") != 1f) {
			anim.SetFloat("TimeScale",1f);
		}
	}
}
