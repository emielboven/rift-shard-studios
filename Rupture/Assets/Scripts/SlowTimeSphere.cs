﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowTimeSphere : MonoBehaviour
{
    public GameObject explosionPrefab;
    public GameObject scorchMark;
    public GameObject chargedCloudFX;
    public bool deleting = false;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(SlowTimer());
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay(Collider other)
    {
        if (!deleting)
        {
            if (other.GetComponentInParent<PlayerController>())
            {
                other.GetComponentInParent<PlayerController>().isSlowed = true;
            }

            if (other.GetComponentInParent<PhysicsObject>())
            {
                other.GetComponentInParent<PhysicsObject>().isSlowed = true;
            }

            if (other.GetComponent<DoorStats>())
            {
                other.GetComponent<DoorStats>().isSlowed = true;
            }
            if (other.GetComponentInParent<EnemyAI>())
            {
                other.GetComponentInParent<EnemyAI>().isSlowed = true;
            }

        }


        if (other.tag == "Fire")
        {
            deleting = true;
            StartCoroutine(FireDelete());
            Destroy(other.gameObject);

        }

        if (deleting)
        {
            if (other.GetComponentInParent<PlayerController>())
            {
                other.GetComponentInParent<PlayerController>().isSlowed = false;
            }
            if (other.GetComponentInParent<PhysicsObject>())
            {
                other.GetComponentInParent<PhysicsObject>().isSlowed = false;
            }
            if (other.GetComponent<DoorStats>())
            {
                other.GetComponent<DoorStats>().isSlowed = false;
            }
            if (other.GetComponentInParent<EnemyAI>())
            {
                other.GetComponentInParent<EnemyAI>().isSlowed = false;
            }
        }
        else
        {

            if (other.tag == "Electric")
            {
                deleting = true;
                StartCoroutine(ElectricDelete());

            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.GetComponentInParent<PlayerController>())
        {
            other.GetComponentInParent<PlayerController>().isSlowed = false;
            if (other.GetComponentInParent<PlayerController>().isDashing)
            {
                //deleting = true;
                //StartCoroutine(ElectricDelete());
            }
        }
        if (other.GetComponentInParent<PhysicsObject>())
        {
            other.GetComponentInParent<PhysicsObject>().isSlowed = false;
        }
        if (other.GetComponentInParent<SlowAnimator>())
        {
            other.GetComponentInParent<SlowAnimator>().isSlowed = false;
        }
        if (other.GetComponentInParent<EnemyAI>())
        {
            other.GetComponentInParent<EnemyAI>().isSlowed = false;
        }
    }

    IEnumerator SlowTimer()
    {

        yield return new WaitForSeconds(20f);

        deleting = true;
        yield return new WaitForFixedUpdate();

        Destroy(gameObject.transform.parent.gameObject);
    }

    IEnumerator FireDelete()
    {
        yield return new WaitForSeconds(0.1f);
        explosionPrefab.SetActive(true);
        gameObject.transform.parent.gameObject.SetActive(false);
        scorchMark.SetActive(true);
        scorchMark.transform.SetParent(null);

    }
    IEnumerator ElectricDelete()
    {
        yield return new WaitForSeconds(0.1f);
        chargedCloudFX.SetActive(true);
        gameObject.transform.parent.gameObject.SetActive(false);

    }

}
