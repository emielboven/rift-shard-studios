﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryScreen : MonoBehaviour {

	private CanvasGroup canvasGroup;
	private InventoryManager inventory;
	// Use this for initialization
	void Start () {
		canvasGroup = GetComponent<CanvasGroup>();
		canvasGroup.interactable = false;
		canvasGroup.blocksRaycasts = false;
		canvasGroup.alpha = 0;
		inventory = GameObject.Find("Inventory").GetComponent<InventoryManager>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Inventory")){
			if(canvasGroup.alpha == 0){
			canvasGroup.interactable = true;
			canvasGroup.blocksRaycasts = true;
			canvasGroup.alpha = 1;
			PersistentManager.Instance.playerControlled = false;
			inventory.SetFirstSelectable();
			} else {
			canvasGroup.interactable = false;
			canvasGroup.blocksRaycasts = false;
			canvasGroup.alpha = 0;
			PersistentManager.Instance.playerControlled = true;
			}
		}

		if (Input.GetButtonDown("Cancel")){
			if(canvasGroup.alpha == 1){
			canvasGroup.interactable = false;
			canvasGroup.blocksRaycasts = false;
			canvasGroup.alpha = 0;
			StartCoroutine(DelayPlayerControl());
			}
		}
		
	}
	 IEnumerator DelayPlayerControl()
    {
        yield return new WaitForEndOfFrame();
        PersistentManager.Instance.playerControlled = true;
    }
	
}
