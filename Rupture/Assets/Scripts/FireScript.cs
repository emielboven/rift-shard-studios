﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//This script adds damage to this object and destroys the whole object after burnTime
public class FireScript : MonoBehaviour
{
    public float damage = 5;
    //public NavMeshSurface navmesh;
    public bool destroy = true;
    public float burnTime = 20f;

    void Start()
    {
        gameObject.tag = "Fire";
       NavManager.instance.Rebuild();
        if (destroy)
        {
            Destroy(this.gameObject, burnTime);
        }

    }

    void OnTriggerStay(Collider other)
    {

        if (other.name == "Player")
        {
            if (other.GetComponent<PlayerController>() != null && !other.GetComponent<PlayerController>().isDashing && !other.GetComponent<PlayerController>().fireTrail && other.GetComponent<PlayerController>().CanGetHitByFire)
            {
                other.GetComponent<PlayerController>().OnFireHit(damage);
            }

            if (other.GetComponent<PlayerController>() != null && other.GetComponent<PlayerController>().isDashing)
            {
                other.GetComponent<PlayerController>().fireTrail = true;
            }
        }
        if (other.tag == "Enemy")
        {
            if (other.GetComponent<EnemyAI>() != null && other.GetComponent<EnemyAI>().CanGetHitByFire)
            {
                other.GetComponent<EnemyAI>().OnHit(damage, true);
                other.GetComponent<EnemyAI>().OnFire = true;
            }
        }
    }

    void OnDestroy()
    {
        if (NavManager.instance != null){
            NavManager.instance.Rebuild();
        }
    }
}
