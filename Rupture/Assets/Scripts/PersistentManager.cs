﻿using UnityEngine;

public class PersistentManager : MonoBehaviour
{

    public static PersistentManager Instance { get; private set; }

	public bool devMode;
	public bool playerControlled = true;

	private void Awake()
	{
		if (Instance == null) {
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else{
			Destroy(gameObject);
		}


	}


}
