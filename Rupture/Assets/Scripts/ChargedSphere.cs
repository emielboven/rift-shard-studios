﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargedSphere : MonoBehaviour
{
    public float damage = 10;
    // Use this for initialization
    void Start()
    {
        Destroy(transform.parent.parent.gameObject, 20f);
    }

    // Update is called once per frame
    void Update()
    {

    }



    void OnTriggerStay(Collider other)
    {

        if (other.name == "Player")
        {
            if (other.GetComponent<PlayerController>() != null && !other.GetComponent<PlayerController>().isDashing && other.GetComponent<PlayerController>().CanGetHitByFire)
            {
                other.GetComponent<PlayerController>().OnFireHit(damage);
            }

        }

        if (other.tag == "Enemy")
        {
            if (other.GetComponent<EnemyAI>() != null && other.GetComponent<EnemyAI>().CanGetHitByFire)
            {
                other.GetComponent<EnemyAI>().OnHit(damage, true);

            }

        }
    }





}
