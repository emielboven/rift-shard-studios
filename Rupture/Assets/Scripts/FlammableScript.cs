﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlammableScript : MonoBehaviour
{
    public GameObject fireFX;
    public bool instantFlammable;
    protected bool addFireScript = true;
    public float burnTime = 10;

    private bool onFire;
    private float timer = 4;

    public GameObject debris;

    public bool burnDebris;


     public virtual void Start()
    {
        if (fireFX == null)
        {
            fireFX = Resources.Load("FireResources/MeshFire") as GameObject;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Fire" && !onFire)
        {
            if (instantFlammable)
            {
                SetFire();
            }
            else
            {
                WaitBeforeFlames();
            }
        }
    }

    protected virtual void WaitBeforeFlames()
    {
        if (timer > 0)
        {
            timer -= 1f * Time.deltaTime;
        }
        else
        {
            SetFire();
        }
    }

    protected virtual void SetFire()
    {
        if (addFireScript)
        {
            FireScript script = gameObject.AddComponent<FireScript>() as FireScript;
            script.burnTime = burnTime;
        }

        onFire = true;
        GameObject fire = Instantiate(fireFX, transform.position, transform.rotation);
        fire.transform.SetParent(transform);
        var ps = fire.transform.GetChild(0).GetComponent<ParticleSystem>();
        var sh = ps.shape;
        sh.enabled = true;
        Vector3 size = GetComponent<Collider>().bounds.size;
        short volume = (short)(size.x + size.y + size.z +1);
        ps.emission.SetBursts(new ParticleSystem.Burst[1] { new ParticleSystem.Burst(0, volume, (short)(volume*2f), 1, 0.01f) }, 5);

        if (GetComponent<MeshRenderer>())
        {
            sh.shapeType = ParticleSystemShapeType.MeshRenderer;
            sh.meshRenderer = GetComponent<MeshRenderer>();
        }
        if (GetComponent<SkinnedMeshRenderer>())
        {
            sh.shapeType = ParticleSystemShapeType.SkinnedMeshRenderer;
            sh.skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
        }
    }

    private void OnDestroy()
    {
        if (debris != null)
        {
            debris.transform.SetParent(null);
            debris.SetActive(true);
            if (burnDebris)
            {
                MeshRenderer[] meshes = debris.GetComponentsInChildren<MeshRenderer>();
                foreach (MeshRenderer mesh in meshes)
                {
                    Collider col = mesh.gameObject.AddComponent<BoxCollider>();
                    col.isTrigger = true;
                    mesh.gameObject.GetComponent<Rigidbody>().AddExplosionForce(5, transform.position, 1, 1, ForceMode.Impulse);
                    FlammableScript flam = mesh.gameObject.AddComponent<FlammableScript>();
                    flam.instantFlammable = true;
                    flam.SetFire();
                }
            }

        }
    }

}
