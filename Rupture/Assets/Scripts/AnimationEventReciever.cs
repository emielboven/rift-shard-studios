﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventReciever : MonoBehaviour
{

    public GameObject footstepRight;
    public GameObject footstepLeft;

    private GameObject[] lefts;
    private GameObject[] rights;

    public Transform leftFoot;
    public Transform rightFoot;

    private int leftStepCount = 0;

    private int rightStepCount = 0;
    // Use this for initialization
    void Start()
    {
       
        lefts = new GameObject[5];
        rights = new GameObject[5];


        for (int i = 0; i < lefts.Length; i++)
        {
            GameObject go = Instantiate(footstepLeft, new Vector3(1000, 1000, 1000), Quaternion.identity);
            lefts[i] = go;
            go = Instantiate(footstepRight, new Vector3(1000, 1000, 1000), Quaternion.identity);
            rights[i] = go;
        }
    }

     public void OnHitEnd(AnimationEvent animationEvent)
    {
        //PersistentManager.Instance.playerControlled = true;
    }

    public void PlaceLeftStep(AnimationEvent animationEvent)
    {

        lefts[leftStepCount].transform.position = leftFoot.position;
        lefts[leftStepCount].transform.rotation = leftFoot.rotation;
        leftStepCount++;
        if (leftStepCount >= lefts.Length)
        {
            leftStepCount = 0;
        }
    }

    public void PlaceRightStep(AnimationEvent animationEvent)
    {
        rights[rightStepCount].transform.position = rightFoot.position;
        rights[rightStepCount].transform.rotation = rightFoot.rotation;
        rightStepCount++;
        if (rightStepCount >= rights.Length)
        {
            rightStepCount = 0;
        }
    }

}
