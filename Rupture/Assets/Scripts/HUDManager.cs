﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour {

	public Image healthBar;
	public Image healthBack;
	public GameObject crosshair;
    public GameObject pickupText;
    public Image tinderCounter;
    public Image bombCounter;

    public Sprite[] counterSprites;
    public Image tinderCooldown;
    public Image bombCooldown;
    public Image pushCooldown;
	public Sprite bombs;
	public Sprite tinder;

    public Animator tinderAnim;
    public Animator bombsAnim;
public Animator pushAnim;
	private PlayerStats stats;


	void Start () {
		stats = GameObject.Find("Player").GetComponent<PlayerStats>();
		crosshair.SetActive(false);
        pickupText.SetActive(false);
	}
	
	public void UpdateHealth () {
		healthBack.fillAmount = healthBar.fillAmount;
		healthBar.fillAmount = stats.health / stats.maxHealth;
		StartCoroutine(HealthCutoff());
	}


    public void UpdateBombCounter()
    {

            bombCounter.sprite = counterSprites[stats.numberOfBombs];
       
    }

        public void UpdateTinderCounter()
    {

            tinderCounter.sprite = counterSprites[stats.numberOfTinder];
       
    }
  

	IEnumerator HealthCutoff()
    {   
        yield return new WaitForSeconds(.5f);
        healthBack.fillAmount = healthBar.fillAmount;
    }

    public void FlashTinder(){
        tinderAnim.SetTrigger("Flash"); 
    }
        public void FlashBombs(){
        bombsAnim.SetTrigger("Flash");
    }
    public void FlashPush(){
        pushAnim.SetTrigger("Flash");
    }
}
