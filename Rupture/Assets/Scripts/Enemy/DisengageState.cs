﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisengageState : FSMState {

    private TestEnemy testEnemy;
    bool firstFrame;

    public DisengageState()
    {
        stateID = StateID.Disengage;
    }

    public override void DoBeforeEntering()
    {
        Debug.Log("ENTERING DISENGAGE");
        firstFrame = false;
    } 

    public override void Reason(GameObject player, GameObject npc)
    {
        if (!testEnemy || !firstFrame)
        {
            testEnemy = npc.GetComponent<TestEnemy>();
            testEnemy.rotated = false;
            firstFrame = true;
        }
        //    Debug.Log("DISENGAGE!");



        if (Vector3.Distance(testEnemy.transform.position, player.transform.position) < testEnemy.engageDistance)
        {
            testEnemy.SetTransition(Transition.GoToEngage);

        }
        else if(Vector3.Distance(testEnemy.transform.position, testEnemy.originalPosition) < 1)
        { 
        //    Debug.Log("GOTOIDLE"); 
            testEnemy.SetTransition(Transition.GoToIdle);
        }

    }

    public override void Act(GameObject player, GameObject npc)
    {
        if (!testEnemy || !firstFrame)
        {
            testEnemy = npc.GetComponent<TestEnemy>();
            testEnemy.rotated = false;
            firstFrame = true;
        }

        if (!testEnemy.rotated)
        {
            testEnemy.agent.SetDestination(testEnemy.originalPosition);
            testEnemy.RotateToDestination();
        }
        else
        {
            testEnemy.agent.SetDestination(testEnemy.originalPosition);
        }

    }

    public override void DoBeforeLeaving()
    {
        if(stateID == StateID.Idle)
        {
            testEnemy.walkingToWp = false;
        }
    }
}
