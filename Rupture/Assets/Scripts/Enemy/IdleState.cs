﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : FSMState
{

    int target = -1;
    private TestEnemy testEnemy;
    bool foundPlayer;
    bool isStanding;

    public IdleState()
    {
        stateID = StateID.Idle;
    }

    public override void DoBeforeEntering()
    {
        target = -1;

        foundPlayer = false;
        //     Debug.Log("INIDLE");
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        if (!testEnemy)
        {
            testEnemy = npc.GetComponent<TestEnemy>();
        }

        //if the player is in engage distance of the enemy the enemy will go into engage state
        if (Vector3.Distance(testEnemy.transform.position, player.transform.position) < testEnemy.engageDistance)
        {
            //       Debug.Log("IN IDLE -> FOUND PLAYER! ENGAGE!");
            foundPlayer = true;
            testEnemy.SetTransition(Transition.GoToEngage);
        }

    }

    public override void Act(GameObject player, GameObject npc)
    {
        if (!testEnemy)
        {
            testEnemy = npc.GetComponent<TestEnemy>();
        }

        //if enemy has not found the player
        if (foundPlayer)
        {
            testEnemy.walkingToWp = false;
            testEnemy.rotated = false;
            testEnemy.standStill = false;
        }
        else
        {
            if (testEnemy.standStill)
            {
                if (!isStanding)
                {
                    isStanding = true;
                    testEnemy.StandStillCooldown();
                }

            }
            else
            {
                //Walking to Wp = if Enemy is walking to its waypoint and has not arrived yet. 
                if (!testEnemy.walkingToWp)
                {
                    //if Enemy has arrived at waypoint then look for a new wp to walk to.
                    testEnemy.agent.destination = testEnemy.waypoints[MoveToNextWaypoint()].transform.position;

                    //enemy now has a new waypoint and is walking towards it
                    testEnemy.walkingToWp = true;
                    testEnemy.rotated = false;
                }

                //if enemy is not rotated to waypoint
                if (!testEnemy.rotated)
                {
                    testEnemy.RotateToDestination();
                }
                else
                {
                    //if the distance between enemy and waypoint is 1 then it has arrived at the waypoint
                    if (Vector3.Distance(testEnemy.transform.position, testEnemy.agent.destination) <= 1)
                    {
                        //enemy has arrived at waypoint
                        testEnemy.walkingToWp = false;
                        testEnemy.standStill = true;
                        isStanding = false;
                    }
                }
            }
        }

    }

    public int MoveToNextWaypoint()
    {

        if (target < testEnemy.waypoints.Length - 1 && target > -1)
        {
            target++;
        }
        else if (target == -1)
        {
            target = Random.Range(0, testEnemy.waypoints.Length - 1);
        }
        else
        {

            target = 0;
        }
        return target;
    }


    public override void DoBeforeLeaving()
    {
        foundPlayer = false;
        testEnemy.walkingToWp = false;
        testEnemy.rotated = true;
        testEnemy.standStill = false;
    }

}
