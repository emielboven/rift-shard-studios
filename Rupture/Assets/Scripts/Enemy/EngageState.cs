﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngageState : FSMState {

    private TestEnemy testEnemy;
    bool firstFrame;
    bool lostPlayerSight;

    public EngageState()
    {
        stateID = StateID.Engage;
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        if (!testEnemy || !firstFrame)
        {
            testEnemy = npc.GetComponent<TestEnemy>();
            testEnemy.rotated = false;
            firstFrame = true;
        }

        if (Vector3.Distance(testEnemy.transform.position, player.transform.position) < testEnemy.aggroDistance)
        {
            Debug.Log("ENGAGESTATE -> ENEMY IN AGGRO DISTANCE ATTACK");
            testEnemy.SetTransition(Transition.GoToAttack);
        }
        else
        {
            if (lostPlayerSight)
            {
                Debug.Log("LOST PLAYER RUN BACK");
                lostPlayerSight = false;
                testEnemy.SetTransition(Transition.GoToDisEngage);
            }
        }
    }

    public override void Act(GameObject player, GameObject npc)
    {
        if (!testEnemy || !firstFrame)
        {
            testEnemy = npc.GetComponent<TestEnemy>();
            testEnemy.rotated = false;
            firstFrame = true;
        }

        if (!testEnemy.rotated)
        {
            Debug.Log("ENEMY IS NOT ROTATED");
            testEnemy.agent.destination = player.transform.position;
            testEnemy.RotateToDestination();            
        }
        else
        {
            if (!lostPlayerSight)
            {
                if (Vector3.Distance(testEnemy.transform.position, player.transform.position) < testEnemy.engageDistance)
                {
                    testEnemy.agent.destination = player.transform.position;
                }
                else
                {
                    lostPlayerSight = true;
                }
            }        
        }
    }
}
