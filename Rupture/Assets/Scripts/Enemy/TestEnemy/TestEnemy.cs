﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEnemy : BaseEnemy
{
    public GameObject parriedFX;
    public GameObject deathFX;
    public GameObject hitFX;
    public Transform[] waypoints;
    public bool walkingToWp = false;
    public float engageDistance = 12;
    public float aggroDistance = 8;
    public float attackDistance = 3;
    public float longRangeAttackChance = 5;
    public bool longRangeAttackInterval;
    public float longRangeAttackIntervalCounter = 5;

    public bool attackInterval;
    public float attackIntervalTimer = 2f;
    public float longRangeAttackDistance = 5;
    public float shortRangeAttackDistance = 3;
    public LayerMask mask;
    
    [Header ("Weapon Range Detection")]
    public bool showEnemyViewRange;
    public float weaponRangeDistance = 1;
    public float weaponRangeSize = 1;


    [HideInInspector] public Vector3 originalPosition;

    public bool attacking;

    public float standStillCooldown = 5f;
    public bool standStill;
    public bool rotated;
    public bool combo;

    public override void Start()
    {
        originalPosition = transform.transform.position;
        weapon = transform.Find("SwordHolder").GetChild(0).GetComponent<EnemyWeaponScript>();
        parriedFX.SetActive(false);
        deathFX.SetActive(false);

        base.Start();
    }

    public override void OnHit(float damage)
    {

        unBlockableMove = false;
        overwriteOnHitImmunity = true;
        if (Immune)
            return;

        base.OnHit(damage);

        Debug.Log(Immune);
        Debug.Log("ONHIT");
        anim.SetTrigger("OnHit1");

        stunned = true;

        attacking = false;
        anim.SetBool("Attack", false);
        anim.SetBool("Attack2", false);

        parried = false;
        anim.ResetTrigger("Parried");
        anim.ResetTrigger("KnockBack");
       // anim.SetBool("OnHit", true);


        StartCoroutine(Immunity());
        StartCoroutine(HitFXTimer());

    }

    public override void OnHitEnd(AnimationEvent animationEvent)
    {
        stunned = false;
        StartCoroutine("CoolDown", knockBackIntervalTimer);
        parriedFX.SetActive(false);
    }

    public override void OnParried()
    {
        Debug.Log("ONPARRIED");
        parriedFX.SetActive(true);
        anim.SetBool("Attack", false);
        anim.SetBool("Attack2", false);

        attacking = false;

        anim.SetTrigger("Parried");

        stunned = true;
        StartCoroutine("CoolDown", parryKnockBackIntervalTimer);

    }

    public override void OnParriedEnd(AnimationEvent animationEvent)
    {
        Debug.Log("ONPARRIED END");
        anim.ResetTrigger("Parried");
        parriedFX.SetActive(false);
        stunned = false;
        parried = false;

    }

    public override void OnKnockBack()
    {
        //weapon.GetComponent<BoxCollider>().enabled = false;

        anim.SetBool("Attack", false);

        attacking = false;

        anim.SetTrigger("KnockBack");

        stunned = true;
        Debug.Log("KNOCKBACK");
    }

    public override void OnKnockBackEnd(AnimationEvent animationEvent)
    {
        Debug.Log("KNOCKBACKEND");
        stunned = false;

        anim.ResetTrigger("KnockBack");
    }

    void OnDrawGizmos()
    {
        if (showEnemyViewRange)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position + (transform.forward * weaponRangeDistance), weaponRangeSize);
        }
    }

    public void EnableWeaponCollider(AnimationEvent animationEvent)
    {
        //weapon.GetComponent<BoxCollider>().enabled = true;
        //weapon.Collider.enabled = true;
    }

    public void DisableWeaponCollider(AnimationEvent animationEvent)
    {
       // weapon.GetComponent<BoxCollider>().enabled = false;
        //weapon.Collider.enabled = false;
    }

    public void AttackHasStopped(AnimationEvent animationEvent)
    {
        attacking = false;
        anim.SetBool("Attack", false);
        anim.SetBool("Attack2", false);
        unBlockableMove = false;

        StartCoroutine("CoolDown", attackIntervalTimer);
        // transform.LookAt(player.transform.position, Vector3.up);
    }

    public void EnableParry(AnimationEvent animationEvent)
    {
        parriable = true;
    }

    public void DisableParry(AnimationEvent animationEvent)
    {
        parriable = false;
    }


    public IEnumerator CoolDown(float time)
    {
        attackInterval = true;
        //weapon.GetComponent<BoxCollider>().enabled = false;
        //weapon.Collider.enabled = false;
        yield return new WaitForSeconds(time);
        attackInterval = false;
    }

    public IEnumerator LongRangeCoolDown(float time)
    {
        yield return new WaitForSeconds(time);
        longRangeAttackInterval = false;
    }

    public void CheckLongRange()
    {
        if (!longRangeAttackInterval)
        {
            longRangeAttackInterval = true;
            StartCoroutine("LongRangeCoolDown", longRangeAttackIntervalCounter);
        }
    }

    public override void OnDeath()
    {
        base.OnDeath();
         deathFX.transform.parent = null;
        deathFX.SetActive(true);
        anim.SetBool("Alive", false);

    }
     IEnumerator HitFXTimer()
    {
        hitFX.SetActive(false);
        hitFX.SetActive(true);
        yield return new WaitForSeconds(0.6f);
        hitFX.SetActive(false);
    }


    public void RotateToDestination()
    {
        //rotate to destination
        var targetRotation = Quaternion.LookRotation(agent.destination - transform.parent.position);// testEnemy.transform.parent.LookAt(player.transform.position, Vector3.up);
        transform.parent.rotation = Quaternion.RotateTowards(transform.parent.rotation, targetRotation, rotationSpeed);
        agent.speed = 0;

        /*
        Debug.Log(testEnemy.transform.parent.rotation);
        Debug.Log(targetRotation);
        Debug.Log(Quaternion.Angle(testEnemy.transform.parent.rotation, targetRotation));
        */

        //check the angle between enemy and waypoint
        if (Quaternion.Angle(transform.parent.rotation, targetRotation) < 6)
        {
            //if angle is lower than 6 then you are rotated
            agent.speed = 3.5f;
            rotated = true;
        }
    }

    public void StandStillCooldown()
    {
        if (standStill)
        {
            StartCoroutine("StandStillCoolDown", standStillCooldown);
        }
    }
    public IEnumerator StandStillCoolDown(float time)
    {
        Debug.Log("STANDINGSTILL"); 
        yield return new WaitForSeconds(time);
        standStill = false;
    }

}
