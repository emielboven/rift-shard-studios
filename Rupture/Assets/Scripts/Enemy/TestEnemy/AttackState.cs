﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : FSMState
{


    public AttackState()
    {
        stateID = StateID.Attack;
    }

    TestEnemy testEnemy;
    bool playerInAttackView;


    public override void Act(GameObject player, GameObject npc)
    {
        if (!testEnemy)
        {
            testEnemy = npc.GetComponent<TestEnemy>();
        }
      //  Debug.Log(testEnemy.stunned);
        if (!testEnemy.stunned)
        {
            Collider[] hitColliders = Physics.OverlapSphere(testEnemy.transform.position + (testEnemy.transform.forward * testEnemy.weaponRangeDistance), testEnemy.weaponRangeSize, testEnemy.mask);
            if (hitColliders != null)
            {

                if (hitColliders.Length > 0)
                {
                    playerInAttackView = true;
                }
                else
                {
                    playerInAttackView = false;
                }

            }

            if (testEnemy.attackInterval || !playerInAttackView && !testEnemy.attacking)
            {
                //   Debug.Log("ROTATTEEEE");
                var targetRotation = Quaternion.LookRotation(player.transform.position - testEnemy.transform.parent.position);// testEnemy.transform.parent.LookAt(player.transform.position, Vector3.up);
                testEnemy.transform.parent.rotation = Quaternion.Slerp(testEnemy.transform.parent.rotation, targetRotation, testEnemy.rotationSpeed * Time.deltaTime);

            }

            //Check if I am attacking and if I am in attack range
            if (Vector3.Distance(testEnemy.transform.position, player.transform.position) > testEnemy.shortRangeAttackDistance && !testEnemy.attacking || !testEnemy.attacking && !playerInAttackView)
            {
                    //    Debug.Log("ATTACK STATE -> PLAYER IS NOT IN RANGE");
                    //If I am not in Attack range, walk to the player
                    testEnemy.agent.destination = player.transform.position;
                    testEnemy.agent.speed = 3.5f;
                    testEnemy.agent.isStopped = false;

                if(Vector3.Distance(testEnemy.transform.position, player.transform.position) < testEnemy.longRangeAttackDistance && !testEnemy.attackInterval && Vector3.Distance(testEnemy.transform.position, player.transform.position) > testEnemy.shortRangeAttackDistance && !testEnemy.longRangeAttackInterval)
                {
                    Debug.Log(testEnemy.longRangeAttackInterval);
                    testEnemy.CheckLongRange();
                    int random = Random.Range(0, 10);
                    if (random < testEnemy.longRangeAttackChance)
                    {
                        Debug.Log("ATTACK 2");
                        Debug.Log(random);
                        testEnemy.unBlockableMove = true;
                        testEnemy.attacking = true;
                        testEnemy.anim.SetBool("Attack", true);
                        testEnemy.anim.SetBool("Attack2", true);
                        testEnemy.agent.speed = 3.5f;
                        testEnemy.agent.isStopped = false;
                    }                    
                }
                

            } 
            else
            {
            //        Debug.Log("ATTACK STATE -> PLAYER IS IN RANGE So StopMoving");
                    testEnemy.agent.destination = testEnemy.transform.position;
                    testEnemy.agent.speed = 0;
                    testEnemy.agent.isStopped = true;

                if (!testEnemy.attacking && !testEnemy.attackInterval && playerInAttackView)
                {

                        //       Debug.Log("CAN ATTACK SO ATTACK");
                        testEnemy.attacking = true;
                        testEnemy.anim.SetBool("Attack", true);
                        testEnemy.agent.speed = 3.5f;
                        testEnemy.agent.isStopped = false;
                        //       Debug.Log("PLAY ATTACK ANIM");
                    if(Random.Range(0,10) < 5)
                    {
                        testEnemy.combo = true;
                        testEnemy.anim.SetBool("Combo", true);
                    }
                    else
                    {
                        testEnemy.combo = false;
                        testEnemy.anim.SetBool("Combo", false);
                    }

                }

            }
        }
        else
        {

            testEnemy.anim.SetBool("Attack", false);
            testEnemy.attacking = false;
            testEnemy.agent.destination = testEnemy.transform.position;
            testEnemy.agent.speed = 0;
            testEnemy.agent.isStopped = true;
            //   testEnemy.parried = false;

        }
    }


    public override void Reason(GameObject player, GameObject npc)
    {
        if (!testEnemy)
        {
            testEnemy = npc.GetComponent<TestEnemy>();
        }

        if (Vector3.Distance(testEnemy.transform.position, player.transform.position) > testEnemy.engageDistance)
        {
            testEnemy.SetTransition(Transition.GoToDisEngage);
        }
    }

    public override void DoBeforeLeaving()
    {
        base.DoBeforeLeaving();
        testEnemy.attacking = false;
        testEnemy.anim.SetBool("Attack", false);
    }
}
