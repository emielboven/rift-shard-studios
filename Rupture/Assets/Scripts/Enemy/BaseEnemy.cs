﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class BaseEnemy : MonoBehaviour
{
    public GameObject player;
    private FSMSystem fsm;
    public NavMeshAgent agent;

    float currentHp;
    public float maxHp = 100;
    public float bonusDamage = 5;
    public Image healthBar;
    public Image healthBack;
    bool alive = true;
    public EnemyWeaponScript weapon;
    public bool Immune;
    public float immunityTime = 0.1f;
    public float rotationSpeed = 10;
    public float knockBackIntervalTimer = 1;
    public float parryKnockBackIntervalTimer = 0.5f;
    public bool unBlockableMove;
    public bool parriable;
    public bool stunned;
    public bool parried;
    public bool aggro;
    private Coroutine healthCoRoutine;

    [HideInInspector] public Animator anim;

    public void SetTransition(Transition t) { fsm.PerformTransition(t); }

    public virtual void Start()
    {
        agent = GetComponentInParent<NavMeshAgent>();
        player = GameObject.Find("Player");
        anim = GetComponent<Animator>();
        currentHp = maxHp;
        MakeFSM();
    }

    public virtual void FixedUpdate()
    {
        if (!alive)
            return;

        fsm.CurrentState.Reason(player, gameObject);
        fsm.CurrentState.Act(player, gameObject);

//if player is out of range don't show hud
       if (Vector3.Distance(transform.position, player.transform.position) <= 14 && !healthBack.transform.parent.gameObject.activeInHierarchy) {
           healthBack.transform.parent.gameObject.SetActive(true);
       }

        if (Vector3.Distance(transform.position, player.transform.position) > 14 && healthBack.transform.parent.gameObject.activeInHierarchy) {
           healthBack.transform.parent.gameObject.SetActive(false);
       }

    }

    // The NPC has two states: FollowPath and ChasePlayer
    // If it's on the first state and SawPlayer transition is fired, it changes to ChasePlayer
    // If it's on ChasePlayerState and LostPlayer transition is fired, it returns to FollowPath
    public virtual void MakeFSM()
    {

        IdleState idle = new IdleState();
        idle.AddTransition(Transition.GoToEngage, StateID.Engage);

        EngageState engage = new EngageState();
        engage.AddTransition(Transition.GoToDisEngage, StateID.Disengage);
        engage.AddTransition(Transition.GoToAttack, StateID.Attack);

        DisengageState disengage = new DisengageState();
        disengage.AddTransition(Transition.GoToIdle, StateID.Idle);
        disengage.AddTransition(Transition.GoToEngage, StateID.Engage); 

        AttackState attack = new AttackState();
        attack.AddTransition(Transition.GoToDisEngage, StateID.Disengage);

        fsm = new FSMSystem();
        fsm.AddState(idle);
        fsm.AddState(engage);
        fsm.AddState(disengage);
        fsm.AddState(attack);

    }

    public bool overwriteOnHitImmunity;

    public virtual void OnHit(float damage)
    {
        if (!alive || Immune)
            return;

        Immune = true;
        currentHp -= damage;
        healthBack.fillAmount = healthBar.fillAmount;
        healthBar.fillAmount = currentHp / maxHp;
            
        if (healthCoRoutine != null){
            StopCoroutine(healthCoRoutine);
        }
        healthCoRoutine = StartCoroutine(HUDHealthCutoff());

        if (currentHp <= 0)
        {
            OnDeath();
        }

        if (!overwriteOnHitImmunity)
        {
            StartCoroutine(Immunity());
        }
        Debug.Log("GOT HIT BY PLAYER" + " currentHP:  " + currentHp);
        
    }

    public virtual void OnKnockBack(){ }

    public virtual void OnParried() { }
    public virtual void OnParriedEnd(AnimationEvent animationEvent) { }

    public virtual void OnKnockBackEnd(AnimationEvent animationEvent) { }

    public virtual void OnHitEnd(AnimationEvent animationEvent) { }

    public virtual void OnDeadEnd(AnimationEvent animationEvent)
    {
        transform.parent.gameObject.SetActive(false);
    }

    public virtual void OnDeath()
    {
        Debug.Log("I AM DEAD!!");
        alive = false;
    }

    IEnumerator HUDHealthCutoff()
    {
        yield return new WaitForSeconds(.5f);
        healthBack.fillAmount = healthBar.fillAmount;
    }

    public IEnumerator Immunity()
    {
        Immune = true;
        yield return new WaitForSeconds(immunityTime);
        Immune = false;
    }
}
