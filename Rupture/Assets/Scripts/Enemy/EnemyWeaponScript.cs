﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeaponScript : MonoBehaviour {

    private float damage;
    void Start(){
        damage = GetComponentInParent<EnemyAI>().attackDamage;
    }

    void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.name == "PlayerMesh")
        {
                other.gameObject.GetComponentInParent<PlayerController>().OnHit(damage);
        }
    }

   

}
