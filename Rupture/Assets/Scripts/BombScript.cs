﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombScript : MonoBehaviour
{
    public GameObject explosionPrefab;
    public GameObject sorchMarkPrefab;
  
    public float explosionRadius = 2;
    public float damage = 10;
    public float force;

    private GameObject explosionFX;
    private Rigidbody rb;
    
    void Start()
    {
        explosionFX = transform.GetChild(0).GetChild(0).gameObject;
        explosionFX.SetActive(false);
        rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * force, ForceMode.Impulse);
    }
    
    void OnCollisionEnter(Collision other)
    {

        transform.rotation = Quaternion.identity;
        explosionFX.SetActive(true);
        explosionFX.transform.parent.SetParent(null);
       
        Destroy(explosionFX.transform.GetChild(0).gameObject, 0.1f);
        Destroy(gameObject);

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Fire")
        {
            transform.rotation = Quaternion.identity;
            explosionFX.SetActive(true);
            explosionFX.transform.parent.SetParent(null);

            Destroy(explosionFX.transform.GetChild(0).gameObject, 0.1f);
            Destroy(gameObject);
        }
    }

   
}
