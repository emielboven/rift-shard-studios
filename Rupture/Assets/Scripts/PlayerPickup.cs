﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPickup : MonoBehaviour {

    public PlayerStats stats;
    public HUDManager hud;
    public List<Collider> currentItems = new List<Collider>();
    
	void Start ()
    {
        hud = GameObject.Find("HUDManager").GetComponent<HUDManager>();
        stats = GetComponent<PlayerStats>();
    }
	
	void Update () {
        if (Input.GetButtonDown("Interact"))
        {
            if(currentItems.Count > 0)
            {
                Pickup();
            }
        }
        if (currentItems.Count > 0)
        {
            hud.pickupText.SetActive(true);
        }
        else
        {
            hud.pickupText.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "pickupItem" && !currentItems.Contains(other))
        {
            currentItems.Add(other);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "pickupItem" && currentItems.Contains(other))
        {
            currentItems.Remove(other);
        }
    }

    void Pickup()
    {
        Collider closestItemColl = null;
        float shortestDist = -1;
        foreach (Collider c in currentItems)
        {
            float dist = Vector3.Distance(c.transform.position, transform.position);
            if (shortestDist == -1 || dist < shortestDist)
            {
                shortestDist = dist;
                closestItemColl = c;
            }
        }
        
        currentItems.Remove(closestItemColl);

        string fullItemName = closestItemColl.name;
        //Alleen de eerste 6 tekens worden opgeslagen zodat je ze makelijk kunt dupliceren.
        string itemName = fullItemName.Substring(0, 6);

        if (itemName == "Mushro")
        {
            Destroy(closestItemColl.gameObject);
            stats.numberOfBombs++;
            hud.pickupText.SetActive(false);
           // hud.UpdatePowerCounter();
        }
        else if (itemName == "Tinder")
        {
            Destroy(closestItemColl.gameObject);
            stats.numberOfTinder++;
            hud.pickupText.SetActive(false);
           // hud.UpdatePowerCounter();
        }
    }
}
