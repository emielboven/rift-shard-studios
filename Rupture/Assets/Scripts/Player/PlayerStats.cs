﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{

    [Header("Combat")]
    public float health = 10;
    public float maxHealth = 10;
    public float bonusDamage = 0;
    public float weaponSpeed = 1.5f;
    public float throwableSpeed = 50;
    public float pushRadius;
    public float pushForce;
    public float pushCooldown= 1;
    [HideInInspector]
    public float pushTimer=1;

    [Range(0, 100)]
    public float dmgReduction = 0;

    [Header("Movement")]
    public float moveSpeed = 5;
    public float sneakSpeed = 2f;
    public float dashTime = 0.3f;
    public float dashSpeed = 2f;
    public float dashCooldown = 1f;


    [Header("Special Attack")]
    public bool showPlayerSpecialAttackRange;
    public float specialAttackMultiplier = 2;
    public float SpecialAttackRange = 1.4f;
    public float SpecialAttackSize = 1.6f;
    public LayerMask specialAttackMask;

    [Header("Items")]
    public int numberOfTinder;
    public int numberOfBombs;
    
    [Header("Fire")]
    public float maxPlacementDepth = 2f;
    public float placementDistance = 1.7f;

}
