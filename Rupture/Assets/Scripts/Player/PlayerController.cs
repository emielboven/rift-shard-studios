﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.PostProcessing;
using XInputDotNetPure;

public class PlayerController : MonoBehaviour
{

    PlayerIndex playerIndex;
    PlayerIndex controllerNumber = PlayerIndex.One;
    GamePadState state = GamePad.GetState(PlayerIndex.One);
    public float vibrationTime = 0.2f;
    public bool vibrating;
    public PlayerStats stats;
    public HUDManager hud;
    [Range(0, 1)]
    public float joystickThreshold = 0.3f;
    public bool freezeYWhileDashing;
    private Vector3 translation;
    private Vector3 dashDirection;
    private GameObject camY;
    private Rigidbody rb;
    public bool isDashing;
    [HideInInspector]
    public bool isAttacking;
    public bool isThrowing;
    public bool isWalking;
    public bool isRunning;

    public bool isSneaking;
    public bool isDeath;
    public bool canAttackAgain;
    public bool secondAttack;

    private GameObject dashFX;
    private GameObject dashFX2;
    private GameObject pushFX;
    private GameObject hitFX;
    public ParticleSystem goreFX;
    [HideInInspector]
    public Animator anim;
    public GameObject throwable;
    //counter
    private float sprintCounter;

    [HideInInspector]
    public GameObject FireMarkerObject;
    public GameObject FirePrefab;

    private bool isHoldingPower;

    public bool isGrounded;
    private bool cancelPowers;
    public bool canWalk;

    public bool isSlowed;
    private bool canDash;
    private bool canPush;

    public float aimDelay = 0.5f;
    private float aimTimer = 0;
    public float slomoTime;

    public GameObject fireBlock;

    public bool fireTrail;
    public float trailDistance;
    private Vector3 lastTrailPos;
    public LayerMask raycastMask;
    public bool CanGetHitByFire;

    public float audioMovement = 0;

    public LayerMask walkLayer;
    public LayerMask groundedMask;
    public PostProcessingProfile postFX;

    public Color dashCooldownColor;
    public Material dashMaterial;
    public SkinnedMeshRenderer mesh;
    private float dashTime;
    private Transform fireParent;

    private bool firstFireHit;

    private float heatBuildup;
    private bool isHurt;

    public Color hitColor;

    void Start()
    {

        anim = transform.GetChild(0).GetChild(0).GetComponent<Animator>();
        camY = GameObject.Find("CamY");
        rb = GetComponent<Rigidbody>();
        StartCoroutine(FreezeWhileLoading());
        stats = GetComponent<PlayerStats>();
        hud = GameObject.Find("HUDManager").GetComponent<HUDManager>();
        canDash = true;
        canPush = true;
        dashFX = transform.Find("DashFX").gameObject;
        dashFX.SetActive(false);
        dashFX2 = transform.Find("DashFX2").gameObject;
        dashFX2.SetActive(false);
        hitFX = transform.Find("HitFX").gameObject;
        hitFX.SetActive(false);
        pushFX = transform.Find("PushFX").gameObject;
        pushFX.SetActive(false);
        FireMarkerObject.SetActive(false);
        PlayerIndex player = PlayerIndex.One;
        GamePadState testState = GamePad.GetState(player);
        if (testState.IsConnected)
        {
            Debug.Log("|| Controller Connected ||");
        }
        //AkSoundEngine.PostEvent("Moving", gameObject);
       // AkSoundEngine.SetState("InToxicCloudCheck", "OutsideCloud");

        dashMaterial = mesh.materials[8];
        if (CheckPoints.Instance.checkPoint != null)
        {
            transform.position = CheckPoints.Instance.checkPoint;
            transform.rotation = CheckPoints.Instance.checkPointRotation;
            camY.transform.rotation = CheckPoints.Instance.camRotation;
        }
    }

    void FixedUpdate()
    {

        if (!PersistentManager.Instance.devMode)
        {

            if (PersistentManager.Instance.playerControlled)
            {
                Movement();
            }
        }

        if (isSlowed)
        {
            //AkSoundEngine.SetState("InToxicCloudCheck", "InsideCloud");
            OnSlowTime();
        }
        else if (slomoTime != 1)
        {
            OnSlowTimeEnd();
        }

        CheckForGround();
    }

    void Update()
    {
        if (stats.health <= 0 && !isDeath)
        {
            OnDeath();
        }
        if (isDeath)
        {

            var black = GameObject.Find("BlackScreen").GetComponent<FadeScript>();
            if (black.cnvs.alpha == 1)
            {
                SceneManager.LoadScene("Scene_Start");

            }
        }
       // AkSoundEngine.SetRTPCValue("Movement", audioMovement);
        if (PersistentManager.Instance.playerControlled)
        {

            if (Input.GetAxis("Power") > 0 && !isAttacking && !isDashing && !cancelPowers && stats.numberOfBombs >= 1)
            {
                if (isHoldingPower)
                {
                    if (aimTimer >= aimDelay)
                    {
                        Aiming();
                    }
                    else
                    {
                        aimTimer += 1 * Time.deltaTime;
                        transform.forward = camY.transform.forward;
                    }
                }
                else
                {
                    isHoldingPower = true;
                }
            }
            if (Input.GetAxis("Power") > 0 && !isAttacking && !isDashing && !cancelPowers && stats.numberOfBombs <= 0)
            {
                hud.FlashBombs();
            }


            if (Input.GetButtonDown("MakeFire") && !isAttacking && !isDashing && !cancelPowers && stats.numberOfTinder >= 1)
            {
                ShowFire();
            }
            if (Input.GetButtonUp("MakeFire") && !isAttacking && !isDashing && !cancelPowers)
            {
                MakeFire();
            }

            if (Input.GetButtonDown("MakeFire") && !isAttacking && !isDashing && !cancelPowers && stats.numberOfTinder <= 0)
            {
                hud.FlashTinder();
            }


            if (Input.GetAxis("Power") == 0 && isHoldingPower && !isAttacking && !isDashing && !cancelPowers)
            {
                Throw();
                aimTimer = 0;
                isHoldingPower = false;
            }




            if (Input.GetAxis("PowerCancel") > 0 && !isAttacking && !isDashing)
            {
                CancelThrow();
                cancelPowers = true;
                isHoldingPower = false;
                isThrowing = false;

            }

            if (Input.GetAxis("Power") == 0)
            {

                if (cancelPowers == true)
                {
                    cancelPowers = false;
                }

            }

            if (Input.GetButtonDown("Dash") && !isAttacking && !isDashing && canDash)
            {
                Dash();
                isThrowing = false;
                hud.crosshair.SetActive(false);

            }
            if (Input.GetButtonDown("PickUpThrowable") && stats.pushTimer < stats.pushCooldown)
            {
                hud.FlashPush();
            }
            if (Input.GetButtonDown("PickUpThrowable") && !isAttacking && !isDashing && stats.pushTimer >= stats.pushCooldown)
            {

                Push();
            }




        }

        anim.SetBool("walking", isWalking);
        anim.SetBool("running", isRunning);
        anim.SetBool("dashing", isDashing);

        if (canDash && dashTime < 1)
        {
            dashTime += 10 * Time.deltaTime;
            dashMaterial.SetColor("_EmissionColor", Color.Lerp(dashCooldownColor * 3f, Color.black, dashTime));
        }


        if (isHurt && postFX.vignette.settings.intensity != 0.35f)
        {
            var vignette = postFX.vignette.settings;
            vignette.intensity = Mathf.Lerp(vignette.intensity, 0.35f, Time.deltaTime * 50f);
            vignette.color = hitColor;
            postFX.vignette.settings = vignette;
        }
        if (!isHurt && postFX.vignette.settings.intensity != 0.15f)
        {
            var vignette = postFX.vignette.settings;
            vignette.intensity = Mathf.Lerp(vignette.intensity, 0.15f, Time.deltaTime * 20f);
            vignette.color = Color.black;
            postFX.vignette.settings = vignette;
        }
    }

    void VibrateController()
    {
        if (!vibrating)
        {
            StartCoroutine("VibrationTime", vibrationTime);
        }
    }


    IEnumerator VibrationTime(float time)
    {
        GamePad.SetVibration(playerIndex, 100, 100);
        yield return new WaitForSeconds(time);
        GamePad.SetVibration(playerIndex, 0, 0);
        vibrating = false;

    }

    void Movement()
    {

        if (Physics.CheckCapsule(transform.position + transform.TransformDirection(new Vector3(0, -0.3f, 0.4f)), transform.position + transform.TransformDirection(new Vector3(0, 0.75f, 0.4f)), 0.2f, walkLayer) && isGrounded)
        {
            canWalk = false;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
        else
        {
            canWalk = true;

        }

        //Get walk direction from input
        translation = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));




        if (!isDashing && !isAttacking)
        {

            if ((Input.GetAxis("Horizontal") < -joystickThreshold || Input.GetAxis("Horizontal") > joystickThreshold || Input.GetAxis("Vertical") < -joystickThreshold || Input.GetAxis("Vertical") > joystickThreshold) && canWalk)
            {
                if (isSneaking)
                {
                    audioMovement = 0;
                    rb.MovePosition(this.transform.position + camY.transform.TransformDirection(translation) * Time.deltaTime * stats.sneakSpeed * slomoTime);
                }
                else
                {
                    isWalking = true;
                    audioMovement = 30;
                    rb.MovePosition(this.transform.position + camY.transform.TransformDirection(translation) * Time.deltaTime * stats.moveSpeed * slomoTime);

                    if (Input.GetAxis("Horizontal") < -0.5f || Input.GetAxis("Horizontal") > 0.5f || Input.GetAxis("Vertical") < -0.5f || Input.GetAxis("Vertical") > 0.5f)
                    {
                        audioMovement = 60;
                        isRunning = true;
                    }
                }

            }
            else
            {
                audioMovement = 0f;
                isWalking = false;
                isRunning = false;
            }


            //rotate character to face the walk direction
            if (Input.GetAxis("Horizontal") < -joystickThreshold || Input.GetAxis("Horizontal") > joystickThreshold || Input.GetAxis("Vertical") < -joystickThreshold || Input.GetAxis("Vertical") > joystickThreshold)
            {
                transform.forward = Vector3.Lerp(transform.forward, camY.transform.TransformDirection(translation), Time.deltaTime * 15);

            }

        }
        if (isDashing && canWalk)
        {
            rb.AddForce(dashDirection * stats.dashSpeed, ForceMode.VelocityChange);
            audioMovement = 80;
            if (rb.velocity.y < 0)
            {
                rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
            }

        }

        if (fireTrail)
        {
            if (Vector3.Distance(lastTrailPos, transform.position) > trailDistance)
            {
                RaycastHit hit;

                if (Physics.Raycast(transform.position, -Vector3.up, out hit, 2.0f, raycastMask))
                {
                    GameObject fireObject = Instantiate(fireBlock, hit.point, Quaternion.identity);

                    Vector3 scale = fireObject.transform.localScale;
                    scale.x = Random.Range(0.5f, 1.2f);
                    scale.z = Random.Range(0.5f, 1.2f);
                    fireObject.transform.localScale = scale;

                }

                lastTrailPos = transform.position;

            }
        }

        if (heatBuildup > 0)
        {
            heatBuildup -= Time.deltaTime;

        }
        else if (heatBuildup < 0)
        {
            heatBuildup = 0;
        }
    }



    void Dash()
    {

        if (translation != Vector3.zero)
        {
            dashDirection = camY.transform.TransformDirection(translation.normalized);
        }
        else
        {
            dashDirection = camY.transform.TransformDirection(new Vector3(0, 0, 1));

        }
        isDashing = true;
        transform.forward = dashDirection;
        dashFX.SetActive(true);
        StartCoroutine(DashTimer());
        StartCoroutine(DashFXTimer());
        StartCoroutine(DashCooldown());
        dashMaterial.SetColor("_EmissionColor", dashCooldownColor * 3f);

    }
    void Sneak()
    {
        //Blijft lekker leeg
    }
    void Push()
    {

        if (translation != Vector3.zero)
        {
            dashDirection = camY.transform.TransformDirection(translation.normalized);
        }
        else
        {
            dashDirection = transform.forward;

        }
        transform.forward = dashDirection;

        Collider[] colliders = Physics.OverlapSphere(transform.position + transform.TransformDirection(new Vector3(0, 0, stats.pushRadius + 0.2f)), stats.pushRadius);
        foreach (Collider collider in colliders)
        {
            if (collider.gameObject.tag == ("Enemy"))
            {
                collider.gameObject.GetComponent<EnemyAI>().OnPush(transform.position - transform.TransformDirection(new Vector3(0, 0, 2f)), stats.pushRadius * 4, stats.pushForce);
            }
            if (collider.gameObject.name == ("Player"))
            {
                //collider.gameObject.GetComponent<PlayerController>().OnHit(damage);
            }
            if (collider.gameObject.layer == 14 || collider.gameObject.layer == 15)
            {
                collider.gameObject.GetComponent<Rigidbody>().AddExplosionForce(stats.pushForce, transform.position - transform.TransformDirection(new Vector3(0, 0, 2f)), stats.pushRadius * 4, 0, ForceMode.Impulse);
            }
            else
            {
                if (collider.gameObject.tag != ("Enemy") && collider.gameObject.tag != ("Player"))
                {
                    if (collider.gameObject.GetComponent<Rigidbody>())
                    {
                        collider.gameObject.GetComponent<Rigidbody>().AddExplosionForce(stats.pushForce, transform.position - transform.TransformDirection(new Vector3(0, 0, 2f)), stats.pushRadius * 4, 0, ForceMode.Impulse);
                    }
                }

            }

        }
        StartCoroutine(PushCooldown());
        pushFX.SetActive(true);
    }

    IEnumerator PushCooldown()
    {
        stats.pushTimer = 0;
        yield return new WaitForSeconds(stats.pushCooldown);

        pushFX.SetActive(false);
    }

    void Aiming()
    {
        hud.crosshair.SetActive(true);
        isThrowing = true;
        transform.forward = camY.transform.forward;

    }
    void Throw()
    {
        hud.crosshair.SetActive(false);
        isThrowing = false;
        GameObject thrown = Instantiate(throwable, transform.position + (Vector3.up * 0.5f), transform.rotation);
        thrown.transform.LookAt((Camera.main.transform.position + (Vector3.up * 2)) + (Camera.main.transform.forward * 20));
        Physics.IgnoreCollision(thrown.GetComponent<Collider>(), GetComponent<Collider>(), true);
        thrown.GetComponent<BombScript>().force = stats.throwableSpeed;
        stats.numberOfBombs -= 1;
        hud.UpdateBombCounter();


    }

    void CancelThrow()
    {
        hud.crosshair.SetActive(false);
        isThrowing = false;
    }


    void ShowFire()
    {

        RaycastHit hit;

        if (Physics.Raycast(transform.position + (transform.forward * stats.placementDistance), -Vector3.up, out hit, stats.maxPlacementDepth, raycastMask))
        {
            if (Vector3.Distance(hit.normal, Vector3.up) < 0.8f)
            {
                if (!Physics.Raycast(transform.position + new Vector3(0, -0.7f, 0), transform.forward, out hit, stats.placementDistance, raycastMask))
                {
                    FireMarkerObject.SetActive(true);
                    FireMarkerObject.transform.position = hit.point;
                    FireMarkerObject.transform.up = hit.normal;
                    fireParent = hit.transform;

                }
                else
                {
                    FireMarkerObject.SetActive(false);
                }

            }
            else
            {
                FireMarkerObject.SetActive(false);
            }
        }
        else
        {
            FireMarkerObject.SetActive(false);
        }

    }
    void MakeFire()
    {
        if (FireMarkerObject.activeInHierarchy)
        {
            FireMarkerObject.SetActive(false);
            GameObject fireObject = Instantiate(FirePrefab, FireMarkerObject.transform.position, Quaternion.identity);
            fireObject.transform.up = FireMarkerObject.transform.up;
            fireObject.transform.SetParent(fireParent);
            stats.numberOfTinder--;
            hud.UpdateTinderCounter();
        }

    }

    void CancelFire()
    {
        FireMarkerObject.SetActive(false);

    }


    IEnumerator DashTimer()
    {
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
        if (freezeYWhileDashing)
        {
            rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;

        }
        yield return new WaitForSeconds(stats.dashTime);
        rb.constraints = RigidbodyConstraints.None;
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
        rb.velocity = Vector3.zero;
        isDashing = false;
        dashTime = 0;



        dashFX.SetActive(false);
        yield return new WaitForSeconds(0.3f);
        StartCoroutine(FireCooldown());
        fireTrail = false;
    }



    public void OnHit(float damage)
    {
        VibrateController();
        stats.health -= damage;
        hud.UpdateHealth();
        anim.SetTrigger("GetHit");
        StartCoroutine(HitFXTimer());

    }

    public void OnFireHit(float damage)
    {

        if (heatBuildup > 1)
        {
            VibrateController();
            stats.health -= damage;
            hud.UpdateHealth();
            anim.SetTrigger("GetHit");
            StartCoroutine(FireCooldown());
            StartCoroutine(HitFXTimer());
        }
        else
        {
            heatBuildup += Time.deltaTime * 2;
        }

    }

    IEnumerator FireCooldown()
    {

        CanGetHitByFire = false;
        yield return new WaitForSeconds(1f);
        CanGetHitByFire = true;
    }

    public void OnDeath()
    {
        isDeath = true;
        var black = GameObject.Find("BlackScreen").GetComponent<FadeScript>();
        black.fadeIn = false;


    }


    IEnumerator CanAttackTimer()
    {
        canAttackAgain = false;
        yield return new WaitForSeconds(0.2f);
        canAttackAgain = true;
    }

    IEnumerator DashFXTimer()
    {
        dashFX2.SetActive(true);
        yield return new WaitForSeconds(0.4f);
        dashFX2.SetActive(false);
    }
    IEnumerator DashCooldown()
    {
        canDash = false;
        yield return new WaitForSeconds(stats.dashCooldown);
        canDash = true;
    }

    IEnumerator HitFXTimer()
    {
        isHurt = true;
        yield return new WaitForSeconds(0.6f);
        isHurt = false;
    }

    IEnumerator GoreFXTimer()
    {
        yield return new WaitForSeconds(0.69f);
        goreFX.Play(withChildren: true);
        yield return new WaitForSeconds(1f);
        goreFX.Stop(withChildren: true);
    }

    void OnSlowTime()
    {

        if (!isDashing)
        {
            slomoTime = 0.3f;
            rb.velocity = rb.velocity * slomoTime;
            rb.angularVelocity = rb.angularVelocity * slomoTime;
            anim.SetFloat("TimeScale", slomoTime);
        }
        else
        {
            //rb.velocity = rb.velocity * 1.2f;
        }
    }

    void OnSlowTimeEnd()
    {
        slomoTime = 1f;
        anim.SetFloat("TimeScale", slomoTime);
        StartCoroutine(TimeCloudSoundStateTimer());

    }

    void CheckForGround()
    {

        if (Physics.CheckSphere(transform.position + Vector3.down, 0.5F, groundedMask))
        {
            isGrounded = true;

        }
        else
        {
            isGrounded = false;
        }
        anim.SetBool("grounded", isGrounded);

    }


    IEnumerator TimeCloudSoundStateTimer()
    {
        yield return new WaitForSeconds(0.1f);
       // AkSoundEngine.SetState("InToxicCloudCheck", "OutsideCloud");
    }
    IEnumerator FreezeWhileLoading()
    {
        //rb.isKinematic = true;
        yield return new WaitForSeconds(0.5f);
        //rb.isKinematic = false;
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position + transform.TransformDirection(new Vector3(0, 0, stats.pushRadius + 0.2f)), stats.pushRadius);
        if (stats.showPlayerSpecialAttackRange)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position + (transform.forward * stats.SpecialAttackRange), stats.SpecialAttackSize);
        }
    }

}
