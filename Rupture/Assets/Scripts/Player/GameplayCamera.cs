﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayCamera : MonoBehaviour
{

    [Range(0, 50)]
    public float Sensitivity;
    public GameObject camY;
    public GameObject camX;
    public GameObject camHolder;
    public GameObject cam;
    public Transform aimCam;
    public float camHeight = 1;
    public GameObject player;
    private PlayerController playerController;
    public LayerMask rayMask;
    public bool walkRotation;
    public bool aiming;
    private float walkRotate;
    public int camXMin = 330;
    public int camXMax = 80;

    public bool canDevMode;

    void Start()
    {
        player = GameObject.Find("Player");
        camY = GameObject.Find("CamY");
        camX = GameObject.Find("CamX");
        camHolder = GameObject.Find("CamHolder");
        aimCam = camHolder.transform.GetChild(0);
        cam = GameObject.Find("Main Camera");
        playerController = player.GetComponent<PlayerController>();

        camY.transform.position = player.transform.position;
        transform.SetParent(camHolder.transform);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

    }
    
    void FixedUpdate()
    {
        aiming = playerController.isThrowing;

        if (!PersistentManager.Instance.devMode)
        {
            if (PersistentManager.Instance.playerControlled)
            {
                ThirdPerson();
            }
        }

        if (PersistentManager.Instance.devMode)
        {
            FreeRoaming();
        }
    }




    void ThirdPerson()
    {
        if (playerController.isDashing)
        {
            camY.transform.position = Vector3.Lerp(camY.transform.position, player.transform.position + new Vector3(0, camHeight, 0), Time.deltaTime * 2);
        }
        else
        {
            camY.transform.position = Vector3.Lerp(camY.transform.position, player.transform.position + new Vector3(0, camHeight, 0), Time.deltaTime * 15);
        }
        float yRotate = Input.GetAxis("CamHorizontal");
        float xRotate = Input.GetAxis("CamVertical");

        if (!aiming)
        {
            camY.transform.Rotate(0, yRotate * Time.deltaTime * Sensitivity * 10, 0);
            camX.transform.Rotate(xRotate * Time.deltaTime * Sensitivity * 10, 0, 0);
        }
        else
        {
            camY.transform.Rotate(0, yRotate * Time.deltaTime * Sensitivity * 7, 0);
            camX.transform.Rotate(xRotate * Time.deltaTime * Sensitivity * 7, 0, 0);
        }

        if (walkRotation)
        {

            if (Input.GetAxis("Horizontal") < -0.1f || Input.GetAxis("Horizontal") > 0.1f)
            {
                walkRotate = Input.GetAxis("Horizontal") * 1.5f;
                camY.transform.Rotate(0, walkRotate, 0);
            }

        }

        RaycastHit hit;

        Vector3 heading = camHolder.transform.position - (player.transform.position + Vector3.up);
        float distance = heading.magnitude;
        Vector3 direction = heading / distance;
        if (aiming)
        {
            transform.position = Vector3.Lerp(this.transform.position, aimCam.position, Time.deltaTime * 20);
        }
        else
        {
            Ray r = new Ray(player.transform.position + Vector3.up, direction);
            if (Physics.Raycast(r, out hit, distance, rayMask))
            {
                gameObject.transform.position = hit.point - direction;
            }
            else
            {

                transform.position = Vector3.Lerp(this.transform.position, camHolder.transform.position, Time.deltaTime * 20);

            }
        }

        if (camX.transform.localRotation.eulerAngles.x < 90 && camX.transform.localRotation.eulerAngles.x > camXMax)
        {
            camX.transform.localRotation = Quaternion.Euler(camXMax, 0, 0);
        }

        if (camX.transform.localRotation.eulerAngles.x > 270 && camX.transform.localRotation.eulerAngles.x < camXMin)
        {
            camX.transform.localRotation = Quaternion.Euler(camXMin, 0, 0);
        }
        if (Input.GetButtonDown("ResetCamera"))
        {
            ResetCamera();
        }



    }
    void FreeRoaming()
    {
        float yRotate = Input.GetAxis("CamHorizontal");
        float xRotate = Input.GetAxis("CamVertical"); ;
        gameObject.transform.Rotate(0, yRotate * Time.deltaTime * Sensitivity * 10, 0);
        cam.transform.Rotate(xRotate * Time.deltaTime * Sensitivity * 10, 0, 0);


        Vector3 z = transform.InverseTransformDirection(cam.transform.forward * Input.GetAxis("Vertical"));
        Vector3 x = transform.InverseTransformDirection(cam.transform.right * Input.GetAxis("Horizontal"));

        Vector3 translation = new Vector3(x.x, z.y, z.z);




        transform.Translate(translation * Time.deltaTime * 20);



        if (Input.GetButtonDown("DevTeleport"))
        {
            RaycastHit hit;
            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, 500))
            {
                player.transform.position = hit.point;
            }
        }




    }

    void ResetCamera()
    {

        camY.transform.rotation = player.transform.rotation;
        camX.transform.localRotation = Quaternion.Euler(10, 0, 0);


    }



    void Update()
    {
        if (canDevMode)
        { 
            if (Input.GetButtonDown("DevMode"))
            {
                PersistentManager.Instance.devMode = !PersistentManager.Instance.devMode;
                Debug.Log("DevMode " + PersistentManager.Instance.devMode);

                if (PersistentManager.Instance.devMode)
                {
                    transform.SetParent(null);
                    transform.rotation = Quaternion.identity;
                    cam.transform.rotation = Quaternion.identity;

                }

                if (!PersistentManager.Instance.devMode)
                {
                    transform.SetParent(camHolder.transform);
                    transform.transform.position = camHolder.transform.position;
                    cam.transform.transform.position = transform.position;
                    transform.rotation = camHolder.transform.rotation;
                    cam.transform.rotation = transform.rotation;


                }
            }
        }
    }

}
