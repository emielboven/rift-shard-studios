﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FieldOfView : MonoBehaviour
{
    
    private Transform player;
    private Vector3 direction;

    [Header("Fov Properties")]
    public LayerMask obstacleLayermask;
    public float fovAngle = 60;
    public float fovRadius = 15;
    public float awarenessRange;
    public float personalRange;

    [Header("Fov Visualisation Properties")]
    public Color fovColor;
    public float meshResolution = 0.08f;
    public bool canSeePlayer;
    public bool canHearPlayer;
    public bool canFeelPlayer;

    [Header("Debug Values")]

    public bool showDebugVisualization;
    public float currentDist;
    public float currentAngle;

    void Start()
    {
        player = GameObject.Find("Player").transform;
    }

    private Vector3 debugVector(bool isGlobalAngle, float angle)
    {
        if (!isGlobalAngle)
        {
            angle += transform.eulerAngles.y;
        }
        Vector3 output = new Vector3(Mathf.Sin(angle * Mathf.Deg2Rad), 0, Mathf.Cos(angle * Mathf.Deg2Rad));
        return output;
    }

    public Vector3? outerDirection()
    {
        Vector3 dirToPlayer = player.position - transform.position;
         float angle = Vector3.Angle(transform.forward, dirToPlayer);

        currentAngle = angle; //DEBUG VALUE
        if (angle < (fovAngle / 2))
        {
            direction = dirToPlayer;
            return dirToPlayer;
        }
        else
        {
            direction = Vector3.zero;
            return null;
        }
    }

    void Update()
    {
        if (player != null)
        {
            outerDirection();
            currentDist = Vector3.Distance(transform.position, player.position);
            //Zit hij in het fov field (juiste hoek) EN zit hij in de range van de fov?
            if (direction != Vector3.zero && currentDist < fovRadius)
            {
                //Zit er niks met de layer obstacle LM tussen de enemy en de speler?
                Ray rayToPlayer = new Ray(transform.position, direction);
                RaycastHit hit;
                if (!Physics.Raycast(rayToPlayer, out hit, currentDist, obstacleLayermask))
                {
                    canSeePlayer = true;
                }
                else if (canSeePlayer)
                {
                    canSeePlayer = false;
                }

            }
            else if (canSeePlayer)
            {
                canSeePlayer = false;
            }

            if (currentDist < awarenessRange)
            {
                canHearPlayer = true;
            }
            else if (canHearPlayer)
            {
                canHearPlayer = false;
            }

            if (currentDist < personalRange)
            {
                canFeelPlayer = true;
            }
            else if (canFeelPlayer)
            {
                canFeelPlayer = false;
            }

        }
    }

    private void OnDrawGizmos()
    {
        if (player != null && showDebugVisualization )
        {
            //Line from enemy to player
            if (canSeePlayer)
            {
                Gizmos.color = Color.green;
            }
            else
            {
                Gizmos.color = Color.red;
            }
            Gizmos.DrawLine(transform.position, player.position);

            //Lines for debugging field of view
            int stepCount = Mathf.RoundToInt(fovAngle * meshResolution);
            float stepAngleSize = fovAngle / stepCount;
            for (int i = 0; i <= stepCount; i++)
            {
                float angle = transform.eulerAngles.y - fovAngle / 2 + stepAngleSize * i;
                Gizmos.color = fovColor;
                Gizmos.DrawLine(transform.position, transform.position + debugVector(true, angle) * fovRadius);
            }

            //Sphere for debugging audio range
            Gizmos.DrawWireSphere(transform.position, awarenessRange);

            Gizmos.DrawWireSphere(transform.position, personalRange);
        }
    }
}
