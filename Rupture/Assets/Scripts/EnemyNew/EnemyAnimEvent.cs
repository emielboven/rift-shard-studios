﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimEvent : MonoBehaviour
{

    public EnemyAI enemy;
    // Use this for initialization
    void Start()
    {
        enemy = this.transform.parent.GetComponent<EnemyAI>();
    }

    // Update is called once per frame

    void EnableWeaponCollider(AnimationEvent animationEvent)
    {

        enemy.weaponHitbox.enabled = true;
    }

    void DisableWeaponCollider(AnimationEvent animationEvent)
    {

        StartCoroutine(DisableWeapon());
    }

    public IEnumerator DisableWeapon()
    {
        yield return new WaitForSeconds(0.2f);
        enemy.weaponHitbox.enabled = false;
    }
    void ThrowThrowable(AnimationEvent animationEvent)
    {
        enemy.Throw();
    }

     void PickUpThrowable(AnimationEvent animationEvent)
    {

        enemy.ConnectThrowable();
    }


}
