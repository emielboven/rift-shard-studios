﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioObserver : MonoBehaviour {

    public static AudioObserver instance; //dit is de singleton

    //private List<Enemy> enemies = new List<Enemy>();

    private delegate void PlayerAudioDelegate(Vector3 audioPos);
    private event PlayerAudioDelegate onPlayerAudioEvent;

    void Awake()
    {
        instance = this;
    }

    //wordt aangeroepen in elke Enemy Class(in void Start):
    public void SubscribeEnemy(Enemy en)
    {
       // enemies.Add(en);
      //  onPlayerAudioEvent += en.ListenToAudio;
    }

    //moet worden aangeroepen als de enemy gedestroyd wordt
    public void UnSubscribeEnemy(Enemy en)
    {
       // enemies.Remove(en);
       // onPlayerAudioEvent -= en.ListenToAudio;
    }

    //wordt aangeroepen vanaf de speler
    public void PlayPlayerAudioEvent(Vector3 audioPos)
    {
        if(onPlayerAudioEvent != null){
            //event wordt hier afgespeeld; alle functies in de enemy zullen nu aangeroepen worden
            onPlayerAudioEvent(audioPos);
        }
    }

}
