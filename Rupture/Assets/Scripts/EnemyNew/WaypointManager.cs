﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointManager : MonoBehaviour
{


    public bool showWaypoints;
    public GameObject[] waypoints;
    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }



    void OnDrawGizmos()
    {
        if (showWaypoints)
        {
            foreach (GameObject wp in waypoints)
            {
                Gizmos.DrawSphere(wp.transform.position, 0.2f);
            }
            for (int i = 0; i < waypoints.Length; i++)
            {
                int wp2 = i+1;
                if (wp2 >= waypoints.Length)
                {
                    wp2 = 0;
                }
                Gizmos.DrawLine(waypoints[i].transform.position, waypoints[wp2].transform.position);
            }
        }
    }
}
