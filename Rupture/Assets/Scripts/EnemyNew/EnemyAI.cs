﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public enum State
{
    wandering,
    alerted,
    aggro,
    stunned
}
public class EnemyAI : MonoBehaviour
{
    public bool elite;
    public State state;
    public Transform[] waypoints;
    public Vector2 waitTimerRange;
    public Vector2 lookAroundTimerRange;
    public Collider weaponHitbox;

    [Header("Movement")]
    public float wanderSpeed;
    public float alertedSpeed;
    public float aggroSpeed;

    [Header("Combat")]
    public float attackDamage;

    private NavMeshAgent agent;
    private Animator anim;
    public FieldOfView fov;
    private EnemyHUD hud;
    private PlayerController playerScript;
    private EnemyStats stats;
    private Transform player;
    private Material markerMaterial;
    private float lineOfSightTimer = 0;
    private float WaitAtWayPointTimer = 0;
    private float WaitToLookAroundTimer = 0;
    private bool isStanding;
    private bool isWalking;
    private bool isInvestigating;
    private bool isLookingAround;
    private bool isAttacking;
    private bool isEngaging;
    private bool isEngagingThrowable;
    private bool isPickingUpThrowable;
    private bool isThrowing;
    private bool isThrowingCooldown;
    private bool isCalculating;
    public bool isSlowed;
    private bool isDeath;

    private bool hasPickedUp;
    private bool hasBeenSurprised;
    private bool hasTaunted;
    private Vector3 target;
    private int waypointNumber;

    private float slomoTime;

    //debug
    private GameObject targetMarker;
    private GameObject surpriseMarker;
    public bool CanGetHitByFire = true;

    public GameObject DeadBody;

    public LayerMask throwableMask;

    private GameObject pickupTarget;

    public Collider[] pickables;

    public Transform rightHand;

    private GameObject childObj;
    public Rigidbody rb;
    public bool isStunned;
    private GameObject stunFX;
    public GameObject fireFX;
    public bool OnFire;
    private Coroutine onFireCoroutine;
    private Coroutine tauntCoroutine;



    void Start()
    {
        stunFX = transform.Find("StunFX").gameObject;
        stunFX.SetActive(false);

        rb = GetComponent<Rigidbody>();
        EnemyManager.instance.SubscribeEnemy(this);
        CanGetHitByFire = true;
        agent = GetComponent<NavMeshAgent>();
        anim = transform.GetChild(0).GetComponent<Animator>();
        fov = GetComponent<FieldOfView>();
        stats = GetComponent<EnemyStats>();

        childObj = transform.GetChild(4).gameObject;
        childObj.SetActive(true);
        hud = GetComponentInChildren<EnemyHUD>();

        player = GameObject.Find("Player").transform;
        playerScript = player.GetComponent<PlayerController>();
        markerMaterial = transform.GetChild(1).GetComponent<Renderer>().material;
        targetMarker = transform.Find("Target").gameObject;
        surpriseMarker = transform.Find("ExclamationMark").gameObject;
        WaitAtWayPointTimer = Random.Range(waitTimerRange.x, waitTimerRange.y);
        WaitToLookAroundTimer = Random.Range(lookAroundTimerRange.x, lookAroundTimerRange.y);
    }

    void Update()
    {
        float HUDrange = 0;
        if (elite)
        {
            HUDrange = 5;
        }
        else
        {
            HUDrange = 10;
        }

        if (Vector3.Distance(transform.position, player.position) > HUDrange && hud.showHUD)
        {
            hud.showHUD = false;
        }
        if (Vector3.Distance(transform.position, player.position) <= HUDrange && !hud.showHUD)
        {
            hud.showHUD = true;
        }
        targetMarker.transform.position = target;
        if (state == State.wandering)
        {
            if (agent.speed != wanderSpeed)
            {
                agent.speed = wanderSpeed;
            }
            if (hasBeenSurprised)
            {
                hasBeenSurprised = false;
            }
            if (hasTaunted)
            {
                hasTaunted = false;
            }

            if (isInvestigating)
            {
                isInvestigating = false;
            }
            if (isLookingAround)
            {
                isLookingAround = false;
            }
            Wandering();
            markerMaterial.color = Color.black;

        }
        if (state == State.alerted)
        {

            if (agent.speed != alertedSpeed)
            {
                agent.speed = alertedSpeed;
            }
            if (isWalking)
            {
                isWalking = false;
            }
            if (isAttacking || isEngaging || isThrowing || isPickingUpThrowable || isEngagingThrowable || hasPickedUp || pickupTarget != null)
            {
                isAttacking = false;
                isThrowing = false;
                isPickingUpThrowable = false;
                isEngagingThrowable = false;
                isEngaging = false;
                hasPickedUp = false;
                if (pickupTarget != null)
                {
                    pickupTarget.layer = 14;
                }
                pickupTarget = null;
            }
            if (isStanding)
            {
                isStanding = false;
            }
            Alerted();
            markerMaterial.color = Color.yellow;
        }
        if (state == State.aggro)
        {
            if (agent.speed != aggroSpeed)
            {
                agent.speed = aggroSpeed;
            }
            if (!hasBeenSurprised)
            {
                hasBeenSurprised = true;
            }
            if (isInvestigating)
            {
                isInvestigating = false;
            }
            if (isLookingAround)
            {
                isLookingAround = false;
            }

            Aggro();
            markerMaterial.color = Color.red;


        }
        if (isStunned)
        {
            agent.isStopped = true;
        }

        if (isSlowed)
        {
            OnSlowTime();
        }
        else if (slomoTime != 1)
        {
            OnSlowTimeEnd();
        }
        if (stats.health <= 0 && !isDeath)
        {
            OnDeath();
        }

        if (OnFire)
        {

            fireFX.SetActive(true);
            tag = "Fire";
            if (CanGetHitByFire)
            {

                OnHit(7.5f, true);
                if (onFireCoroutine == null)
                {
                    onFireCoroutine = StartCoroutine(OnFireCooldown());
                }
            }
        }
        else if (fireFX.active == true)
        {
            fireFX.SetActive(false);
            tag = "Enemy";
        }
    }

    public IEnumerator OnFireCooldown()
    {
        OnFire = true;
        yield return new WaitForSeconds(5f);
        OnFire = false;
        onFireCoroutine = null;

    }



    // ------------------------------------------------------------------------------------------------------------------------------
    //	WANDERING - State & Substates
    // ------------------------------------------------------------------------------------------------------------------------------

    void Wandering()
    {
        if (!isWalking && !isStanding)
        {
            GoWalk();
        }

        if (!isWalking && isStanding)
        {
            if (WaitAtWayPointTimer <= 0)
            {
                GoWalk();
                WaitAtWayPointTimer = Random.Range(waitTimerRange.x, waitTimerRange.y);
            }
            else
            {
                WaitAtWayPointTimer -= Time.deltaTime;
            }
        }

        if (DistanceToTarget() < 1.5f)
        {
            GoStand();
        }

        if ((fov.canHearPlayer && !playerScript.isSneaking && playerScript.isWalking) || fov.canFeelPlayer || fov.canSeePlayer)
        {
            state = State.alerted;
        }


    }

    void GoWalk()
    {
        anim.SetBool("isWalking", true);
        isWalking = true;
        isStanding = false;
        agent.isStopped = false;
        agent.SetDestination(GetWaypointAsTarget());
    }

    void GoStand()
    {
        anim.SetBool("isWalking", false);
        isStanding = true;
        isWalking = false;
        agent.isStopped = true;
    }



    // ------------------------------------------------------------------------------------------------------------------------------
    //	ALERTED - State & Substates
    // ------------------------------------------------------------------------------------------------------------------------------

    void Alerted()
    {
        anim.SetBool("isRunning", false);

        if ((fov.canHearPlayer && !playerScript.isSneaking && playerScript.isWalking) || fov.canFeelPlayer || fov.canSeePlayer)
        {
            NavMeshHit hit;
            if (NavMesh.SamplePosition(player.position, out hit, 1f, NavMesh.AllAreas))
            {
                target = hit.position;
                agent.SetDestination(target);
            }
        }

        if (hasBeenSurprised)
        {
            if (fov.canSeePlayer)
            {
                state = State.aggro;
            }
            else
            {
                if (DistanceToTarget() < 1.5f)
                {
                    GoLookAround();
                }
                else if (isInvestigating == false)
                {
                    GoInvestigate();
                }
                if (isLookingAround && !isInvestigating)
                {
                    if (WaitToLookAroundTimer <= 0)
                    {
                        state = State.wandering;
                        WaitToLookAroundTimer = Random.Range(lookAroundTimerRange.x, lookAroundTimerRange.y);
                    }
                    else
                    {
                        WaitToLookAroundTimer -= Time.deltaTime;
                    }
                }
            }
        }
        else
        {
            GoBeSurprised();
            RotateToTarget();
        }

    }

    void GoLookAround()
    {
        anim.SetBool("isWalking", false);
        isLookingAround = true;
        isInvestigating = false;
        agent.isStopped = true;
    }


    void GoInvestigate()
    {
        anim.SetBool("isWalking", true);
        isInvestigating = true;
        isLookingAround = false;
        agent.isStopped = false;
    }

    void GoBeSurprised()
    {
        agent.isStopped = true;
        anim.SetBool("isWalking", false);
        StartCoroutine(Surprised());
    }
    public IEnumerator Surprised()
    {
        surpriseMarker.SetActive(true);
        yield return new WaitForSeconds(1);
        hasBeenSurprised = true;
        surpriseMarker.SetActive(false);
    }



    // ------------------------------------------------------------------------------------------------------------------------------ 
    // 	AGGRO - State & Substates
    // ------------------------------------------------------------------------------------------------------------------------------

    void Aggro()
    {

        if (hasTaunted)
        {

            if (!isEngaging && !isAttacking && !isEngagingThrowable && !isPickingUpThrowable && !isThrowing && !isThrowingCooldown)
            {
                CheckBattlefield();
            }
            else
            {
                if ((fov.canHearPlayer && !playerScript.isSneaking && playerScript.isWalking) || fov.canFeelPlayer || fov.canSeePlayer)
                {

                }
                else // if the player is out of sight and is sneaking go out of agro after some seconds
                {
                    if (!isThrowing && !hasPickedUp)
                    {
                        if (lineOfSightTimer < 6)
                        {
                            lineOfSightTimer += 1 * Time.deltaTime;
                        }
                        else
                        {
                            state = State.alerted;
                            lineOfSightTimer = 0;
                        }
                    }
                }
                if (isEngaging)
                {
                    Engage();
                }

                if (isEngagingThrowable)
                {
                    EngageThrowable();
                }
                if (isAttacking)
                {
                    RotateToTarget();
                }

                if (isPickingUpThrowable)
                {
                    PickupThrowable();
                }

                if (isThrowing && hasPickedUp)
                {
                    var heading = new Vector3(player.position.x, transform.position.y, player.position.z) - transform.position;
                    var distance = heading.magnitude;
                    var direction = heading / distance;
                    transform.forward = Vector3.Lerp(transform.forward, direction, Time.deltaTime * agent.angularSpeed * 0.05f);
                }
            }
        }
        else
        {
            GoTaunt();
            RotateToTarget();
        }

    }
    void GoTaunt()
    {
        agent.isStopped = true;
        anim.SetBool("isWalking", false);

        if (tauntCoroutine == null)
        {
            tauntCoroutine = StartCoroutine(Taunt());
        }
    }
    public IEnumerator Taunt()
    {
        anim.SetTrigger("Taunt");
        yield return new WaitForSeconds(1.7f);
        hasTaunted = true;
        tauntCoroutine = null;

    }


    void CheckBattlefield()
    {
        pickables = null;
        pickables = Physics.OverlapSphere(transform.position, fov.fovRadius * 1f, throwableMask);
        if (pickables.Length != 0 && Vector3.Distance(transform.position, player.position) > 5f)
        {
            foreach (Collider pick in pickables)
            {

                var heading = pick.transform.position - transform.position;
                var distance = heading.magnitude;
                var direction = heading / distance;
                Ray rayToThrowable = new Ray(transform.position, direction);

                if (!Physics.Raycast(rayToThrowable, distance, fov.obstacleLayermask))
                {
                    if (pickupTarget != null)
                    {
                        if (distance < Vector3.Distance(pickupTarget.transform.position, transform.position) && distance < Vector3.Distance(transform.position, player.position))
                        {
                            pickupTarget = pick.gameObject;
                        }
                    }
                    else
                    {
                        if (distance < Vector3.Distance(transform.position, player.position))
                        {
                            pickupTarget = pick.gameObject;
                        }
                    }
                }


            }
            if (pickupTarget != null)
            {
                pickupTarget.layer = 15;
                NavMeshHit hit;
                if (NavMesh.SamplePosition(pickupTarget.transform.position + pickupTarget.transform.forward, out hit, 2f, NavMesh.AllAreas))
                {
                    target = hit.position;
                    agent.SetDestination(target);
                }
                isEngagingThrowable = true;
            }
            else
            {
                isEngaging = true;
            }
        }
        else
        {
            isEngaging = true;
        }

    }
    void Engage()
    {
        //if line of sight or hearing the player update target position
        if ((fov.canHearPlayer && !playerScript.isSneaking && playerScript.isWalking) || fov.canFeelPlayer || fov.canSeePlayer)
        {
            NavMeshHit hit;
            if (NavMesh.SamplePosition(player.position, out hit, 1f, NavMesh.AllAreas))
            {
                target = hit.position;
                agent.SetDestination(target);
            }
        }
        else // if the player is out of sight and is sneaking go out of agro after some seconds
        {
            if (lineOfSightTimer < 6)
            {
                lineOfSightTimer += 1 * Time.deltaTime;
            }
            else
            {
                state = State.alerted;
                lineOfSightTimer = 0;
            }
        }
        if (DistanceToTarget() < 1.5f)
        {
            if (Vector3.Distance(transform.position, player.position) < 1.5f)
            {
                if (!isAttacking)
                {
                    Attack();
                }
            }
            anim.SetBool("isWalking", false);
            anim.SetBool("isRunning", false);

        }
        else
        {
            anim.SetBool("isWalking", true);
            anim.SetBool("isRunning", true);
            if (agent.isStopped && !isAttacking)
            {
                agent.isStopped = false;
            }
        }

        if (Random.Range(0, 300) < 1)
        {
            CheckBattlefield();
            isEngaging = false;
        }
    }

    void EngageThrowable()
    {

        anim.SetBool("isWalking", true);
        anim.SetBool("isRunning", true);
        if (agent.isStopped)
        {
            agent.isStopped = false;
        }

        if (Vector3.Distance(pickupTarget.transform.position, transform.position) <= 2f)
        {
            Debug.Log("At Target");
            isPickingUpThrowable = true;
            isEngagingThrowable = false;
            agent.isStopped = true;
        }

    }

    void PickupThrowable()
    {
        if (Vector3.Distance(pickupTarget.transform.position, transform.position) > 2f)
        {
            isEngagingThrowable = true;
            isPickingUpThrowable = false;

            NavMeshHit hit;
            if (NavMesh.SamplePosition(pickupTarget.transform.position + pickupTarget.transform.forward, out hit, 2f, NavMesh.AllAreas))
            {
                target = hit.position;
                agent.SetDestination(target);
            }

        }
        else
        {


            //lookat Target
            var heading = new Vector3(pickupTarget.transform.position.x, transform.position.y, pickupTarget.transform.position.z) - transform.position;
            var distance = heading.magnitude;
            var direction = heading / distance;
            transform.forward = Vector3.Lerp(transform.forward, direction, Time.deltaTime * agent.angularSpeed * 0.05f);
            if (transform.forward == direction)
            {
                anim.SetTrigger("PickUp");
                isThrowing = true;
                isPickingUpThrowable = false;
                isEngagingThrowable = false;

            }

            anim.SetBool("isWalking", false);
            anim.SetBool("isRunning", false);
            if (!agent.isStopped)
            {
                agent.isStopped = true;
            }

        }
    }

    public void ConnectThrowable()
    {
        pickupTarget.transform.SetParent(rightHand);
        pickupTarget.GetComponent<Rigidbody>().isKinematic = true;
        pickupTarget.transform.position = rightHand.transform.position;
        pickupTarget.transform.rotation = rightHand.transform.rotation;
        hasPickedUp = true;

    }
    public void Throw()
    {
        var heading = new Vector3(player.position.x, transform.position.y, player.position.z) - transform.position;
        var distance = heading.magnitude;
        var direction = heading / distance;
        transform.forward = Vector3.Lerp(transform.forward, direction, Time.deltaTime * agent.angularSpeed * 0.1f);

        var headingAim = (player.position + (Vector3.up * 0.5f)) - transform.position;
        var distanceAim = headingAim.magnitude;
        var directionAim = headingAim / distanceAim;
        pickupTarget.transform.SetParent(null);
        pickupTarget.GetComponent<Rigidbody>().isKinematic = false;
        pickupTarget.GetComponent<Rigidbody>().AddForce(directionAim * ((Vector3.Distance(player.position, transform.position) * 2.2f) + 1), ForceMode.Impulse);
        pickupTarget.GetComponent<Rigidbody>().AddTorque(100, 100, 100);
        pickupTarget.GetComponent<TrailRenderer>().enabled = true;
        pickupTarget.GetComponent<Throwable>().canBreak = true;
        pickupTarget = null;
        isThrowingCooldown = true;
        StartCoroutine(ThrowCooldown());
        isThrowing = false;
        isPickingUpThrowable = false;
        isEngagingThrowable = false;
        isEngaging = false;
        hasPickedUp = false;

    }

    void LostThrowable()
    {

    }

    void Attack()
    {
        isAttacking = true;
        agent.isStopped = true;
        agent.velocity = Vector3.zero;
        anim.SetTrigger("isAttacking");
        StartCoroutine(AttackCooldown());
    }
    public IEnumerator AttackCooldown()
    {
        yield return new WaitForSeconds(1.2f);
        agent.isStopped = false;
        isAttacking = false;
        CheckBattlefield();
    }
    public IEnumerator ThrowCooldown()
    {
        yield return new WaitForSeconds(1f);
        isThrowingCooldown = false;
    }



    // ------------------------------------------------------------------------------------------------------------------------------ 
    //Other functions
    // ------------------------------------------------------------------------------------------------------------------------------ 
    public void OnPush(Vector3 pos, float radius, float force)
    {
        StartCoroutine(Push(pos, radius, force));

    }

    IEnumerator Push(Vector3 pos, float radius, float force)
    {
        agent.isStopped = true;
        anim.SetBool("IsStunned", true);

        rb.isKinematic = false;
        rb.freezeRotation = true;
        rb.AddExplosionForce(force, pos, radius, 0, ForceMode.Impulse);
        isStunned = true;

        if (pickupTarget != null)
        {
            pickupTarget.transform.SetParent(null);
            pickupTarget.GetComponent<Rigidbody>().isKinematic = false;
            pickupTarget.layer = 14;
            pickupTarget = null;
            isThrowing = false;
            isPickingUpThrowable = false;
            isEngagingThrowable = false;
            isEngaging = false;
            hasPickedUp = false;
        }
        stunFX.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        rb.isKinematic = true;

        yield return new WaitForSeconds(Random.Range(0.5f, 1f));
        isStunned = false;
        stunFX.SetActive(false);
        anim.SetBool("IsStunned", false);
    }
    public void Retarget()
    {
        NavMeshHit hit;
        if (NavMesh.SamplePosition(target, out hit, 2f, NavMesh.AllAreas))
        {
            target = hit.position;
            agent.SetDestination(target);
        }
    }


    private float DistanceToTarget()
    {
        return Vector3.Distance(transform.position, target);
    }

    private Vector3 GetWaypointAsTarget()
    {
        target = waypoints[GetWaypointNumber()].position;

        return target;
    }

    private int GetWaypointNumber()
    {
        waypointNumber++;

        if (waypointNumber >= waypoints.Length)
        {
            waypointNumber = 0;
        }

        return waypointNumber;
    }

    void RotateToTarget()
    {
        var heading = new Vector3(target.x, transform.position.y, target.z) - transform.position;
        var distance = heading.magnitude;
        var direction = heading / distance;
        transform.forward = Vector3.Lerp(transform.forward, direction, Time.deltaTime * agent.angularSpeed * 0.1f);
    }

    public void OnHit(float damage, bool isFire)
    {
        if ((stats.health - damage) > 0)
        {
            stats.health -= damage;
        }
        else
        {
            stats.health = 0;
        }

        hud.UpdateHealth();

        if (isFire)
        {
            StartCoroutine(FireCooldown());
        }
    }

    IEnumerator FireCooldown()
    {
        CanGetHitByFire = false;
        yield return new WaitForSeconds(1f);
        CanGetHitByFire = true;
    }

    void OnSlowTime()
    {
        slomoTime = 0.3f;

        if (state == State.wandering)
        {
            agent.speed = wanderSpeed * slomoTime;
        }
        if (state == State.alerted)
        {
            agent.speed = alertedSpeed * slomoTime;
        }
        if (state == State.aggro)
        {
            agent.speed = aggroSpeed * slomoTime;
        }

        anim.SetFloat("TimeScale", slomoTime);
    }


    public void ExplosionTarget(Vector3 pos)
    {
        if (Vector3.Distance(pos, transform.position) < fov.awarenessRange * 2)
        {
            if (state == State.wandering)
            {
                state = State.alerted;
                NavMeshHit hit;
                if (NavMesh.SamplePosition(pos, out hit, 5f, NavMesh.AllAreas))
                {
                    target = hit.position;
                    agent.SetDestination(target);
                }
            }
            if (state == State.alerted)
            {
                NavMeshHit hit;
                if (NavMesh.SamplePosition(pos, out hit, 5f, NavMesh.AllAreas))
                {
                    target = hit.position;
                    agent.SetDestination(target);
                }
            }
        }
    }
    void OnSlowTimeEnd()
    {
        slomoTime = 1f;
        if (state == State.wandering)
        {
            agent.speed = wanderSpeed * slomoTime;
        }
        if (state == State.alerted)
        {
            agent.speed = alertedSpeed * slomoTime;
        }
        if (state == State.aggro)
        {
            agent.speed = aggroSpeed * slomoTime;
        }
        anim.SetFloat("TimeScale", slomoTime);

    }

    void OnDeath()
    {

        Destroy(gameObject);
    }
    void OnDestroy()
    {
        EnemyManager.instance.UnsubscribeEnemy(this);
        isDeath = true;
        DeadBody.transform.SetParent(null);
        DeadBody.SetActive(true);
    }
}
