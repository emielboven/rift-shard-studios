﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatistics : MonoBehaviour {

    public string playerName;
    public int kills;
    public int assists;
    public int deaths;

}
