﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInterface2 : MonoBehaviour {

    void Start()
    {
        FindObjectOfType<Events2>().playerDeathEvent += OnPlayerDeath;
    }

    public void OnPlayerDeath()
    {
        FindObjectOfType<Events2>().playerDeathEvent -= OnPlayerDeath;
        print("Ik laat death UI zien");
    }

}
