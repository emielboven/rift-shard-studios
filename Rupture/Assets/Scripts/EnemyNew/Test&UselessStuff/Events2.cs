﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Stel dit is de Player class
public class Events2 : MonoBehaviour{

    public delegate void DeathDelegate();
    public event DeathDelegate playerDeathEvent;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)) {
            Die();
        }
    }

    void Die()
    {
        if (playerDeathEvent != null){
            playerDeathEvent();
        }
    }

}
