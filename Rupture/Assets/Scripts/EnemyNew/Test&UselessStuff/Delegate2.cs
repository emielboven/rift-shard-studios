﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delegate2 : MonoBehaviour {

    private delegate int ScoreDelegate(PlayerStatistics stats);
    private ScoreDelegate scoreDelegate;

    //Wordt past opgehaald bij andere comment!
    private int ScoreByKills(PlayerStatistics stats)
    {
        return stats.kills;
    }

    private void OnGameOver(PlayerStatistics[] allStats)
    {
    }

    private string GetTopScore(PlayerStatistics[] allStats, ScoreDelegate scoreDelegate)
    {
        string name = "";
        int topScore = 0;
        foreach (PlayerStatistics stats in allStats){
            //Hier haal je de waardes pas op!
            int score = scoreDelegate(stats);
            if(score > topScore){
                topScore = score;
                name = stats.name;
            }
        }
        return name;
    }

}
