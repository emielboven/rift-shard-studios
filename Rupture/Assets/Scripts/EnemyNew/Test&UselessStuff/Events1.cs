﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Stel dit is de Player class
public class Events1 : MonoBehaviour {

    private Achievements1 achievementsScr;
    private UserInterface1 userInterfaceScr;

    void Start()
    {
        achievementsScr = GetComponent<Achievements1>();
        userInterfaceScr = GetComponent<UserInterface1>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)){
            Die();
        }
    }

    void Die()
    {
        achievementsScr.OnPlayerDie();
        userInterfaceScr.OnPlayerDie();
    }
	
}
