﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delegate1 : MonoBehaviour {

    private delegate void CalcDelegate(int input);
    private event CalcDelegate onClickEvent;

	void Start () {
        onClickEvent += Calculation1;
        onClickEvent += Calculation2;

        if (onClickEvent != null){
            onClickEvent(5);
        }
        else{
            print("onClickEvent is empty");
        }
	}

    void Calculation1(int i)
    {
        print("Calculation 1 : " + i);
    }

    void Calculation2(int i)
    {
        print("Calculation 2 : " + i * 2);
    }
}
