﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dump : MonoBehaviour {
    [Header("Publics")]
    public LayerMask obstacleLayermask;
    public Mesh meshStuff;
    public Transform player;
    public float fovDegrees = 60;
    public float fovRange = 15;

    [Header("PublicPrivate")]
    public Vector3 dirToPlayer;
    public bool canSeePlayer;
    public bool isInFov;
    public float currentDist;
    public float currentAngle;

    public float outerAngle(Vector3 vectorToPlayer)
    {
        float angle = Vector3.Angle(transform.forward, vectorToPlayer);
        currentAngle = angle; //QQQ:
        return angle;
    }

    void Update()
    {
        if (player != null)
        {
            currentDist = Vector3.Distance(transform.position, player.position);
            dirToPlayer = player.position - transform.position;
            float angle = outerAngle(dirToPlayer);

            if (angle < (fovDegrees / 2))
            {
                isInFov = true;
            }
            else
            {
                isInFov = false;
            }

            //Zit hij in het fov field (juiste hoek) EN zit hij in de range van de fov?
            if (isInFov && currentDist < fovRange)
            {
                //Zit er niks met de layer obstacleLM tussen de enemy en de speler?
                Ray rayToPlayer = new Ray(transform.position, dirToPlayer);
                if (!Physics.Raycast(rayToPlayer, fovRange, obstacleLayermask))
                {
                    canSeePlayer = true;
                }
                else
                {
                    canSeePlayer = false;
                }
            }
            else
            {
                canSeePlayer = false;
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (player != null)
        {
            if (canSeePlayer)
            {
                Gizmos.color = Color.green;
            }
            else
            {
                Gizmos.color = Color.red;
            }
            Gizmos.DrawLine(transform.position, player.position);
        }
    }
}
