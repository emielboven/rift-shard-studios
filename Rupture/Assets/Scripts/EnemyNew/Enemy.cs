﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{

    private NavMeshAgent agent;

    public enum State
    {
        idle,
        engage,
        investigate,
        attack,
        disengage
    }
    public State myState;

    public Transform[] waypoints;
    public bool walkingToWp;

    public bool finishedWaiting;

    public bool isInRotate;
    public bool isInIdle;
    public bool goRotate;

    //public bool rotatedToTarget;
    public int rotationSpeed = 10;
    public int target = -1;
    public float agentSpeed;
    public float waitTime;
    public float waitTimeMin = 1.5f;
    public float waitTimeMax = 3.0f;

    public float awarenessRange;
    private float distanceToWp;
    private float outOfSightTimer;

    private Animator anim;
    private Material markerMaterial;

    public Vector3 investigationTarget;
    public Transform attackTarget;
    private FieldOfView fov;

    void Start()
    {
        fov = GetComponent<FieldOfView>();
        agent = GetComponent<NavMeshAgent>();
        anim = transform.GetChild(0).GetComponent<Animator>();
        myState = State.idle;
        markerMaterial = transform.GetChild(1).GetComponent<Renderer>().material;
        markerMaterial.color = Color.black;
    }

    void Update()
    {
        //kijk naar states
        if (myState == State.idle)
        {
            Patrol();
            markerMaterial.color = Color.black;
            anim.SetLayerWeight(1, 0);
        }
        if (myState == State.investigate)
        {
            Investigate();
            markerMaterial.color = Color.yellow;
            agent.SetDestination(investigationTarget);
            anim.SetLayerWeight(1, 0);
        }
        if (myState == State.engage)
        {
            anim.SetBool("isWalking", true);
            goRotate = true;
            isInIdle = false;
            markerMaterial.color = Color.red;
            agent.SetDestination(attackTarget.position);
            anim.SetLayerWeight(1, 1);
            agent.speed = 3.5f;
        }
    }

    public int MoveToNextTarget()
    {
        if (target == -1)
        {
            target = 0;
        }
        else if (target < waypoints.Length - 1 && target > -1)
        {
            target++;
        }
        else
        {
            target = 0;
        }
        return target;
    }

    public void Patrol()
    {
        distanceToWp = Vector3.Distance(agent.destination, transform.position);

        if (distanceToWp <= 1f && !isInIdle)
        {
            agent.SetDestination(waypoints[MoveToNextTarget()].position);
            Idle();
        }


    }

    public void Investigate()
    {
        if (fov.canHearPlayer)
        {
            distanceToWp = Vector3.Distance(agent.destination, transform.position);

            if (distanceToWp <= 1f)
            {
                StartCoroutine(LookAround());
            }
        }
        else
        {
            myState = Enemy.State.idle;
        }


    }

    public void Engage()
    {

        if (fov.canSeePlayer)
        {
            outOfSightTimer = 0;
            investigationTarget = attackTarget.position;
            distanceToWp = Vector3.Distance(agent.destination, transform.position);

            if (distanceToWp <= 1f)
            {
                StartCoroutine(LookAround());
            }
        }
        else
        {
            outOfSightTimer += 1 * Time.deltaTime;
            if (outOfSightTimer >= 1)
            {
                myState = Enemy.State.investigate;
            }

        }


    }

    public void Idle()
    {
        isInIdle = true;
        StartCoroutine(WaitAtWayPoint());
    }

    public IEnumerator WaitAtWayPoint()
    {
        anim.SetBool("isWalking", false);
        agent.speed = 0;
        waitTime = Random.Range(waitTimeMin, waitTimeMax);
        yield return new WaitForSeconds(waitTime);
        anim.SetBool("isWalking", true);
        agent.speed = agentSpeed;
        isInIdle = false;
    }

    public IEnumerator LookAround()
    {
        anim.SetBool("isWalking", false);
        agent.speed = 0;
        waitTime = Random.Range(waitTimeMin*2, waitTimeMax*2);
        yield return new WaitForSeconds(waitTime);
        myState = Enemy.State.idle;
        anim.SetBool("isWalking", true);
        agent.speed = agentSpeed;

    }





}
