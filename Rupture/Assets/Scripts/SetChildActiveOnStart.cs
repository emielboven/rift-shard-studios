﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetChildActiveOnStart : MonoBehaviour {

    public bool isEnemy;
    private GameObject childObj;

	void Awake () {
        if (isEnemy)
        {
            childObj = transform.GetChild(3).gameObject;
            childObj.SetActive(true);
        }
        else
        {
            childObj = transform.GetChild(0).gameObject;
            childObj.SetActive(true);
        }
	}

}
