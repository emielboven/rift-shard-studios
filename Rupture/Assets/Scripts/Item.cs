﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Item {
	public string itemName;
	public int itemID;
	public string itemDesc;
	public Sprite itemIcon;
	public int itemPower;
	public int itemPower2;
	public int itemAmount;
	public ItemType itemType;

	public enum ItemType {
		Weapon,
		Consumable,
		Mask,
		Throwable,
		Shield,
		Amulet
	}
	public Item (string name, int id, string desc, int power, int power2, ItemType type)
	{
		itemName = name;
		itemID = id;
		itemDesc = desc;
		itemIcon = Resources.Load<Sprite>("ItemIcons/S_Item_" + name);
		itemPower = power;
		itemPower2 = power2;
		itemType = type;
		itemAmount = 1;

	}
		
}
