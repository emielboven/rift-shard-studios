﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlammableGenerator : FlammableScript {

	private Transform burnedGenerator;

	public override void Start(){
		base.Start();
		addFireScript = false;
		burnedGenerator = transform.GetChild(0);
	}

	protected override void SetFire(){
		base.SetFire();
		Invoke("DestroyMe", burnTime);	
	}

	private void DestroyMe(){
		burnedGenerator.SetParent(null);
		burnedGenerator.gameObject.SetActive(true);
		Destroy(gameObject);
	}

}
