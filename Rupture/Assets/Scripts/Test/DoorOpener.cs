﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpener : MonoBehaviour
{

    public Transform door;
    private DoorStats doorStatsScr;

    private void Start()
    {
        doorStatsScr = door.GetComponent<DoorStats>();
    }

    void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            doorStatsScr.goUp = true;
        }
    }

    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (doorStatsScr.permanentlyOpen != true)
            {
                doorStatsScr.goUp = false;
            }
        }
    }

    //void OnTriggerStay(Collider other)
    //{
    //    if (other.gameObject.tag == "Enemy")
    //    {
    //        doorStatsScr.goUp = true;
    //    }
    //}

    //void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.tag == "Enemy")
    //    {
    //        if (doorStatsScr.permanentlyOpen != true)
    //        {
    //            doorStatsScr.goUp = false;
    //        }
    //    }
    //}

}
