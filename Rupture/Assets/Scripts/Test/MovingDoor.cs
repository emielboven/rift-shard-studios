﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingDoor : MonoBehaviour {

    private bool goMove;

    public float yMax;
    public float yMin;
    public float downSpeed;
    public float upSpeed;
    public float upTime = 1f;
    public float downTime = 1f;

    void Start () {
        StartCoroutine(MovingThing());
	}

    void Update()
    {
        if(goMove = true)
        {
            StartCoroutine(MovingThing());
        }
    }

    IEnumerator MovingThing()
    {
        goMove = false;
        while (transform.position.y < yMax) {
            GoUp();
        }
        yield return null;
        Vector3 doorPos = new Vector3(transform.position.x, yMax, transform.position.z);
        transform.position = doorPos;
        yield return new WaitForSeconds(upTime);
        while (transform.position.y > yMin){
            GoDown();
        }
        yield return null;
        doorPos = new Vector3(transform.position.x, yMax, transform.position.z);
        transform.position = doorPos;
        yield return new WaitForSeconds(downTime);
        goMove = true;
    }

    void GoUp()
    {
        transform.Translate(new Vector3(0, 1, 0) * upSpeed * Time.deltaTime);
    }

    void GoDown()
    {
        transform.Translate(new Vector3(0, -1, 0) * downSpeed * Time.deltaTime);
    }

}
