﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportToPoint : MonoBehaviour {

    public TeleportPoints pointsScr;
    public int teleportID = -1;

    private GameObject camY;

	void Start () { 
        camY = GameObject.Find("CamY");
	}

    void Update()
    {
        if (pointsScr == null)
        {
            pointsScr = FindObjectOfType<TeleportPoints>();
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Minus) && teleportID > 0)
            {
                teleportID--;
                Teleport();
            }
            if (Input.GetKeyDown(KeyCode.Equals) && teleportID < (pointsScr.startPoints.Length - 1))
            {
                teleportID++;
                Teleport();
            }
        }
    }

    void Teleport()
    {
        transform.position = pointsScr.startPoints[teleportID].transform.position;
        transform.rotation = pointsScr.startPoints[teleportID].transform.rotation;
        camY.transform.rotation = transform.rotation;
    }
}
