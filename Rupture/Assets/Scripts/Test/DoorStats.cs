﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DoorStats : MonoBehaviour {

    private AudioSource audioS;
    private bool canSnap;
    private float slomoTime = 1f;

    public AudioClip clip;
    public bool goUp;
    public bool isSlowed;

    [Header("Door Stats")]
    public float yMin;
    public float yMax;
    public float upSpeed;
    public float downSpeed;
    public bool permanentlyOpen;

    void Start(){
        if(GetComponent<AudioSource>() != null){
            audioS = GetComponent<AudioSource>();
            print("Hij vind hem wel");
            audioS.PlayOneShot(clip);
            //audioS.Play();
        }
        
    }

    void Update()
    {
        if(audioS)
            Debug.Log(audioS.isPlaying);
        if (isSlowed && slomoTime != 0.3f)
        {
            slomoTime = 0.3f;
        }
        else if (!isSlowed && slomoTime != 1f)
        {
            slomoTime = 1f;
        }

        if (goUp)
        {
            if (transform.position.y < yMax)
            {
                canSnap = true;
                transform.Translate(new Vector3(0, 1, 0) * upSpeed * slomoTime * Time.deltaTime);
            }
            else
            {
                if(canSnap)
                {
                    if(audioS != null){
                        print("Hij hoort iets te doen!");
                        audioS.Play();
                    }
                    canSnap = false;
                    Vector3 topPosition = new Vector3(transform.position.x, yMax, transform.position.z);
                    transform.position = topPosition;
                }
            }
        }
        else
        {
            if (transform.position.y > yMin)
            {
                canSnap = true;
                transform.Translate(new Vector3(0, -1, 0) * downSpeed * slomoTime * Time.deltaTime);
            }
            else
            {
                if(canSnap)
                {
                     if(audioS != null){
                        audioS.Play();
                     }
                    canSnap = false;
                    Vector3 downPosition = new Vector3(transform.position.x, yMin, transform.position.z);
                    transform.position = downPosition;
                }
            }
        }
    }
}
