﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPoints : MonoBehaviour {

    public GameObject[] startPoints;

    void Start()
    {
        startPoints = new GameObject[transform.childCount];
        for(int i = 0; i < transform.childCount; i++)
        {
            startPoints[i] = transform.GetChild(i).gameObject;
        }
    }

}
