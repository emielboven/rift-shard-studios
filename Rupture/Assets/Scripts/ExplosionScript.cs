﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionScript : MonoBehaviour
{

    private GameObject explosionFX;
    public float explosionRadius = 2;
    public float damage = 20;
    public LayerMask mask;

    // Use this for initialization
    void Start()
    {
        //AkSoundEngine.PostEvent("Explosion", gameObject);
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius, mask);
        foreach (Collider collider in colliders)
        {
            if (collider.gameObject.tag == ("Enemy"))
            {
                collider.gameObject.GetComponent<EnemyAI>().OnHit(damage, false);
            }
            if (collider.gameObject.name == ("Player"))
            {
                collider.gameObject.GetComponent<PlayerController>().OnHit(damage);
            }


        }

         foreach (EnemyAI enemy in EnemyManager.instance.enemies){
             enemy.ExplosionTarget(transform.position);
         }
        Destroy(gameObject.transform.parent.gameObject, 2f);
        StartCoroutine(EndExplosion());


    }


    IEnumerator EndExplosion()
    {
        yield return new WaitForFixedUpdate();
        Destroy(transform.GetChild(0).gameObject);
    }


}
