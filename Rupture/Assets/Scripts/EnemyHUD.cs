﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class EnemyHUD : MonoBehaviour
{

    private Camera c;
    private Transform parentPos;
    public Renderer rend;
    public float yOffset = 1.5f;
    public EnemyStats stats;

    public Image healthbar;
    public Image healthbarBack;
    private Coroutine healthCoRoutine;

    public GameObject hud;
    public bool showHUD;
    // Use this for initialization
    void Start()
    {
        c = Camera.main;
        parentPos = transform.parent.parent;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (c != null)
        {
            if (rend.isVisible && showHUD)
            {
                if (!hud.activeInHierarchy)
                {
                    hud.SetActive(true);
                }
                Vector2 pos = c.WorldToScreenPoint(parentPos.position + new Vector3(0, yOffset, 0));
                transform.position = pos;
            }
            else
            {
                if (hud.activeInHierarchy)
                {
                    hud.SetActive(false);
                }
            }

        } else {
            c = Camera.main;
        }
    }

    public void UpdateHealth()
    {
        healthbarBack.fillAmount = healthbar.fillAmount;
        healthbar.fillAmount = stats.health / stats.maxHealth;

        if (healthCoRoutine != null)
        {
            StopCoroutine(healthCoRoutine);
        }
        healthCoRoutine = StartCoroutine(HUDHealthCutoff());
    }

    IEnumerator HUDHealthCutoff()
    {
        yield return new WaitForSeconds(.5f);
        healthbarBack.fillAmount = healthbar.fillAmount;
    }


}
