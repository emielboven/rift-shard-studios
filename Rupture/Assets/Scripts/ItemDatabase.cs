﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour {
	public List<Item> items = new List<Item>();


	void Start()
	{	

		///Consumables 
		items.Add(new Item("Health Potion",000, "Drink this to restore your health.", 50,0, Item.ItemType.Consumable));

		///Weapons
		items.Add(new Item("Iron Longsword",100, "A sword made from iron.", 10,0, Item.ItemType.Weapon));
		items.Add(new Item("Warhammer",101, "A heavy warhammer only wieldable by those of a strong physique.", 15,0, Item.ItemType.Weapon));


		///Shields
		items.Add(new Item("Templar Shield",200, "A fine decorated shield", 80,20, Item.ItemType.Shield));
		
		///Masks
		items.Add(new Item("Mask of Nimble Feet",300, "Increased your stamina by 20%", 20,0, Item.ItemType.Mask));

		///Throwables
		items.Add(new Item("Shatter Dust Bomb",400, "A bomb that will explode on impact when thrown", 3,0, Item.ItemType.Throwable));

		///Amulets
		items.Add(new Item("Amulet of Azuhl",500, "A token of strenght. 20% bonus damage.", 3,0, Item.ItemType.Amulet));
	}
}
