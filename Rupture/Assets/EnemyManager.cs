﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{

    public static EnemyManager instance; //dit is de singleton
    public List<EnemyAI> enemies;
    public State state;

    private void Awake()
    {
        instance = this;
        state = State.wandering;
    }

    void Start()
    {
       // AkSoundEngine.PostEvent("Music", gameObject);
    }
    
    void Update()
    {
        foreach (EnemyAI enemy in enemies)
        {

            if (enemy.GetComponent<EnemyAI>().state == State.wandering)
            {
                state = State.wandering;
                //AkSoundEngine.SetState("Music_state", "Explore");
                break;
            }
            else if (enemy.GetComponent<EnemyAI>().state == State.alerted)
            {
                state = State.alerted;
                //AkSoundEngine.SetState("Music_state", "Alert");
                break;
            }
            else
            {
                state = State.aggro;
                //AkSoundEngine.SetState("Music_state", "Combat");
            }

        }
        if (enemies.Count == 0)
        {
            state = State.wandering;
           // AkSoundEngine.SetState("Music_state", "Explore");
        }

        float shortestDist = 0;

        foreach (EnemyAI enemy in enemies)
        {
            if(shortestDist != 0)
            {
                if (shortestDist > enemy.fov.currentDist)
                {
                    shortestDist = enemy.fov.currentDist;
                }
            }
            else
            {
                shortestDist = enemy.fov.currentDist;
            }
        }

        //AkSoundEngine.SetRTPCValue("Enemy_Distance", shortestDist);

    }


    public void SubscribeEnemy(EnemyAI en)
    {
        enemies.Add(en);
    }

    //moet worden aangeroepen als de enemy gedestroyd wordt
    public void UnsubscribeEnemy(EnemyAI en)
    {
        enemies.Remove(en);
    }

}
