﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeScript : MonoBehaviour {

public bool fadeIn;
public CanvasGroup cnvs;

public float fadeSpeed = 1f;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (!fadeIn && cnvs.alpha <1) {
			cnvs.alpha = Mathf.Lerp(cnvs.alpha, 1.5f, Time.deltaTime*fadeSpeed);
		}
		if (fadeIn && cnvs.alpha >0) {
			cnvs.alpha = Mathf.Lerp(cnvs.alpha, -0.5f, Time.deltaTime*fadeSpeed);
		}
	}
}
