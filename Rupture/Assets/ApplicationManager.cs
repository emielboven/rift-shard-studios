﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ApplicationManager : MonoBehaviour
{
    bool quit;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Return"))
        {
            quit = true;
        }
        if (quit)
        {
            var black = GameObject.Find("BlackScreen").GetComponent<FadeScript>();
            black.fadeIn = false;
            if (black.cnvs.alpha == 1)
            {
                SceneManager.LoadScene("Scene_Menu");

            }
        }
    }
}
