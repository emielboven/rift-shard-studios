﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    public bool goScene;
    public CanvasGroup canvas;
    public CanvasGroup canvas2;
    public EventSystem events;
    public GameObject FirstSelectedButton;
    private bool showButtons;

    public Button continueButton;
    private bool starting;
    // Use this for initialization
    void Start()
    {
        canvas.alpha = 1;
        canvas2.alpha = 0;
        canvas2.interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!starting)
        {
            if (canvas.alpha >= 0)
            {
                canvas.alpha = Mathf.Lerp(canvas.alpha, -1f, Time.deltaTime * 0.5f);
                if (canvas.alpha <= 0)
                {
                    showButtons = true;
                    if (GameObject.Find("Checkpoints") != null)
                    {
                        continueButton.interactable = true;
                    }
                    else
                    {
                        continueButton.interactable = false;
                    }
                }
            }

            if (canvas2.alpha < 1 && showButtons)
            {
                canvas2.alpha = Mathf.Lerp(canvas2.alpha, 2f, Time.deltaTime * 0.5f);
                if (canvas2.alpha >= 1)
                {
                    canvas2.interactable = true;
                    events.SetSelectedGameObject(FirstSelectedButton);
                }
            }

        }
        if (goScene)
        {
            canvas.alpha = Mathf.Lerp(canvas.alpha, 2f, Time.deltaTime * 0.5f);
            if (canvas.alpha >= 1)
            {
                SceneManager.UnloadSceneAsync("Scene_Menu");

            }
        }

    }

    public void StartGame()
    {
        if (!starting)
        {
            StartCoroutine(ShowScene());
            starting = true;
        }

    }

    public void StartNewGame()
    {
        if (GameObject.Find("Checkpoints") != null)
        { 
           Destroy(GameObject.Find("Checkpoints"));
        }
        if (!starting)
        {
            StartCoroutine(ShowScene());
            starting = true;
        }

    }

    public void QuitGame()
    {
        Application.Quit();
    }

    IEnumerator ShowScene()
    {
        goScene = true;
        yield return new WaitForSeconds(1.75f);
        SceneManager.LoadSceneAsync("Scene_Start", LoadSceneMode.Additive);
    }
}
