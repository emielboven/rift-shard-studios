﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavManager : MonoBehaviour
{
    public static NavManager instance; //dit is de singleton
    public List<NavMeshSurface> navmeshes;
    private Coroutine routine;
    
    void Awake()
    {
        instance = this;
    }

    public void Subscribe(NavMeshSurface nav)
    {
        navmeshes.Add(nav);
    }
    
    public void Unsubscribe(NavMeshSurface nav)
    {
        navmeshes.Remove(nav);
    }

    public void Rebuild()
    {
        if(routine != null){
            StopCoroutine(routine);
        }
        routine = StartCoroutine(RebuildDelay());
    }

    IEnumerator RebuildDelay()
    {
        yield return new WaitForSeconds(0.25f);
        foreach (NavMeshSurface navmesh in navmeshes)
        {
            navmesh.BuildNavMesh();
        }
        foreach (EnemyAI enemy in EnemyManager.instance.enemies)
        {
            enemy.Retarget();
        }
    }

}
