﻿
using UnityEngine;

public class CheckPoints : MonoBehaviour {

	public static CheckPoints Instance { get; private set; }

	public Vector3 checkPoint;
	public Quaternion checkPointRotation;
	public Quaternion camRotation;

	private void Awake()
	{DontDestroyOnLoad(gameObject);
		if (Instance == null) {
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else{
			Destroy(gameObject);
		}


	}



}
