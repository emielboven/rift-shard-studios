﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpThrowable : MonoBehaviour
{
    public LayerMask throwableLayer;

    public GameObject pickup;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        if (pickup == null)
        {
            if (Physics.CheckSphere(transform.position + transform.TransformDirection(new Vector3(0, 0, 0.3f)), 0.5f, throwableLayer))
            {
                if (Input.GetButtonDown("PickUpThrowable"))
                {
                    pickup = Physics.OverlapSphere(transform.position + transform.TransformDirection(new Vector3(0, 0, 0.3f)), 0.5f, throwableLayer)[0].gameObject;
                    pickup.transform.SetParent(this.transform);
                    pickup.GetComponent<Rigidbody>().isKinematic = true;
                    pickup.transform.position = transform.position + transform.TransformDirection(new Vector3(0, 0, 1.5f));
                    pickup.transform.rotation = transform.rotation;
                    pickup.layer = 8;
                }
            }
        }
        else if (Input.GetButtonDown("PickUpThrowable"))
        {
            pickup.transform.SetParent(null);
            pickup.GetComponent<Rigidbody>().isKinematic = false;
            pickup.layer = 14;
            pickup = null;

        }
    }
}
