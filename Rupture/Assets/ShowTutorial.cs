﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowTutorial : MonoBehaviour {

public CanvasGroup cnvs;
public bool fadeIn;
public float fadeSpeed = 1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (fadeIn && cnvs.alpha <1) {
			cnvs.alpha = Mathf.Lerp(cnvs.alpha, 1.5f, Time.deltaTime*fadeSpeed);
		}
		if (!fadeIn && cnvs.alpha >0) {
			cnvs.alpha = Mathf.Lerp(cnvs.alpha, -0.5f, Time.deltaTime*fadeSpeed);
		}
	}

	void OnTriggerEnter (Collider other){
		if (other.GetComponent<PlayerController>() != null) {
			fadeIn = true;
		}

	}

	void OnTriggerStay (Collider other){
		if (other.GetComponent<PlayerController>() != null && fadeIn) {
			fadeIn = true;
		}

	}

	void OnTriggerExit (Collider other){
		if (other.GetComponent<PlayerController>() != null) {
			fadeIn = false;
		}

	}
}
