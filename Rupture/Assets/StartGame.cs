﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{

    public CanvasGroup canvas;
    public string[] scenes;
    private Coroutine go;

    public bool goScene;
    // Use this for initialization
    void Start()
    {
        SceneManager.LoadSceneAsync("Scene_Lighting", LoadSceneMode.Additive);
        SceneManager.LoadSceneAsync("Scene_Core", LoadSceneMode.Additive);

        foreach (string scene in scenes){
             SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
        }
    }

    // Update is called once per frame
    void Update()
    {  

        if (SceneManager.GetSceneByName("Scene_Lighting").isLoaded)
        {
            StartCoroutine(ShowScene());
			SceneManager.SetActiveScene(SceneManager.GetSceneByName("Scene_Lighting"));

            if (goScene)
            {
               
                GameObject.Find("BlackScreen").GetComponent<FadeScript>().fadeIn = true;
                GameObject.Find("BlackScreen").GetComponent<FadeScript>().cnvs.alpha = 1;
                SceneManager.UnloadSceneAsync("Scene_Start");
                
            }
        }
    }

    IEnumerator ShowScene()
    {
        yield return new WaitForSeconds(1.5f);
        goScene = true;
    }
}
