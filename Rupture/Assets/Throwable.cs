﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throwable : MonoBehaviour
{


    public GameObject pieces;
    public GameObject fx;
    public bool canBreak;

    public float damage = 10;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
            if (gameObject.tag == "Fire") {
                gameObject.layer =8;
            }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (canBreak)
        {
            gameObject.layer = 14;
            canBreak = false;
            GetComponent<TrailRenderer>().enabled = false;
            //Explode(pieces.transform.position, 2, 1);
            if (other.gameObject.layer == 8)
            {
                other.gameObject.GetComponentInParent<PlayerController>().OnHit(damage);
                
            }

        }
    }

    public void Explode(Vector3 center, float force, float radius)
    {
        fx.transform.SetParent(null);
        fx.SetActive(true);
        pieces.transform.SetParent(null);
        pieces.SetActive(true);
        Rigidbody[] rbs = pieces.transform.GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in rbs)
        {
            
            rb.AddExplosionForce(force, center, radius, 1   , ForceMode.Impulse);
        }
        Destroy(gameObject);
    }


}
