// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SH_Water"
{
	Properties
	{
		_TessValue( "Max Tessellation", Range( 1, 32 ) ) = 8
		_TessMin( "Tess Min Distance", Float ) = 10
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_TessMax( "Tess Max Distance", Float ) = 25
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		_NoiseSize("Noise Size", Range( 0 , 1)) = 41.97272
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" }
		Cull Back
		CGPROGRAM
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Tessellation.cginc"
		#pragma target 4.6
		#pragma surface surf Standard alpha:fade keepalpha vertex:vertexDataFunc tessellate:tessFunction 
		struct Input
		{
			float2 uv_texcoord;
			float4 screenPos;
		};

		struct appdata
		{
			float4 vertex : POSITION;
			float4 tangent : TANGENT;
			float3 normal : NORMAL;
			float4 texcoord : TEXCOORD0;
			float4 texcoord1 : TEXCOORD1;
			float4 texcoord2 : TEXCOORD2;
			float4 texcoord3 : TEXCOORD3;
			fixed4 color : COLOR;
			UNITY_VERTEX_INPUT_INSTANCE_ID
		};

		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform sampler2D _CameraDepthTexture;
		uniform sampler2D _TextureSample1;
		uniform float _NoiseSize;
		uniform float _TessValue;
		uniform float _TessMin;
		uniform float _TessMax;

		float4 tessFunction( appdata v0, appdata v1, appdata v2 )
		{
			return UnityDistanceBasedTess( v0.vertex, v1.vertex, v2.vertex, _TessMin, _TessMax, _TessValue );
		}

		void vertexDataFunc( inout appdata v )
		{
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float2 appendResult33 = (float2(ase_worldPos.z , ase_worldPos.x));
			float2 panner30 = ( ( appendResult33 * _NoiseSize ) + 1.0 * _Time.y * float2( 0.2,-0.2 ));
			v.vertex.xyz += ( tex2Dlod( _TextureSample1, float4( panner30, 0, 1.0) ).b * float3(0,0.25,0) );
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 tex2DNode1 = tex2D( _TextureSample0, uv_TextureSample0 );
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float screenDepth11 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD(ase_screenPos))));
			float distanceDepth11 = abs( ( screenDepth11 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( 5.0 ) );
			float clampResult15 = clamp( ( distanceDepth11 + 0.5 ) , 0.0 , 1.0 );
			float4 lerpResult23 = lerp( ( float4(0.07212369,0.4264706,0.3678201,1) * tex2DNode1 ) , ( tex2DNode1 * float4(0,0.09051726,0.1544118,1) ) , clampResult15);
			float screenDepth16 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD(ase_screenPos))));
			float distanceDepth16 = abs( ( screenDepth16 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( 0.2 ) );
			float clampResult17 = clamp( distanceDepth16 , 0.0 , 1.0 );
			float ifLocalVar52 = 0;
			o.Albedo = ( lerpResult23 + ifLocalVar52 ).rgb;
			o.Metallic = 0.0;
			o.Smoothness = 1.0;
			o.Alpha = ( ifLocalVar52 + clampResult15 );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13701
144;95;1906;1004;1467.523;533.9238;1.441798;True;False
Node;AmplifyShaderEditor.WorldPosInputsNode;34;-1817.006,841.4988;Float;False;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.DepthFade;11;-937.9026,414.6997;Float;False;1;0;FLOAT;5.0;False;1;FLOAT
Node;AmplifyShaderEditor.DynamicAppendNode;33;-1544.504,877.6976;Float;False;FLOAT2;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.RangedFloatNode;36;-1452.905,1166.898;Float;False;Property;_NoiseSize;Noise Size;6;0;41.97272;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.DepthFade;16;-783.5027,273.3985;Float;False;1;0;FLOAT;0.2;False;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;3;-1116.099,-4.900042;Float;False;Constant;_Color0;Color 0;1;0;0,0.09051726,0.1544118,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;17;-601.3022,273.1984;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;-1312.905,999.8981;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0,0,0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.SamplerNode;1;-1113.1,-221.3001;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;24;-1133.006,-394.8024;Float;False;Constant;_Color1;Color 1;1;0;0.07212369,0.4264706,0.3678201,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;22;-704.1024,460.5988;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.5;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-646.605,-33.80231;Float;False;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.PannerNode;30;-1205.805,775.2977;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.2,-0.2;False;1;FLOAT;1.0;False;1;FLOAT2
Node;AmplifyShaderEditor.OneMinusNode;18;-428.6034,303.2987;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;4;-680.8999,-282.7;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.ClampOpNode;15;-554.1019,570.7991;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.LerpOp;23;-401.406,-225.6015;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.ConditionalIfNode;52;-248.6751,319.1971;Float;False;False;5;0;FLOAT;0.0;False;1;FLOAT;0.5;False;2;FLOAT;1.0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;26;-982.9045,697.6975;Float;True;Property;_TextureSample1;Texture Sample 1;3;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.Vector3Node;27;-801.7034,972.8966;Float;False;Constant;_Vector0;Vector 0;2;0;0,0.25,0;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-453.8046,801.2977;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT3;0;False;1;FLOAT3
Node;AmplifyShaderEditor.PannerNode;40;-1199.136,1320.196;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.2,-0.2;False;1;FLOAT;1.0;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;41;-1472,1456;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0,0,0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;49;-1102.302,1862.598;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0,0,0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.TextureCoordinatesNode;31;-1711.805,581.2977;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;20;58.79657,296.9996;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;38;-813.2039,1229.798;Float;True;Property;_TextureSample2;Texture Sample 2;1;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;47;-828.103,1449.198;Float;True;Property;_TextureSample3;Texture Sample 3;2;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ConditionalIfNode;43;-405.8036,1196.498;Float;False;False;5;0;FLOAT;0.0;False;1;FLOAT;0.2;False;2;FLOAT;1.0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;39;-1584,1712;Float;False;Property;_Float3;Float 3;5;0;41.97272;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;6;-29.5022,-248.7001;Float;False;Constant;_Float0;Float 0;2;0;0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;50;-1214.302,2118.598;Float;False;Property;_Float4;Float 4;4;0;41.97272;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;21;-171.7037,17.39841;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.PannerNode;51;-915.3901,1860.349;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.1,0.1;False;1;FLOAT;1.0;False;1;FLOAT2
Node;AmplifyShaderEditor.RangedFloatNode;7;-16.8024,-139.1001;Float;False;Constant;_Float1;Float 1;2;0;1;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;42;-209.8044,713.2982;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;48;-440.2032,1464.698;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;370,-1.907349E-06;Float;False;True;6;Float;ASEMaterialInspector;0;0;Standard;SH_Water;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;Back;0;0;False;0;0;Transparent;0.5;True;False;0;False;Transparent;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;True;0;8;10;25;False;0.5;True;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;33;0;34;3
WireConnection;33;1;34;1
WireConnection;17;0;16;0
WireConnection;35;0;33;0
WireConnection;35;1;36;0
WireConnection;22;0;11;0
WireConnection;25;0;1;0
WireConnection;25;1;3;0
WireConnection;30;0;35;0
WireConnection;18;0;17;0
WireConnection;4;0;24;0
WireConnection;4;1;1;0
WireConnection;15;0;22;0
WireConnection;23;0;4;0
WireConnection;23;1;25;0
WireConnection;23;2;15;0
WireConnection;52;0;18;0
WireConnection;26;1;30;0
WireConnection;29;0;26;3
WireConnection;29;1;27;0
WireConnection;40;0;41;0
WireConnection;41;0;33;0
WireConnection;41;1;39;0
WireConnection;49;0;33;0
WireConnection;49;1;50;0
WireConnection;20;0;52;0
WireConnection;20;1;15;0
WireConnection;38;1;40;0
WireConnection;47;1;51;0
WireConnection;43;0;48;0
WireConnection;21;0;23;0
WireConnection;21;1;52;0
WireConnection;51;0;49;0
WireConnection;42;0;52;0
WireConnection;42;1;43;0
WireConnection;48;0;38;4
WireConnection;48;1;47;1
WireConnection;0;0;21;0
WireConnection;0;3;6;0
WireConnection;0;4;7;0
WireConnection;0;9;20;0
WireConnection;0;11;29;0
ASEEND*/
//CHKSM=A6A4A31CBB4CE939ECB7B2B4A433C6E6E4FE0257