// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Rupture/SH_FogSheet"
{
	Properties
	{
		_Color("Color", Color) = (0,0,0,0)
		_DepthFade("DepthFade", Range( 0 , 100)) = 2
		_FogMask("FogMask", 2D) = "white" {}
		_DistanceFade("DistanceFade", Range( 0 , 10)) = 0.1
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit alpha:fade keepalpha noshadow nodynlightmap nodirlightmap nometa noforwardadd vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
			float2 texcoord_0;
			float4 screenPos;
			float3 worldPos;
		};

		uniform float4 _Color;
		uniform sampler2D _FogMask;
		uniform float4 _FogMask_ST;
		uniform sampler2D _TextureSample0;
		uniform sampler2D _CameraDepthTexture;
		uniform float _DepthFade;
		uniform float _DistanceFade;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			o.texcoord_0.xy = v.texcoord.xy * float2( 2,2 ) + float2( 0,0 );
		}

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			o.Emission = _Color.rgb;
			float2 uv_FogMask = i.uv_texcoord * _FogMask_ST.xy + _FogMask_ST.zw;
			float2 panner27 = ( i.texcoord_0 + 0.2 * _Time.y * float2( 0.1,0.1 ));
			float clampResult33 = clamp( ( tex2D( _TextureSample0, panner27 ).r + 0.4 ) , 0.0 , 1.0 );
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float screenDepth1 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD(ase_screenPos))));
			float distanceDepth1 = abs( ( screenDepth1 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( _DepthFade ) );
			float3 ase_worldPos = i.worldPos;
			float temp_output_11_0 = distance( _WorldSpaceCameraPos , ase_worldPos );
			float clampResult8 = clamp( ( distanceDepth1 * ( temp_output_11_0 / _DistanceFade ) ) , 0.0 , 1.0 );
			o.Alpha = ( ( tex2D( _FogMask, uv_FogMask ).r * clampResult33 ) * clampResult8 );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13701
7;29;1906;1004;1556.189;554.5771;1.3;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;28;-1055.293,-699.7972;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;2,2;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.PannerNode;27;-661.6927,-698.1973;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.1,0.1;False;1;FLOAT;0.2;False;1;FLOAT2
Node;AmplifyShaderEditor.WorldPosInputsNode;14;-933,351;Float;False;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.WorldSpaceCameraPos;12;-967,205;Float;False;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;21;-693,498;Float;False;Property;_DistanceFade;DistanceFade;3;0;0.1;0;10;0;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;26;-279.2924,-653.3973;Float;True;Property;_TextureSample0;Texture Sample 0;4;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.DistanceOpNode;11;-670,231;Float;True;2;0;FLOAT3;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;3;-938,79;Float;False;Property;_DepthFade;DepthFade;1;0;2;0;100;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;32;112.7074,-445.3976;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.4;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;35;-328.9891,423.0229;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.DepthFade;1;-626,116;Float;False;1;0;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;6;-1120.2,-412.8;Float;True;Property;_FogMask;FogMask;2;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;33;373.5073,-416.5975;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-235.5254,171.8312;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;8;-131.9,176.2;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-295.2926,-322.1975;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;2;-689,-155;Float;False;Property;_Color;Color;0;0;0,0,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-348.0001,269.1998;Float;False;3;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;131.9,156.5;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;346,-22;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;Rupture/SH_FogSheet;False;False;False;False;False;False;False;True;True;False;True;True;False;False;True;False;False;Off;0;0;False;0;0;Transparent;0.5;True;False;0;False;Transparent;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;False;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;14;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;27;0;28;0
WireConnection;26;1;27;0
WireConnection;11;0;12;0
WireConnection;11;1;14;0
WireConnection;32;0;26;1
WireConnection;35;0;11;0
WireConnection;35;1;21;0
WireConnection;1;0;3;0
WireConnection;33;0;32;0
WireConnection;34;0;1;0
WireConnection;34;1;35;0
WireConnection;8;0;34;0
WireConnection;29;0;6;1
WireConnection;29;1;33;0
WireConnection;20;0;11;0
WireConnection;20;1;21;0
WireConnection;20;2;1;0
WireConnection;25;0;29;0
WireConnection;25;1;8;0
WireConnection;0;2;2;0
WireConnection;0;9;25;0
ASEEND*/
//CHKSM=8F9169917B25298EE784EDFD76478E8B920DB60D