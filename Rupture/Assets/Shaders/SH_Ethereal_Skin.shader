// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SH_Ethereal_Skin"
{
	Properties
	{
		_skinEdges("skinEdges", Color) = (0.2279412,0.2235025,0.2178849,0)
		_skin("skin", Color) = (0.2279412,0.2235025,0.2178849,0)
		_Color0("Color 0", Color) = (0.2573529,1,0.741,0)
		_TextureSample0("Texture Sample 0", 2D) = "bump" {}
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		_TextureSample2("Texture Sample 2", 2D) = "white" {}
		_TextureSample3("Texture Sample 3", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform float4 _skin;
		uniform float4 _skinEdges;
		uniform sampler2D _TextureSample3;
		uniform float4 _TextureSample3_ST;
		uniform sampler2D _TextureSample1;
		uniform float4 _TextureSample1_ST;
		uniform float4 _Color0;
		uniform sampler2D _TextureSample2;
		uniform float4 _TextureSample2_ST;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			o.Normal = UnpackNormal( tex2D( _TextureSample0, uv_TextureSample0 ) );
			float2 uv_TextureSample3 = i.uv_texcoord * _TextureSample3_ST.xy + _TextureSample3_ST.zw;
			float4 tex2DNode30 = tex2D( _TextureSample3, uv_TextureSample3 );
			float4 lerpResult29 = lerp( _skin , _skinEdges , tex2DNode30.r);
			o.Albedo = lerpResult29.rgb;
			float2 uv_TextureSample1 = i.uv_texcoord * _TextureSample1_ST.xy + _TextureSample1_ST.zw;
			float mulTime17 = _Time.y * 2.0;
			float temp_output_20_0 = ( ( sin( mulTime17 ) * 0.6 ) + 0.7 );
			float2 uv_TextureSample2 = i.uv_texcoord * _TextureSample2_ST.xy + _TextureSample2_ST.zw;
			o.Emission = ( ( ( tex2D( _TextureSample1, uv_TextureSample1 ) * temp_output_20_0 ) * _Color0 ) + ( tex2D( _TextureSample2, uv_TextureSample2 ) * temp_output_20_0 ) ).rgb;
			o.Metallic = 0.0;
			o.Smoothness = ( ( 1.0 - tex2DNode30.r ) * 0.1 );
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13701
-1913;29;1906;1004;2135.595;419.1593;1.451118;True;False
Node;AmplifyShaderEditor.SimpleTimeNode;17;-1903.559,1033.323;Float;False;1;0;FLOAT;2.0;False;1;FLOAT
Node;AmplifyShaderEditor.SinOpNode;19;-1670.929,1029.732;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;-1468.911,989.9397;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.6;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;20;-1258.709,928.7217;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.7;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;13;-1277.57,585.834;Float;True;Property;_TextureSample1;Texture Sample 1;3;0;Assets/Textures/T_Enemy_Fighter_Emmision.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;25;-1400.403,1261.214;Float;True;Property;_TextureSample2;Texture Sample 2;4;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;30;-954.6567,-36.17395;Float;True;Property;_TextureSample3;Texture Sample 3;5;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;16;-692.4913,1089.007;Float;False;Property;_Color0;Color 0;2;0;0.2573529,1,0.741,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;-976.673,823.5048;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.ColorNode;2;-777,-440;Float;False;Property;_skin;skin;1;0;0.2279412,0.2235025,0.2178849,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;31;-806.6567,-241.174;Float;False;Property;_skinEdges;skinEdges;0;0;0.2279412,0.2235025,0.2178849,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;7;-566.4511,585;Float;False;Constant;_Float1;Float 1;0;0;0.1;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-1015.403,1189.214;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;14;-624.36,820.6299;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.OneMinusNode;34;-489.1489,317.3797;Float;True;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.LerpOp;29;-341.6567,-64.17395;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.RangedFloatNode;6;-871,331;Float;False;Constant;_Float0;Float 0;0;0;0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;27;-427.403,808.2145;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;-288.1489,362.3797;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;12;-571.1283,67.59659;Float;True;Property;_TextureSample0;Texture Sample 0;2;0;Assets/Textures/T_Enemy_Fighter_normals.png;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;247,-27;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;SH_Ethereal_Skin;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;19;0;17;0
WireConnection;21;0;19;0
WireConnection;20;0;21;0
WireConnection;22;0;13;0
WireConnection;22;1;20;0
WireConnection;26;0;25;0
WireConnection;26;1;20;0
WireConnection;14;0;22;0
WireConnection;14;1;16;0
WireConnection;34;0;30;1
WireConnection;29;0;2;0
WireConnection;29;1;31;0
WireConnection;29;2;30;1
WireConnection;27;0;14;0
WireConnection;27;1;26;0
WireConnection;35;0;34;0
WireConnection;35;1;7;0
WireConnection;0;0;29;0
WireConnection;0;1;12;0
WireConnection;0;2;27;0
WireConnection;0;3;6;0
WireConnection;0;4;35;0
ASEEND*/
//CHKSM=0AFA4EC3BF2ED93FEABD9A497B3965B082924D76