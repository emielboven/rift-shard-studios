// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ProceduralSky"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_skyColor("skyColor", Color) = (0.2502163,0.3823529,0.3495466,0)
		_HorizonColor("HorizonColor", Color) = (0,0,0,0)
		_CloudTexture("Cloud Texture", 2D) = "white" {}
		_CloudTiling("Cloud Tiling", Vector) = (2,2,0,0)
		_CloudColor("Cloud Color", Color) = (0,0,0,0)
		_CloudShadowColor("Cloud Shadow Color", Color) = (0,0,0,0)
		_CloudShadowIntensity("CloudShadow Intensity", Range( 0 , 0.5)) = 0.15
		_NoiseTexture("NoiseTexture", 2D) = "white" {}
		_NoiseTiling("Noise Tiling", Float) = 3
		_NoiseIntensity("Noise Intensity", Float) = 1
		_CloudBlend("Cloud Blend", Float) = 0
		_MaskClipValue( "Mask Clip Value", Float ) = 0.5
	}

	SubShader
	{
		Tags{ "RenderType" = "Background"  "Queue" = "Background+0" "IsEmissive" = "true"  }
		Cull Back
		ZWrite Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog nometa noforwardadd 
		struct Input
		{
			float3 worldPos;
		};

		uniform float4 _HorizonColor;
		uniform sampler2D _CloudTexture;
		uniform sampler2D _NoiseTexture;
		uniform float _NoiseTiling;
		uniform float _NoiseIntensity;
		uniform float2 _CloudTiling;
		uniform float _CloudBlend;
		uniform float4 _CloudColor;
		uniform float4 _CloudShadowColor;
		uniform float _CloudShadowIntensity;
		uniform float4 _skyColor;
		uniform float _MaskClipValue = 0.5;

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 temp_output_320_0 = normalize( ase_worldPos );
			float componentMask26 = temp_output_320_0.y;
			float temp_output_30_0 = ( 1.0 - clamp( abs( componentMask26 ) , 0.0 , 1.0 ) );
			float2 componentMask70 = temp_output_320_0.xy;
			float2 temp_cast_0 = (( tex2D( _NoiseTexture, (abs( ( componentMask70 * _NoiseTiling )+_Time.y * float2(0.01,0.005 ))) ).r * _NoiseIntensity )).xx;
			float4 tex2DNode76 = tex2D( _CloudTexture, (abs( ( temp_cast_0 + ( componentMask70 * _CloudTiling ) )+_Time.y * float2(0.005,0 ))) );
			float componentMask194 = temp_output_320_0.z;
			float temp_output_371_0 = clamp( ( 1.0 - pow( ( 1.0 - componentMask194 ) , _CloudBlend ) ) , 0.0 , 1.0 );
			float temp_output_396_0 = ( _CloudBlend * 2.0 );
			float temp_output_233_0 = clamp( pow( ( temp_output_371_0 * temp_output_371_0 ) , temp_output_396_0 ) , 0.0 , 1.0 );
			float2 appendResult191 = float2( ( temp_output_320_0.z * -1.0 ) , temp_output_320_0.y );
			float2 temp_cast_1 = (( tex2D( _NoiseTexture, (abs( ( appendResult191 * _NoiseTiling )+_Time.y * float2(0.01,0.005 ))) ).r * _NoiseIntensity )).xx;
			float4 tex2DNode189 = tex2D( _CloudTexture, (abs( ( temp_cast_1 + ( appendResult191 * _CloudTiling ) )+_Time.y * float2(0.005,0 ))) );
			float componentMask172 = temp_output_320_0.x;
			float temp_output_369_0 = clamp( ( 1.0 - pow( ( 1.0 - componentMask172 ) , _CloudBlend ) ) , 0.0 , 1.0 );
			float temp_output_231_0 = clamp( pow( ( temp_output_369_0 * temp_output_369_0 ) , temp_output_396_0 ) , 0.0 , 1.0 );
			float2 appendResult359 = float2( ( temp_output_320_0.x * -1.0 ) , temp_output_320_0.y );
			float2 temp_cast_2 = (( tex2D( _NoiseTexture, (abs( ( appendResult359 * _NoiseTiling )+_Time.y * float2(0.01,0.005 ))) ).r * _NoiseIntensity )).xx;
			float4 tex2DNode344 = tex2D( _CloudTexture, (abs( ( temp_cast_2 + ( appendResult359 * _CloudTiling ) )+_Time.y * float2(0.005,0 ))) );
			float componentMask353 = temp_output_320_0.z;
			float temp_output_378_0 = clamp( ( 1.0 - pow( ( 1.0 - componentMask353 ) , ( _CloudBlend * -1.0 ) ) ) , 0.0 , 1.0 );
			float temp_output_348_0 = clamp( pow( ( temp_output_378_0 * temp_output_378_0 ) , temp_output_396_0 ) , 0.0 , 1.0 );
			float2 appendResult323 = float2( temp_output_320_0.z , temp_output_320_0.y );
			float2 temp_cast_3 = (( tex2D( _NoiseTexture, (abs( ( appendResult323 * _NoiseTiling )+_Time.y * float2(0.01,0.005 ))) ).r * _NoiseIntensity )).xx;
			float4 tex2DNode343 = tex2D( _CloudTexture, (abs( ( temp_cast_3 + ( appendResult323 * _CloudTiling ) )+_Time.y * float2(0.005,0 ))) );
			float componentMask352 = temp_output_320_0.x;
			float temp_output_373_0 = ( 1.0 - clamp( pow( ( 1.0 - componentMask352 ) , ( _CloudBlend * -1.0 ) ) , 0.0 , 1.0 ) );
			float temp_output_349_0 = clamp( pow( ( temp_output_373_0 * temp_output_373_0 ) , temp_output_396_0 ) , 0.0 , 1.0 );
			float temp_output_206_0 = clamp( ( ( ( tex2DNode76.r * temp_output_233_0 ) + ( tex2DNode189.r * temp_output_231_0 ) ) + ( ( tex2DNode344.r * temp_output_348_0 ) + ( tex2DNode343.r * temp_output_349_0 ) ) ) , 0.0 , 1.0 );
			float temp_output_311_0 = clamp( ( ( componentMask26 * pow( temp_output_30_0 , 3.0 ) ) * 5.0 ) , 0.0 , 1.0 );
			float3 ase_lightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			float temp_output_4_0 = dot( temp_output_320_0 , ase_lightDir );
			float temp_output_13_0 = ( 0.5 * ( 1.0 + temp_output_4_0 ) );
			float temp_output_15_0 = pow( temp_output_13_0 , 8.0 );
			float temp_output_219_0 = pow( temp_output_15_0 , 5.0 );
			float temp_output_305_0 = clamp( ( ( ( tex2DNode76.b * temp_output_233_0 ) + ( tex2DNode189.b * temp_output_231_0 ) ) + ( ( tex2DNode344.b * temp_output_348_0 ) + ( tex2DNode343.b * temp_output_349_0 ) ) ) , 0.0 , 1.0 );
			o.Emission = ( ( clamp( ( ( pow( temp_output_30_0 , 4.0 ) * _HorizonColor ) + ( ( ( ( temp_output_206_0 * temp_output_311_0 ) * _CloudColor ) + ( ( ( clamp( ( ( ( tex2DNode76.g * temp_output_233_0 ) + ( tex2DNode189.g * temp_output_231_0 ) ) + ( ( tex2DNode344.g * temp_output_348_0 ) + ( tex2DNode343.g * temp_output_349_0 ) ) ) , 0.0 , 1.0 ) * temp_output_219_0 ) + ( temp_output_206_0 * ( ( ( temp_output_219_0 * temp_output_219_0 ) * 0.3 ) * temp_output_305_0 ) ) ) * _LightColor0 ) ) - ( ( ( temp_output_311_0 * ( 1.0 - temp_output_305_0 ) ) * ( 1.0 - _CloudShadowColor ) ) * _CloudShadowIntensity ) ) ) , float4( 0,0,0,0 ) , float4( 1,1,1,0 ) ) + ( ( clamp( ( pow( clamp( temp_output_4_0 , 0.0 , 1.0 ) , exp( 10.0 ) ) * exp( 50.0 ) ) , 0.0 , 1.0 ) * ( 1.0 - pow( clamp( ( temp_output_206_0 + -0.1 ) , 0.0 , 1.0 ) , 0.001 ) ) ) * _LightColor0 ) ) + lerp( _skyColor , ( ( _LightColor0 * float4( 0.09558821,0.09558821,0.09558821,0 ) ) + _skyColor ) , ( temp_output_15_0 + temp_output_13_0 ) ) ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=10003
0;337;1157;801;6150.142;3943.438;2.5;True;False
Node;AmplifyShaderEditor.WorldPosInputsNode;319;-8408.399,-3115.074;Float;False;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.NormalizeNode;320;-8196.459,-3140.678;Float;False;1;0;FLOAT3;0,0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.BreakToComponentsNode;360;-7646.124,-1744.745;Float;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.BreakToComponentsNode;192;-7367.336,-3941.448;Float;False;FLOAT3;1;0;FLOAT3;0.0;False;16;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;361;-7400.777,-1741.744;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;-1.0;False;1;FLOAT
Node;AmplifyShaderEditor.BreakToComponentsNode;321;-7548.967,-906.6567;Float;False;FLOAT3;1;0;FLOAT3;0.0;False;16;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;315;-7068.663,-3808.44;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;-1.0;False;1;FLOAT
Node;AmplifyShaderEditor.CommentaryNode;201;-5673.99,-5649.17;Float;False;1760.605;393.1852;;8;172;366;368;235;369;173;393;231;ZY Mask;0;0
Node;AmplifyShaderEditor.CommentaryNode;317;-5780.922,-2659.98;Float;False;1760.605;393.1852;;9;356;352;349;347;362;373;372;374;391;ZY Mask;0;0
Node;AmplifyShaderEditor.CommentaryNode;316;-5748.203,-3086.741;Float;False;1652.742;309.2622;;8;357;353;346;363;375;377;378;392;XY Mask;0;0
Node;AmplifyShaderEditor.CommentaryNode;200;-5569.071,-6121.533;Float;False;1652.742;309.2622;eeee;7;234;194;197;367;370;371;394;XY Mask;0;0
Node;AmplifyShaderEditor.ComponentMaskNode;70;-7187.885,-4748.596;Float;True;True;True;False;True;1;0;FLOAT3;0,0,0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.AppendNode;323;-7103.069,-876.0566;Float;False;FLOAT2;0;0;0;0;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.AppendNode;191;-6923.936,-3910.848;Float;False;FLOAT2;0;0;0;0;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.RangedFloatNode;409;-8605.346,-3219.938;Float;False;Property;_NoiseTiling;Noise Tiling;8;0;3;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;193;-6078.627,-3596.264;Float;False;Property;_CloudBlend;Cloud Blend;10;0;0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.ComponentMaskNode;172;-5615.612,-5448.128;Float;False;True;False;False;False;1;0;FLOAT3;0,0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.ComponentMaskNode;353;-5698.203,-2853.64;Float;False;False;False;True;False;1;0;FLOAT3;0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.ComponentMaskNode;352;-5692.646,-2434.441;Float;False;True;False;False;False;1;0;FLOAT3;0,0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.AppendNode;359;-7198.625,-1773.344;Float;False;FLOAT2;0;0;0;0;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.ComponentMaskNode;194;-5548.969,-5912.929;Float;False;False;False;True;False;1;0;FLOAT3;0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;374;-5514.375,-2619.54;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;-1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleTimeNode;324;-8427.8,-3613.932;Float;False;1;0;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;266;-6879.663,-4975.397;Float;False;2;0;FLOAT2;0.0;False;1;FLOAT;3,3;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;358;-7114.288,-1015.904;Float;False;2;0;FLOAT2;0,0;False;1;FLOAT;3,3;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;325;-7058.796,-1940.606;Float;False;2;0;FLOAT2;0.0;False;1;FLOAT;3,3;False;1;FLOAT2
Node;AmplifyShaderEditor.OneMinusNode;367;-5267.973,-5966.438;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;362;-5451.368,-2458.847;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;267;-6935.156,-4050.695;Float;False;2;0;FLOAT2;0,0;False;1;FLOAT;3,3;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;375;-5567.668,-3044.645;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;-1.0;False;1;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;363;-5430.566,-2879.943;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;366;-5312.774,-5505.643;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.PowerNode;197;-5004.87,-6096.029;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;2.5;False;1;FLOAT
Node;AmplifyShaderEditor.PowerNode;356;-5250.391,-2568.454;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;2.5;False;1;FLOAT
Node;AmplifyShaderEditor.PannerNode;264;-6935.538,-4302.752;Float;True;0.01;0.005;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.PowerNode;357;-5239.902,-3021.14;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;2.5;False;1;FLOAT
Node;AmplifyShaderEditor.PowerNode;173;-5055.56,-5584.04;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;2.5;False;1;FLOAT
Node;AmplifyShaderEditor.PannerNode;246;-6708.172,-5176.177;Float;True;0.01;0.005;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.PannerNode;328;-6887.303,-2141.386;Float;True;0.01;0.005;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.PannerNode;327;-7114.67,-1267.961;Float;True;0.01;0.005;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.TexturePropertyNode;268;-7225.263,-3157.194;Float;True;Property;_NoiseTexture;NoiseTexture;7;0;None;False;white;Auto;0;1;SAMPLER2D
Node;AmplifyShaderEditor.SamplerNode;265;-6698.836,-4304.601;Float;True;Property;_TextureSample3;Texture Sample 3;5;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;368;-4821.375,-5587.145;Float;True;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;330;-6615.502,-2152.335;Float;True;Property;_TextureSample14;Texture Sample 14;5;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;410;-8633.849,-3337.336;Float;False;Property;_NoiseIntensity;Noise Intensity;9;0;1;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;331;-6877.967,-1269.81;Float;True;Property;_TextureSample15;Texture Sample 15;5;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;370;-4772.873,-6100.045;Float;True;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;377;-4932.068,-2954.843;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.Vector2Node;204;-7050.38,-2371.139;Float;False;Property;_CloudTiling;Cloud Tiling;3;0;2,2;0;3;FLOAT2;FLOAT;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;372;-4984.973,-2552.689;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;245;-6436.37,-5187.126;Float;True;Property;_TextureSample1;Texture Sample 1;5;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;188;-6266.879,-4097.498;Float;False;2;0;FLOAT2;0,0;False;1;FLOAT2;3,3;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;251;-6131.063,-5093.922;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.05;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;378;-4726.668,-2976.944;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;369;-4634.173,-5562.843;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;23;-1619.899,10.29773;Float;False;1;0;FLOAT;0.0;False;1;FLOAT3
Node;AmplifyShaderEditor.OneMinusNode;373;-4770.175,-2574.292;Float;True;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;333;-6310.195,-2059.131;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.05;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;335;-6430.301,-1670.802;Float;False;2;0;FLOAT2;0,0;False;1;FLOAT2;3,3;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;84;-6251.17,-4705.593;Float;False;2;0;FLOAT2;0,0;False;1;FLOAT2;3,3;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;334;-6446.01,-1062.707;Float;False;2;0;FLOAT2;0,0;False;1;FLOAT2;3,3;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;263;-6388.925,-4267.199;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.05;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;371;-4585.671,-6074.443;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;336;-6568.057,-1232.408;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.05;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;339;-6081.994,-1680.133;Float;False;2;0;FLOAT;0.0;False;1;FLOAT2;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.DotProductOpNode;4;-1247.401,-247.8996;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;347;-4606.009,-2576.843;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;396;-4579.77,-4630.846;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;2.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;235;-4489.076,-5577.33;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;259;-5936.868,-4112.711;Float;False;2;0;FLOAT;0.0;False;1;FLOAT2;0;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;234;-4445.376,-6085.234;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;12;-1582.062,-517.7806;Float;False;Constant;_Float1;Float 1;0;0;1;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;337;-6116,-1077.92;Float;False;2;0;FLOAT;0.0;False;1;FLOAT2;0;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleTimeNode;261;-6026.57,-3882.911;Float;False;1;0;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleTimeNode;338;-6205.701,-848.1196;Float;False;1;0;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;250;-5902.863,-4714.924;Float;False;2;0;FLOAT;0.0,0;False;1;FLOAT2;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;346;-4570.408,-3027.545;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ComponentMaskNode;26;-2044.064,-1279.971;Float;True;False;True;False;True;1;0;FLOAT3;0,0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;11;-1331.518,-489.5009;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.PowerNode;391;-4397.469,-2564.946;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.1;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;14;-1396.102,-627.601;Float;False;Constant;_Float2;Float 2;0;0;0.5;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.PowerNode;393;-4268.27,-5563.852;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.PowerNode;392;-4287.061,-3030.846;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.1;False;1;FLOAT
Node;AmplifyShaderEditor.PannerNode;260;-5761.967,-3932.512;Float;True;0.005;0;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.PannerNode;243;-5859.369,-4493.12;Float;True;0.005;0;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.PowerNode;394;-4252.169,-6086.243;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.PannerNode;342;-6038.5,-1458.329;Float;True;0.005;0;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.PannerNode;341;-5941.098,-897.7207;Float;True;0.005;0;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.TexturePropertyNode;211;-7245.16,-3412.737;Float;True;Property;_CloudTexture;Cloud Texture;2;0;None;False;white;Auto;0;1;SAMPLER2D
Node;AmplifyShaderEditor.SamplerNode;343;-5484.121,-809.3895;Float;True;Property;_TextureSample16;Texture Sample 16;0;0;Assets/decal5.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;233;-4104.577,-6068.034;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.AbsOpNode;29;-1729.005,-1275.362;Float;True;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-1146.614,-684.2911;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;76;-5247.779,-4538.282;Float;True;Property;_TextureSample4;Texture Sample 4;0;0;Assets/decal5.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;344;-5426.91,-1503.491;Float;True;Property;_TextureSample17;Texture Sample 17;0;0;Assets/decal5.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;349;-4233.712,-2571.247;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;348;-4098.114,-3027.945;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;231;-3994.076,-5562.436;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;189;-5304.99,-3844.181;Float;True;Property;_TextureSample2;Texture Sample 2;0;0;Assets/decal5.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;16;-1182.202,-401.2013;Float;False;Constant;_nHorizonRelative;nHorizonRelative;0;0;8;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.CommentaryNode;301;-2621.452,-1969.622;Float;False;749.6583;593.1614;;8;388;390;305;389;387;304;302;303;Cloud Volume Mask;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;302;-2564.852,-1776.021;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;387;-2550.267,-1623.847;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;303;-2561.852,-1913.621;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.CommentaryNode;217;-2652.77,-2997.033;Float;False;880.4576;778.2115;;8;216;386;385;384;383;215;213;214;Cloud Rim Mask;0;0
Node;AmplifyShaderEditor.PowerNode;15;-916.0045,-576.5334;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.CommentaryNode;212;-2695.14,-4157.037;Float;False;1132.647;1081.126;Comment;8;206;382;381;202;379;199;380;198;BaseClouds;0;0
Node;AmplifyShaderEditor.ClampOpNode;33;-1378.118,-1469.138;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;388;-2569.768,-1505.547;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;380;-2624.973,-3322.742;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;383;-2586.068,-2584.145;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;213;-2580.369,-2913.832;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;199;-2638.34,-4100.237;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;379;-2632.773,-3584.043;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;214;-2583.37,-2776.231;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;30;-1099.751,-1606.451;Float;True;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;389;-2329.266,-1603.047;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;198;-2631.739,-3829.039;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;384;-2606.068,-2431.645;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.PowerNode;219;-633.6699,-638.0235;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;5.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;304;-2362.451,-1853.02;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.PowerNode;242;-882.4773,-1767.135;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;3.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;202;-2356.684,-3956.492;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;390;-2188.866,-1759.047;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;299;-351.769,-627.8012;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;385;-2345.966,-2545.546;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;215;-2380.97,-2853.231;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;381;-2372.074,-3568.446;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;386;-2173.166,-2625.547;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;305;-2035.253,-1753.02;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;382;-2106.472,-3706.048;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;288;-594.9686,-1857.701;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;298;-207.6675,-676.0015;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.3;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;206;-1821.278,-3708.345;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;216;-1981.372,-2665.233;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;306;-132.467,-852.2007;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;310;-316.9646,-1911.137;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;5.0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;9;-1140.6,22.59881;Float;False;Constant;_SunSize;SunSize;0;0;10;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;397;-157.1662,-1533.645;Float;False;Property;_CloudShadowColor;Cloud Shadow Color;5;0;0,0,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;401;-269.366,-1627.944;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;311;-48.16455,-1963.238;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;273;69.42899,-786.5021;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;208;-29.37356,-1209.841;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ExpOpNode;10;-956.5012,30.09884;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.LightColorNode;269;295.9292,-663.0031;Float;False;0;1;COLOR
Node;AmplifyShaderEditor.SimpleAddOpNode;272;170.0307,-1120.803;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;22;-718.5992,38.19864;Float;False;Constant;_SunSharpness;SunSharpness;0;0;50;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;402;165.8351,-1813.345;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;300;-358.868,-137.5015;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;-0.1;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;241;103.3159,-2125.135;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;406;120.4357,-1560.545;Float;True;1;0;COLOR;0.0;False;1;COLOR
Node;AmplifyShaderEditor.ClampOpNode;7;-911.9982,-255.8012;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;274;-86.87089,-1792.501;Float;False;Property;_CloudColor;Cloud Color;4;0;0,0,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;223;-233.8688,-132.1305;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;270;470.7273,-1197.803;Float;True;2;0;FLOAT;0.0;False;1;COLOR;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.RangedFloatNode;408;588.4357,-1562.545;Float;False;Property;_CloudShadowIntensity;CloudShadow Intensity;6;0;0.15;0;0.5;0;1;FLOAT
Node;AmplifyShaderEditor.ExpOpNode;21;-524.5997,19.59863;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;275;384.0292,-2089.602;Float;True;2;0;FLOAT;0.0;False;1;COLOR;0.0;False;1;COLOR
Node;AmplifyShaderEditor.RangedFloatNode;32;-1270.016,-1263.237;Float;False;Constant;_HorizonSharpness;HorizonSharpness;0;0;4;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.PowerNode;8;-699.1994,-259.6011;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;398;379.5332,-1687.643;Float;True;2;0;FLOAT;0.0;False;1;COLOR;0.0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;18;-511.5993,-261.5012;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;407;738.4357,-1667.545;Float;False;2;0;COLOR;0.0;False;1;FLOAT;0.2132353,0.2132353,0.2132353,0;False;1;COLOR
Node;AmplifyShaderEditor.ColorNode;285;-683.1718,-1125.903;Float;False;Property;_HorizonColor;HorizonColor;1;0;0,0,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.PowerNode;221;-220.1653,-21.23053;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.001;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;282;658.6285,-1858.798;Float;False;2;0;COLOR;0.0;False;1;COLOR;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.PowerNode;31;-762.859,-1420.941;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.LightColorNode;279;673.7288,-321.1025;Float;False;0;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;283;-285.5726,-1348.295;Float;False;2;0;FLOAT;0.0,0,0,0;False;1;COLOR;0;False;1;COLOR
Node;AmplifyShaderEditor.OneMinusNode;220;40.23409,-55.93156;Float;True;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;20;-346.2994,-255.8011;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;404;1019.436,-1664.545;Float;False;2;0;COLOR;0.0;False;1;COLOR;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.ColorNode;276;684.0277,405.2973;Float;False;Property;_skyColor;skyColor;0;0;0.2502163,0.3823529,0.3495466,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;222;294.3323,-263.5314;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;52;835.5608,-1489.94;Float;True;2;0;COLOR;0.0;False;1;COLOR;0.0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;297;860.0323,-233.5021;Float;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0.09558821,0.09558821,0.09558821,0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;278;808.8283,-509.9021;Float;True;2;0;FLOAT;0.0;False;1;COLOR;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.ClampOpNode;54;1150.461,-1214.64;Float;True;3;0;COLOR;0.0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleAddOpNode;293;-122.7697,-378.1013;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;296;1010.733,-200.8021;Float;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleAddOpNode;50;1351.761,-713.24;Float;False;2;0;COLOR;0.0;False;1;COLOR;0.0;False;1;COLOR
Node;AmplifyShaderEditor.LerpOp;294;1393.128,-91.50147;Float;False;3;0;COLOR;0.0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleAddOpNode;277;1683.729,-508.6031;Float;True;2;0;COLOR;0.0;False;1;COLOR;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2154.494,-242.208;Float;False;True;2;Float;ASEMaterialInspector;0;Unlit;ProceduralSky;False;False;False;False;True;True;True;True;True;True;True;True;Back;2;0;False;0;0;Custom;0.5;True;False;0;True;Background;Background;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;False;0;4;10;25;False;0.5;False;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;Relative;0;;11;-1;-1;-1;14;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;OBJECT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;320;0;319;0
WireConnection;360;0;320;0
WireConnection;192;0;320;0
WireConnection;361;0;360;0
WireConnection;321;0;320;0
WireConnection;315;0;192;2
WireConnection;70;0;320;0
WireConnection;323;0;321;2
WireConnection;323;1;321;1
WireConnection;191;0;315;0
WireConnection;191;1;192;1
WireConnection;172;0;320;0
WireConnection;353;0;320;0
WireConnection;352;0;320;0
WireConnection;359;0;361;0
WireConnection;359;1;360;1
WireConnection;194;0;320;0
WireConnection;374;0;193;0
WireConnection;266;0;70;0
WireConnection;266;1;409;0
WireConnection;358;0;323;0
WireConnection;358;1;409;0
WireConnection;325;0;359;0
WireConnection;325;1;409;0
WireConnection;367;0;194;0
WireConnection;362;0;352;0
WireConnection;267;0;191;0
WireConnection;267;1;409;0
WireConnection;375;0;193;0
WireConnection;363;0;353;0
WireConnection;366;0;172;0
WireConnection;197;0;367;0
WireConnection;197;1;193;0
WireConnection;356;0;362;0
WireConnection;356;1;374;0
WireConnection;264;0;267;0
WireConnection;264;1;324;0
WireConnection;357;0;363;0
WireConnection;357;1;375;0
WireConnection;173;0;366;0
WireConnection;173;1;193;0
WireConnection;246;0;266;0
WireConnection;246;1;324;0
WireConnection;328;0;325;0
WireConnection;328;1;324;0
WireConnection;327;0;358;0
WireConnection;327;1;324;0
WireConnection;265;0;268;0
WireConnection;265;1;264;0
WireConnection;368;0;173;0
WireConnection;330;0;268;0
WireConnection;330;1;328;0
WireConnection;331;0;268;0
WireConnection;331;1;327;0
WireConnection;370;0;197;0
WireConnection;377;0;357;0
WireConnection;372;0;356;0
WireConnection;245;0;268;0
WireConnection;245;1;246;0
WireConnection;188;0;191;0
WireConnection;188;1;204;0
WireConnection;251;0;245;1
WireConnection;251;1;410;0
WireConnection;378;0;377;0
WireConnection;369;0;368;0
WireConnection;373;0;372;0
WireConnection;333;0;330;1
WireConnection;333;1;410;0
WireConnection;335;0;359;0
WireConnection;335;1;204;0
WireConnection;84;0;70;0
WireConnection;84;1;204;0
WireConnection;334;0;323;0
WireConnection;334;1;204;0
WireConnection;263;0;265;1
WireConnection;263;1;410;0
WireConnection;371;0;370;0
WireConnection;336;0;331;1
WireConnection;336;1;410;0
WireConnection;339;0;333;0
WireConnection;339;1;335;0
WireConnection;4;0;320;0
WireConnection;4;1;23;0
WireConnection;347;0;373;0
WireConnection;347;1;373;0
WireConnection;396;0;193;0
WireConnection;235;0;369;0
WireConnection;235;1;369;0
WireConnection;259;0;263;0
WireConnection;259;1;188;0
WireConnection;234;0;371;0
WireConnection;234;1;371;0
WireConnection;337;0;336;0
WireConnection;337;1;334;0
WireConnection;250;0;251;0
WireConnection;250;1;84;0
WireConnection;346;0;378;0
WireConnection;346;1;378;0
WireConnection;26;0;320;0
WireConnection;11;0;12;0
WireConnection;11;1;4;0
WireConnection;391;0;347;0
WireConnection;391;1;396;0
WireConnection;393;0;235;0
WireConnection;393;1;396;0
WireConnection;392;0;346;0
WireConnection;392;1;396;0
WireConnection;260;0;259;0
WireConnection;260;1;261;0
WireConnection;243;0;250;0
WireConnection;243;1;324;0
WireConnection;394;0;234;0
WireConnection;394;1;396;0
WireConnection;342;0;339;0
WireConnection;342;1;324;0
WireConnection;341;0;337;0
WireConnection;341;1;338;0
WireConnection;343;0;211;0
WireConnection;343;1;341;0
WireConnection;233;0;394;0
WireConnection;29;0;26;0
WireConnection;13;0;14;0
WireConnection;13;1;11;0
WireConnection;76;0;211;0
WireConnection;76;1;243;0
WireConnection;344;0;211;0
WireConnection;344;1;342;0
WireConnection;349;0;391;0
WireConnection;348;0;392;0
WireConnection;231;0;393;0
WireConnection;189;0;211;0
WireConnection;189;1;260;0
WireConnection;302;0;189;3
WireConnection;302;1;231;0
WireConnection;387;0;344;3
WireConnection;387;1;348;0
WireConnection;303;0;76;3
WireConnection;303;1;233;0
WireConnection;15;0;13;0
WireConnection;15;1;16;0
WireConnection;33;0;29;0
WireConnection;388;0;343;3
WireConnection;388;1;349;0
WireConnection;380;0;343;1
WireConnection;380;1;349;0
WireConnection;383;0;344;2
WireConnection;383;1;348;0
WireConnection;213;0;76;2
WireConnection;213;1;233;0
WireConnection;199;0;76;1
WireConnection;199;1;233;0
WireConnection;379;0;344;1
WireConnection;379;1;348;0
WireConnection;214;0;189;2
WireConnection;214;1;231;0
WireConnection;30;0;33;0
WireConnection;389;0;387;0
WireConnection;389;1;388;0
WireConnection;198;0;189;1
WireConnection;198;1;231;0
WireConnection;384;0;343;2
WireConnection;384;1;349;0
WireConnection;219;0;15;0
WireConnection;304;0;303;0
WireConnection;304;1;302;0
WireConnection;242;0;30;0
WireConnection;202;0;199;0
WireConnection;202;1;198;0
WireConnection;390;0;304;0
WireConnection;390;1;389;0
WireConnection;299;0;219;0
WireConnection;299;1;219;0
WireConnection;385;0;383;0
WireConnection;385;1;384;0
WireConnection;215;0;213;0
WireConnection;215;1;214;0
WireConnection;381;0;379;0
WireConnection;381;1;380;0
WireConnection;386;0;215;0
WireConnection;386;1;385;0
WireConnection;305;0;390;0
WireConnection;382;0;202;0
WireConnection;382;1;381;0
WireConnection;288;0;26;0
WireConnection;288;1;242;0
WireConnection;298;0;299;0
WireConnection;206;0;382;0
WireConnection;216;0;386;0
WireConnection;306;0;298;0
WireConnection;306;1;305;0
WireConnection;310;0;288;0
WireConnection;401;0;305;0
WireConnection;311;0;310;0
WireConnection;273;0;206;0
WireConnection;273;1;306;0
WireConnection;208;0;216;0
WireConnection;208;1;219;0
WireConnection;10;0;9;0
WireConnection;272;0;208;0
WireConnection;272;1;273;0
WireConnection;402;0;311;0
WireConnection;402;1;401;0
WireConnection;300;0;206;0
WireConnection;241;0;206;0
WireConnection;241;1;311;0
WireConnection;406;0;397;0
WireConnection;7;0;4;0
WireConnection;223;0;300;0
WireConnection;270;0;272;0
WireConnection;270;1;269;0
WireConnection;21;0;22;0
WireConnection;275;0;241;0
WireConnection;275;1;274;0
WireConnection;8;0;7;0
WireConnection;8;1;10;0
WireConnection;398;0;402;0
WireConnection;398;1;406;0
WireConnection;18;0;8;0
WireConnection;18;1;21;0
WireConnection;407;0;398;0
WireConnection;407;1;408;0
WireConnection;221;0;223;0
WireConnection;282;0;275;0
WireConnection;282;1;270;0
WireConnection;31;0;30;0
WireConnection;31;1;32;0
WireConnection;283;0;31;0
WireConnection;283;1;285;0
WireConnection;220;0;221;0
WireConnection;20;0;18;0
WireConnection;404;0;282;0
WireConnection;404;1;407;0
WireConnection;222;0;20;0
WireConnection;222;1;220;0
WireConnection;52;0;283;0
WireConnection;52;1;404;0
WireConnection;297;0;279;0
WireConnection;278;0;222;0
WireConnection;278;1;279;0
WireConnection;54;0;52;0
WireConnection;293;0;15;0
WireConnection;293;1;13;0
WireConnection;296;0;297;0
WireConnection;296;1;276;0
WireConnection;50;0;54;0
WireConnection;50;1;278;0
WireConnection;294;0;276;0
WireConnection;294;1;296;0
WireConnection;294;2;293;0
WireConnection;277;0;50;0
WireConnection;277;1;294;0
WireConnection;0;2;277;0
ASEEND*/
//CHKSM=8654A2E6C2F1A6E68BECE65D1C744D9596FD0324