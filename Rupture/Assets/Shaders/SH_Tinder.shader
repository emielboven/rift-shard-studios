// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SH_Tinder"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_emissionStrength("emissionStrength", Float) = 2
		_emissionColor("emissionColor", Color) = (1,0.6827586,0,0)
		_albedoColor("albedoColor", Color) = (0,0,0,0)
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			fixed filler;
		};

		uniform float4 _albedoColor;
		uniform float4 _emissionColor;
		uniform float _emissionStrength;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			o.Albedo = _albedoColor.rgb;
			o.Emission = ( _emissionColor * _emissionStrength ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=11001
187;275;1906;902;1725.398;42.00009;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;7;-1265.398,263.9999;Float;False;Property;_emissionStrength;emissionStrength;0;0;2;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;1;-1269.798,21.39994;Float;False;Property;_emissionColor;emissionColor;1;0;1,0.6827586,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;10;-1267.398,-176.0001;Float;False;Property;_albedoColor;albedoColor;2;0;0,0,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;-930.398,166.9999;Float;False;2;2;0;COLOR;0.0;False;1;FLOAT;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-545.5999,-9.599999;Float;False;True;2;Float;ASEMaterialInspector;0;Standard;SH_Tinder;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;Relative;0;;-1;-1;-1;-1;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;OBJECT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;6;0;1;0
WireConnection;6;1;7;0
WireConnection;0;0;10;0
WireConnection;0;2;6;0
ASEEND*/
//CHKSM=E0E5AB90317F6644273D4866323DC02D1A86A3FB