// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SH_PrototypeWorldGrid"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_GridTexture("Grid Texture", 2D) = "white" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldNormal;
			float3 worldPos;
		};

		uniform sampler2D _GridTexture;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float temp_output_12_0 = abs( i.worldNormal.x );
			float ifLocalVar26 = 0;
			if( ( temp_output_12_0 * temp_output_12_0 ) > 0.5 )
				ifLocalVar26 = 1.0;
			else if( ( temp_output_12_0 * temp_output_12_0 ) == 0.5 )
				ifLocalVar26 = 0.5;
			else if( ( temp_output_12_0 * temp_output_12_0 ) < 0.5 )
				ifLocalVar26 = 0.0;
			float temp_output_20_0 = abs( i.worldNormal.y );
			float ifLocalVar28 = 0;
			if( ( temp_output_20_0 * temp_output_20_0 ) > 0.5 )
				ifLocalVar28 = 1.0;
			else if( ( temp_output_20_0 * temp_output_20_0 ) == 0.5 )
				ifLocalVar28 = 0.5;
			else if( ( temp_output_20_0 * temp_output_20_0 ) < 0.5 )
				ifLocalVar28 = 0.0;
			float temp_output_23_0 = abs( i.worldNormal.z );
			float ifLocalVar27 = 0;
			if( ( temp_output_23_0 * temp_output_23_0 ) > 0.5 )
				ifLocalVar27 = 1.0;
			else if( ( temp_output_23_0 * temp_output_23_0 ) == 0.5 )
				ifLocalVar27 = 0.5;
			else if( ( temp_output_23_0 * temp_output_23_0 ) < 0.5 )
				ifLocalVar27 = 0.0;
			float3 appendResult19 = float3( ifLocalVar26 , ifLocalVar28 , ifLocalVar27 );
			float3 ase_worldPos = i.worldPos;
			float2 appendResult4 = float2( ase_worldPos.x , ase_worldPos.y );
			float4 _SideColor = float4(1,0.241,0,0);
			float4 temp_output_31_0 = ( tex2D( _GridTexture, appendResult4 ) * _SideColor );
			float2 appendResult5 = float2( ase_worldPos.y , ase_worldPos.z );
			float2 appendResult6 = float2( ase_worldPos.z , ase_worldPos.x );
			float3 layeredBlendVar11 = appendResult19;
			float4 layeredBlend11 = ( lerp( lerp( lerp( temp_output_31_0 , ( tex2D( _GridTexture, appendResult5 ) * _SideColor ) , layeredBlendVar11.x ) , tex2D( _GridTexture, appendResult6 ) , layeredBlendVar11.y ) , temp_output_31_0 , layeredBlendVar11.z ) );
			o.Albedo = layeredBlend11.xyz;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 tSpace0 : TEXCOORD1;
				float4 tSpace1 : TEXCOORD2;
				float4 tSpace2 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = IN.worldPos;
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=11001
7;29;1906;1124;1220.383;379.7596;1.244998;True;True
Node;AmplifyShaderEditor.WorldNormalVector;10;-827.7299,891.2994;Float;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.AbsOpNode;12;-580.9344,890.5494;Float;True;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.WorldPosInputsNode;2;-1909.1,170.2749;Float;True;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.AbsOpNode;23;-567.1068,1321.942;Float;True;1;0;FLOAT;0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.AbsOpNode;20;-574.9081,1106.142;Float;True;1;0;FLOAT;0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.AppendNode;4;-1068.804,348.4754;Float;False;FLOAT2;0;0;0;0;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.AppendNode;5;-1123.377,93.0495;Float;False;FLOAT2;0;0;0;0;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.TexturePropertyNode;7;-1104.004,-213.1004;Float;True;Property;_GridTexture;Grid Texture;0;0;None;False;white;Auto;0;1;SAMPLER2D
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-345.608,1320.744;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;-353.4083,1104.944;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-359.4349,889.3508;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;1;-746.4999,24.7998;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;Assets/Textures/T_Prototype_D.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;9;-720.5024,425.1995;Float;True;Property;_TextureSample2;Texture Sample 2;0;0;Assets/Textures/T_Prototype_D.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;30;-768.2306,-227.7948;Float;False;Constant;_SideColor;Side Color;1;0;1,0.241,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ConditionalIfNode;26;-78.50468,899.7801;Float;False;False;5;0;FLOAT;0.0;False;1;FLOAT;0.5;False;2;FLOAT;1.0;False;3;FLOAT;0.5;False;4;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ConditionalIfNode;28;-75.30635,1122.18;Float;False;False;5;0;FLOAT;0.0;False;1;FLOAT;0.5;False;2;FLOAT;1.0;False;3;FLOAT;0.5;False;4;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.AppendNode;6;-1102.077,213.2999;Float;False;FLOAT2;0;0;0;0;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.ConditionalIfNode;27;-82.90633,1344.98;Float;False;False;5;0;FLOAT;0.0;False;1;FLOAT;0.5;False;2;FLOAT;1.0;False;3;FLOAT;0.5;False;4;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;-149.6848,667.2838;Float;False;2;2;0;FLOAT4;0.0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-183.2368,320.8046;Float;False;2;2;0;FLOAT4;0.0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.AppendNode;19;398.0965,1081.781;Float;True;FLOAT3;0;0;0;0;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT3
Node;AmplifyShaderEditor.SamplerNode;8;-734.803,234.0999;Float;True;Property;_TextureSample1;Texture Sample 1;0;0;Assets/Textures/T_Prototype_D.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.LayeredBlendNode;11;336.095,736.0002;Float;False;6;0;FLOAT3;0.0;False;1;FLOAT4;0.0;False;2;FLOAT4;0.0;False;3;FLOAT4;0.0;False;4;FLOAT4;0.0;False;5;FLOAT;0.0;False;1;FLOAT4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;Standard;SH_PrototypeWorldGrid;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;Relative;0;;-1;-1;-1;-1;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;OBJECT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;12;0;10;1
WireConnection;23;0;10;3
WireConnection;20;0;10;2
WireConnection;4;0;2;1
WireConnection;4;1;2;2
WireConnection;5;0;2;2
WireConnection;5;1;2;3
WireConnection;24;0;23;0
WireConnection;24;1;23;0
WireConnection;21;0;20;0
WireConnection;21;1;20;0
WireConnection;13;0;12;0
WireConnection;13;1;12;0
WireConnection;1;0;7;0
WireConnection;1;1;5;0
WireConnection;9;0;7;0
WireConnection;9;1;4;0
WireConnection;26;0;13;0
WireConnection;28;0;21;0
WireConnection;6;0;2;3
WireConnection;6;1;2;1
WireConnection;27;0;24;0
WireConnection;31;0;9;0
WireConnection;31;1;30;0
WireConnection;29;0;1;0
WireConnection;29;1;30;0
WireConnection;19;0;26;0
WireConnection;19;1;28;0
WireConnection;19;2;27;0
WireConnection;8;0;7;0
WireConnection;8;1;6;0
WireConnection;11;0;19;0
WireConnection;11;1;31;0
WireConnection;11;2;29;0
WireConnection;11;3;8;0
WireConnection;11;4;31;0
WireConnection;0;0;11;0
ASEEND*/
//CHKSM=B2E48558B3A7B76A06935B6A08DA2A813C47F67F