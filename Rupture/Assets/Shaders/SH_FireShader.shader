// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SH_FireShader"
{
	Properties
	{
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_TextureSample4("Texture Sample 4", 2D) = "white" {}
		_Float3("Float 3", Range( 0 , 1)) = 0.2303568
		_TextureSample3("Texture Sample 3", 2D) = "white" {}
		_Float1("Float 1", Range( 0 , 1)) = 0.001
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		_TextureSample2("Texture Sample 2", 2D) = "white" {}
		_HDRRange("HDR Range", Range( 0 , 2)) = 0.5
		_MainColor("Main Color", Color) = (1,0.922921,0.3014706,0)
		_SecondColor("Second Color", Color) = (1,0.2275862,0,1)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit alpha:fade keepalpha vertex:vertexDataFunc 
		struct Input
		{
			float2 texcoord_0;
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
			float4 screenPos;
		};

		uniform float _HDRRange;
		uniform float4 _MainColor;
		uniform float4 _SecondColor;
		uniform sampler2D _TextureSample0;
		uniform sampler2D _TextureSample1;
		uniform sampler2D _TextureSample3;
		uniform sampler2D _TextureSample2;
		uniform float4 _TextureSample2_ST;
		uniform sampler2D _TextureSample4;
		uniform float _Float3;
		uniform float _Float1;
		uniform sampler2D _CameraDepthTexture;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			o.texcoord_0.xy = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
		}

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float temp_output_35_0 = pow( i.texcoord_0.y , 1.6 );
			float clampResult39 = clamp( ( temp_output_35_0 + temp_output_35_0 ) , 0.0 , 1.0 );
			float2 panner2 = ( i.texcoord_0 + 1.0 * _Time.y * float2( -0.2,-1.5 ));
			float2 panner10 = ( i.texcoord_0 + 1.0 * _Time.y * float2( 0.2,-0.7 ));
			float2 uv_TextureSample2 = i.uv_texcoord * _TextureSample2_ST.xy + _TextureSample2_ST.zw;
			float4 tex2DNode8 = tex2D( _TextureSample2, uv_TextureSample2 );
			float2 appendResult33 = (float2(i.texcoord_0.x , ( ( clampResult39 * ( ( pow( tex2D( _TextureSample1, panner2 ).r , 1.0 ) + pow( tex2D( _TextureSample3, panner10 ).a , 3.0 ) ) * ( tex2DNode8.r + tex2DNode8.r ) ) ) + i.texcoord_0.y )));
			float4 tex2DNode1 = tex2D( _TextureSample0, appendResult33 );
			float4 lerpResult72 = lerp( _MainColor , _SecondColor , tex2DNode1.r);
			o.Emission = ( ( unity_ColorSpaceDouble * _HDRRange ) * lerpResult72 ).rgb;
			float temp_output_57_0 = ( _Float3 - _Float1 );
			float clampResult62 = clamp( ( ( distance( tex2D( _TextureSample4, appendResult33 ).r , 0.2 ) - temp_output_57_0 ) / ( ( _Float3 + _Float1 ) - temp_output_57_0 ) ) , 0.0 , 1.0 );
			float temp_output_73_0 = ( ( tex2DNode1.r + tex2DNode1.r ) * clampResult62 );
			float clampResult63 = clamp( ( temp_output_73_0 + temp_output_73_0 + temp_output_73_0 ) , 0.0 , 1.0 );
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float screenDepth76 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD(ase_screenPos))));
			float distanceDepth76 = abs( ( screenDepth76 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( 0.4 ) );
			float clampResult78 = clamp( distanceDepth76 , 0.0 , 1.0 );
			float clampResult77 = clamp( ( i.vertexColor.a * clampResult63 * clampResult78 ) , 0.0 , 1.0 );
			o.Alpha = clampResult77;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13701
7;29;1906;1004;186.6085;-25.9967;1;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;70;-2297.495,190.1023;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.PannerNode;2;-1462.802,-274.4;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-0.2,-1.5;False;1;FLOAT;1.0;False;1;FLOAT2
Node;AmplifyShaderEditor.PannerNode;10;-1317.5,157.7999;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.2,-0.7;False;1;FLOAT;1.0;False;1;FLOAT2
Node;AmplifyShaderEditor.SamplerNode;3;-1187.102,-337.6;Float;True;Property;_TextureSample1;Texture Sample 1;5;0;Assets/Textures/T_FXNoise_Soft.tga;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;9;-1155.101,315.7999;Float;True;Property;_TextureSample3;Texture Sample 3;3;0;Assets/Textures/T_FXNoise_Soft.tga;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.PowerNode;40;-813.2996,362.0007;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;3.0;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;8;-772.1006,-353.2001;Float;True;Property;_TextureSample2;Texture Sample 2;6;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.PowerNode;35;-768.3994,-111.4992;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;1.6;False;1;FLOAT
Node;AmplifyShaderEditor.PowerNode;42;-1047.699,5.600732;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;53;-522.4991,-103.1995;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;43;-451.1994,-323.299;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;17;-715.0009,123.7001;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;-355.5007,197.1002;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;39;-283.7994,-41.29919;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;-87.79948,71.70039;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;31;266.6008,-47.09947;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.DynamicAppendNode;33;639.0009,234.3007;Float;True;FLOAT2;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.RangedFloatNode;55;1278.986,1089.872;Float;False;Property;_Float3;Float 3;2;0;0.2303568;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;64;1013.3,582.6021;Float;True;Property;_TextureSample4;Texture Sample 4;1;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;54;1236.785,1239.472;Float;False;Property;_Float1;Float 1;4;0;0.001;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;57;1645.686,1101.272;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;58;1636.686,1214.272;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.DistanceOpNode;56;1536.686,736.672;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.2;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;59;2051.586,914.7717;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;60;2070.686,1215.272;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;61;2329.897,1076.9;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;1;1002.899,386.4001;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;62;2904.798,877.0997;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;44;1414.299,276.4001;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0,0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;73;2172.902,596.9021;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;65;2448.604,479.5022;Float;True;3;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.DepthFade;76;2103.6,176.7022;Float;False;1;0;FLOAT;0.4;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;52;1194.6,-241.7997;Float;False;Property;_HDRRange;HDR Range;7;0;0.5;0;2;0;1;FLOAT
Node;AmplifyShaderEditor.ColorSpaceDouble;49;1163.3,-491.1997;Float;False;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;78;2254.101,81.20209;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;47;754.7,-233.1998;Float;False;Property;_SecondColor;Second Color;9;0;1,0.2275862,0,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.VertexColorNode;74;1654.701,80.80147;Float;False;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;46;754.7,-404.1998;Float;False;Property;_MainColor;Main Color;8;0;1,0.922921,0.3014706,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;63;1969.9,299.9009;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;75;2184.101,309.2021;Float;False;3;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;1468.3,-369.1997;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.LerpOp;72;1066.503,94.7023;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;50;1672.3,-235.1997;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.ClampOpNode;77;2497.101,293.2021;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2801.8,212.6;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;SH_FireShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;Back;0;0;False;0;0;Transparent;0.5;True;False;0;False;Transparent;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;0;4;10;25;False;0.5;True;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Spherical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;14;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;2;0;70;0
WireConnection;10;0;70;0
WireConnection;3;1;2;0
WireConnection;9;1;10;0
WireConnection;40;0;9;4
WireConnection;35;0;70;2
WireConnection;42;0;3;1
WireConnection;53;0;35;0
WireConnection;53;1;35;0
WireConnection;43;0;8;1
WireConnection;43;1;8;1
WireConnection;17;0;42;0
WireConnection;17;1;40;0
WireConnection;22;0;17;0
WireConnection;22;1;43;0
WireConnection;39;0;53;0
WireConnection;30;0;39;0
WireConnection;30;1;22;0
WireConnection;31;0;30;0
WireConnection;31;1;70;2
WireConnection;33;0;70;1
WireConnection;33;1;31;0
WireConnection;64;1;33;0
WireConnection;57;0;55;0
WireConnection;57;1;54;0
WireConnection;58;0;55;0
WireConnection;58;1;54;0
WireConnection;56;0;64;1
WireConnection;59;0;56;0
WireConnection;59;1;57;0
WireConnection;60;0;58;0
WireConnection;60;1;57;0
WireConnection;61;0;59;0
WireConnection;61;1;60;0
WireConnection;1;1;33;0
WireConnection;62;0;61;0
WireConnection;44;0;1;1
WireConnection;44;1;1;1
WireConnection;73;0;44;0
WireConnection;73;1;62;0
WireConnection;65;0;73;0
WireConnection;65;1;73;0
WireConnection;65;2;73;0
WireConnection;78;0;76;0
WireConnection;63;0;65;0
WireConnection;75;0;74;4
WireConnection;75;1;63;0
WireConnection;75;2;78;0
WireConnection;51;0;49;0
WireConnection;51;1;52;0
WireConnection;72;0;46;0
WireConnection;72;1;47;0
WireConnection;72;2;1;1
WireConnection;50;0;51;0
WireConnection;50;1;72;0
WireConnection;77;0;75;0
WireConnection;0;2;50;0
WireConnection;0;9;77;0
ASEEND*/
//CHKSM=DD9084E10BD5C2546E589A5D22000CBFF30FA20B