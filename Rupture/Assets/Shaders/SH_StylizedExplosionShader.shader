// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "StylizedExplosionShader"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_MaskClipValue( "Mask Clip Value", Float ) = 0.5
		_TessValue( "Max Tessellation", Range( 1, 32 ) ) = 4
		_TessMin( "Tess Min Distance", Float ) = 10
		_TessMax( "Tess Max Distance", Float ) = 25
		_MaxExplosionSize("Max Explosion Size", Range( -1 , 1)) = 1
		_Texture0("Texture 0", 2D) = "white" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Off
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "Tessellation.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 4.6
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldNormal;
			INTERNAL_DATA
		};

		struct appdata
		{
			float4 vertex : POSITION;
			float4 tangent : TANGENT;
			float3 normal : NORMAL;
			float4 texcoord : TEXCOORD0;
			float4 texcoord1 : TEXCOORD1;
			float4 texcoord2 : TEXCOORD2;
			float4 texcoord3 : TEXCOORD3;
			fixed4 color : COLOR;
			UNITY_VERTEX_INPUT_INSTANCE_ID
		};

		uniform sampler2D _Texture0;
		uniform float _MaxExplosionSize;
		uniform float _MaskClipValue = 0.5;
		uniform float _TessValue;
		uniform float _TessMin;
		uniform float _TessMax;

		float4 tessFunction( appdata v0, appdata v1, appdata v2 )
		{
			return UnityDistanceBasedTess( v0.vertex, v1.vertex, v2.vertex, _TessMin, _TessMax, _TessValue );
		}

		void vertexDataFunc( inout appdata v )
		{
			float3 temp_output_73_0 = abs( v.normal );
			float mulTime82 = _Time.y * 0.1;
			float2 appendResult71 = float2( v.normal.y , v.normal.z );
			float2 appendResult70 = float2( v.normal.z , v.normal.x );
			float2 appendResult69 = float2( v.normal.y , v.normal.x );
			float3 layeredBlendVar11 = clamp( ( temp_output_73_0 * temp_output_73_0 ) , float3( 0,0,0 ) , float3( 1,1,1 ) );
			float layeredBlend11 = ( lerp( lerp( lerp( 1.0 , clamp( tex2Dlod( _Texture0, float4( ( (abs( appendResult71+mulTime82 * float2(1.24,0.2 ))) * 0.35 ), 0.0 , 0.0 ) ).g , 0.0 , 1.0 ) , layeredBlendVar11.x ) , clamp( tex2Dlod( _Texture0, float4( ( (abs( appendResult70+mulTime82 * float2(1,-0.3 ))) * 0.35 ), 0.0 , 0.0 ) ).g , 0.0 , 1.0 ) , layeredBlendVar11.y ) , clamp( tex2Dlod( _Texture0, float4( ( (abs( appendResult69+mulTime82 * float2(1.3,0.17 ))) * 0.35 ), 0.0 , 0.0 ) ).g , 0.0 , 1.0 ) , layeredBlendVar11.z ) );
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );;
			float temp_output_30_0 = ( ( ( ( ( ase_objectScale.x + 0.5 ) + ( ase_objectScale.y + 0.5 ) ) + ase_objectScale.z ) * 0.33 ) + -1.0 );
			v.vertex.xyz += ( ( ( layeredBlend11 * v.normal ) * temp_output_30_0 ) + ( v.normal * temp_output_30_0 ) );
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldNormal = i.worldNormal;
			float3 vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			float3 temp_output_73_0 = abs( vertexNormal );
			float mulTime82 = _Time.y * 0.1;
			float2 appendResult71 = float2( vertexNormal.y , vertexNormal.z );
			float2 appendResult70 = float2( vertexNormal.z , vertexNormal.x );
			float2 appendResult69 = float2( vertexNormal.y , vertexNormal.x );
			float3 layeredBlendVar11 = clamp( ( temp_output_73_0 * temp_output_73_0 ) , float3( 0,0,0 ) , float3( 1,1,1 ) );
			float layeredBlend11 = ( lerp( lerp( lerp( 1.0 , clamp( tex2D( _Texture0, ( (abs( appendResult71+mulTime82 * float2(1.24,0.2 ))) * 0.35 ) ).g , 0.0 , 1.0 ) , layeredBlendVar11.x ) , clamp( tex2D( _Texture0, ( (abs( appendResult70+mulTime82 * float2(1,-0.3 ))) * 0.35 ) ).g , 0.0 , 1.0 ) , layeredBlendVar11.y ) , clamp( tex2D( _Texture0, ( (abs( appendResult69+mulTime82 * float2(1.3,0.17 ))) * 0.35 ) ).g , 0.0 , 1.0 ) , layeredBlendVar11.z ) );
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );;
			float temp_output_30_0 = ( ( ( ( ( ase_objectScale.x + 0.5 ) + ( ase_objectScale.y + 0.5 ) ) + ase_objectScale.z ) * 0.33 ) + -1.0 );
			float temp_output_60_0 = ( layeredBlend11 * ( temp_output_30_0 * 3.0 ) );
			o.Emission = lerp( lerp( lerp( float4(1,0.8280889,0,0) , float4(0.7426471,0.1091691,0,0) , smoothstep( 0.05 , 0.7 , temp_output_60_0 ) ) , float4(0.007028542,0.034,0.091,0) , smoothstep( 0.4 , 0.9 , temp_output_60_0 ) ) , float4(0,0,0,0) , smoothstep( 1.0 , 2.0 , temp_output_60_0 ) ).rgb;
			o.Alpha = 1;
			float ifLocalVar40 = 0;
			if( ( ( ( 1.0 - clamp( layeredBlend11 , 0.0 , 1.0 ) ) + 1.1 ) * temp_output_30_0 ) > _MaxExplosionSize )
				ifLocalVar40 = 0.0;
			else if( ( ( ( 1.0 - clamp( layeredBlend11 , 0.0 , 1.0 ) ) + 1.1 ) * temp_output_30_0 ) == _MaxExplosionSize )
				ifLocalVar40 = 1.0;
			else if( ( ( ( 1.0 - clamp( layeredBlend11 , 0.0 , 1.0 ) ) + 1.1 ) * temp_output_30_0 ) < _MaxExplosionSize )
				ifLocalVar40 = 1.0;
			clip( ifLocalVar40 - _MaskClipValue );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows vertex:vertexDataFunc tessellate:tessFunction 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 tSpace0 : TEXCOORD1;
				float4 tSpace1 : TEXCOORD2;
				float4 tSpace2 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=11001
7;29;1906;1124;1291.366;-170.9484;1;True;True
Node;AmplifyShaderEditor.ObjectScaleNode;29;-1063.298,696.2037;Float;False;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.NormalVertexDataNode;68;-2166.124,155.738;Float;False;0;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;91;-889.3665,825.9484;Float;False;2;2;0;FLOAT;0.5;False;1;FLOAT;0.5;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;92;-894.3665,932.9484;Float;False;2;2;0;FLOAT;0.5;False;1;FLOAT;0.5;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleTimeNode;82;-1870.8,-258.6991;Float;False;1;0;FLOAT;0.1;False;1;FLOAT
Node;AmplifyShaderEditor.AppendNode;71;-1613.321,346.6378;Float;False;FLOAT2;0;0;0;0;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.AppendNode;70;-1618.522,198.838;Float;False;FLOAT2;0;0;0;0;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.AppendNode;69;-1617.522,84.83809;Float;False;FLOAT2;0;0;0;0;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleAddOpNode;43;-746.6993,698.1004;Float;False;2;2;0;FLOAT;0,0,0;False;1;FLOAT;0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.PannerNode;81;-1415.203,59.20022;Float;False;1.3;0.17;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.PannerNode;84;-1364.293,199.4796;Float;False;1;-0.3;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.PannerNode;86;-1379.593,339.7801;Float;False;1.24;0.2;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.RangedFloatNode;72;-2010.619,-140.859;Float;False;Constant;_Float2;Float 2;1;0;0.35;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;76;-1163.725,21.34096;Float;False;2;2;0;FLOAT2;0.0;False;1;FLOAT;0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.AbsOpNode;73;-1956.124,-5.362986;Float;False;1;0;FLOAT3;0.0;False;1;FLOAT3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;85;-1167.339,338.0461;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.TexturePropertyNode;7;-1214.296,-350.8003;Float;True;Property;_Texture0;Texture 0;8;0;Assets/Textures/T_FXNoise_Soft.tga;False;white;Auto;0;1;SAMPLER2D
Node;AmplifyShaderEditor.SimpleAddOpNode;44;-499.7993,808.1008;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;83;-1108.012,217.2208;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.SamplerNode;8;-835.7007,142.2;Float;True;Property;_TextureSample1;Texture Sample 1;1;0;Assets/Textures/T_FXNoise_Soft.tga;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;9;-851.7007,354.5;Float;True;Property;_TextureSample2;Texture Sample 2;0;0;Assets/Textures/T_FXNoise_Soft.tga;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;1;-843.5004,-77.9;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;Assets/Textures/T_FXNoise_Soft.tga;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;77;-1427.322,-87.66301;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0;False;1;FLOAT3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;46;-365.9991,712.0008;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.33;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;31;-265.9959,863.3043;Float;False;Constant;_Float1;Float 1;3;0;-1;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;90;-488.9675,370.6485;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;89;-485.3674,230.5485;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;88;-488.3673,100.8485;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;30;-47.19637,755.7045;Float;False;2;2;0;FLOAT;0,0,0;False;1;FLOAT;0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;87;-482.7675,-40.75148;Float;False;3;0;FLOAT3;0.0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;1,1,1;False;1;FLOAT3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;-241.306,-62.29957;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;3.0;False;1;FLOAT
Node;AmplifyShaderEditor.LayeredBlendNode;11;-240.7002,190.3993;Float;False;6;0;FLOAT3;0.0;False;1;FLOAT;1.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;55;203.7979,233.1001;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;60;91.79352,41.0997;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.NormalVertexDataNode;19;-165.096,565.4027;Float;False;0;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;47;408.699,232.7005;Float;True;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SmoothstepOpNode;36;83.30507,-246.3458;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;0.05;False;2;FLOAT;0.7;False;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;38;-191.6952,-796.9449;Float;False;Constant;_Color3;Color 3;3;0;1,0.8280889,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;35;-246.898,-483.1955;Float;False;Constant;_Color2;Color 2;3;0;0.7426471,0.1091691,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;25;368.5998,-594.7003;Float;False;Constant;_Color0;Color 0;3;0;0.007028542,0.034,0.091,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;57;632.5944,281.8003;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;1.1;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;18;60.90399,466.4027;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT3;0.0;False;1;FLOAT3
Node;AmplifyShaderEditor.LerpOp;37;360.0045,-391.5457;Float;False;3;0;COLOR;0.0;False;1;COLOR;0.0,0,0,0;False;2;FLOAT;0.0;False;1;COLOR
Node;AmplifyShaderEditor.SmoothstepOpNode;33;470.0024,-93.69693;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;0.4;False;2;FLOAT;0.9;False;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;39;488.102,-784.0496;Float;False;Constant;_Color4;Color 4;3;0;0,0,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.LerpOp;34;693.5021,-248.997;Float;False;3;0;COLOR;0.0;False;1;COLOR;0.0,0,0,0;False;2;FLOAT;0.0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;67;291.2972,720.3004;Float;False;2;2;0;FLOAT3;0.0;False;1;FLOAT;0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.RangedFloatNode;42;637.3026,757.6016;Float;False;Property;_MaxExplosionSize;Max Explosion Size;7;0;1;-1;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;53;817.0981,307.9004;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;-1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;638.304,505.0029;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.SmoothstepOpNode;32;814.6036,26.10339;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;1.0;False;2;FLOAT;2.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;65;892.8983,515.4997;Float;False;2;2;0;FLOAT3;0.0;False;1;FLOAT3;0.0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.ConditionalIfNode;40;1408.404,553.9009;Float;False;False;5;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;1.0;False;4;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.LerpOp;24;974.6006,-265.0003;Float;False;3;0;COLOR;0.0;False;1;COLOR;0.0,0,0,0;False;2;FLOAT;0.0;False;1;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1746.5,112.8;Float;False;True;6;Float;ASEMaterialInspector;0;Standard;StylizedExplosionShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;0;False;0;0;Custom;0.5;True;True;0;True;TransparentCutout;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;True;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;Relative;0;;0;-1;-1;1;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;OBJECT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;91;0;29;1
WireConnection;92;0;29;2
WireConnection;71;0;68;2
WireConnection;71;1;68;3
WireConnection;70;0;68;3
WireConnection;70;1;68;1
WireConnection;69;0;68;2
WireConnection;69;1;68;1
WireConnection;43;0;91;0
WireConnection;43;1;92;0
WireConnection;81;0;69;0
WireConnection;81;1;82;0
WireConnection;84;0;70;0
WireConnection;84;1;82;0
WireConnection;86;0;71;0
WireConnection;86;1;82;0
WireConnection;76;0;81;0
WireConnection;76;1;72;0
WireConnection;73;0;68;0
WireConnection;85;0;86;0
WireConnection;85;1;72;0
WireConnection;44;0;43;0
WireConnection;44;1;29;3
WireConnection;83;0;84;0
WireConnection;83;1;72;0
WireConnection;8;0;7;0
WireConnection;8;1;83;0
WireConnection;9;0;7;0
WireConnection;9;1;85;0
WireConnection;1;0;7;0
WireConnection;1;1;76;0
WireConnection;77;0;73;0
WireConnection;77;1;73;0
WireConnection;46;0;44;0
WireConnection;90;0;9;2
WireConnection;89;0;8;2
WireConnection;88;0;1;2
WireConnection;30;0;46;0
WireConnection;30;1;31;0
WireConnection;87;0;77;0
WireConnection;61;0;30;0
WireConnection;11;0;87;0
WireConnection;11;2;90;0
WireConnection;11;3;89;0
WireConnection;11;4;88;0
WireConnection;55;0;11;0
WireConnection;60;0;11;0
WireConnection;60;1;61;0
WireConnection;47;0;55;0
WireConnection;36;0;60;0
WireConnection;57;0;47;0
WireConnection;18;0;11;0
WireConnection;18;1;19;0
WireConnection;37;0;38;0
WireConnection;37;1;35;0
WireConnection;37;2;36;0
WireConnection;33;0;60;0
WireConnection;34;0;37;0
WireConnection;34;1;25;0
WireConnection;34;2;33;0
WireConnection;67;0;19;0
WireConnection;67;1;30;0
WireConnection;53;0;57;0
WireConnection;53;1;30;0
WireConnection;20;0;18;0
WireConnection;20;1;30;0
WireConnection;32;0;60;0
WireConnection;65;0;20;0
WireConnection;65;1;67;0
WireConnection;40;0;53;0
WireConnection;40;1;42;0
WireConnection;24;0;34;0
WireConnection;24;1;39;0
WireConnection;24;2;32;0
WireConnection;0;2;24;0
WireConnection;0;10;40;0
WireConnection;0;11;65;0
ASEEND*/
//CHKSM=A4264ACF075E32DE9298A9716FF3D9BFF8ED7E56