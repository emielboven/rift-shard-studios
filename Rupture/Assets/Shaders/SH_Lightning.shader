// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "New AmplifyShader"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_Width2("Width2", Range( 0 , 1)) = 0.2303568
		_Width("Width", Range( 0 , 1)) = 0.2303568
		_Edge2("Edge2", Range( 0 , 1)) = 0.001
		_Edge("Edge", Range( 0 , 1)) = 0.001
		_Texture0("Texture 0", 2D) = "white" {}
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float4 vertexColor : COLOR;
			float2 texcoord_0;
			float2 texcoord_1;
			float2 uv_texcoord;
		};

		uniform sampler2D _Texture0;
		uniform float _Width;
		uniform float _Edge;
		uniform float _Width2;
		uniform float _Edge2;
		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float4 appendResult89 = float4( v.color.r , v.color.g , 0 , 0 );
			o.texcoord_0.xy = v.texcoord.xy * float2( 0.5,0.5 ) + appendResult89.xy;
			o.texcoord_1.xy = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
		}

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 tex2DNode56 = tex2D( _Texture0, (abs( float2( 0,0 )+_Time[1] * float2(2,0.3 ))) );
			float temp_output_39_0 = lerp( tex2D( _Texture0, ( (abs( i.texcoord_0+_Time[1] * float2(-0.5,0.5 ))) + ( tex2DNode56.g * 0.2 ) ) ).r , tex2D( _Texture0, ( ( tex2D( _Texture0, (abs( i.texcoord_1+_Time[1] * float2(1,0 ))) ).b * 0.3 ) + i.texcoord_1 + ( tex2DNode56.r * 0.2 ) ) ).r , 0.5 );
			float temp_output_19_0 = ( _Width - _Edge );
			float temp_output_23_0 = ( _Width2 - _Edge2 );
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 tex2DNode80 = tex2D( _TextureSample0, uv_TextureSample0 );
			float temp_output_33_0 = clamp( ( ( clamp( ( ( distance( temp_output_39_0 , 0.5 ) - temp_output_19_0 ) / ( ( _Width + _Edge ) - temp_output_19_0 ) ) , 0.0 , 1.0 ) - clamp( ( ( distance( temp_output_39_0 , 0.5 ) - temp_output_23_0 ) / ( ( _Width2 + _Edge2 ) - temp_output_23_0 ) ) , 0.0 , 1.0 ) ) * ( tex2DNode80.r + tex2DNode80.r + tex2DNode80.r + tex2DNode80.r + tex2DNode80.r ) ) , 0.0 , 1.0 );
			o.Emission = ( i.vertexColor * ( temp_output_33_0 + temp_output_33_0 + temp_output_33_0 + temp_output_33_0 ) ).rgb;
			o.Alpha = ( temp_output_33_0 * i.vertexColor.a );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Unlit alpha:fade keepalpha fullforwardshadows vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 texcoords01 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.texcoords01 = float4( v.texcoord.xy, v.texcoord1.xy );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord.xy = IN.texcoords01.xy;
				float3 worldPos = IN.worldPos;
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutput o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutput, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=11001
7;29;1906;1004;-574.6917;-86.39893;1;True;False
Node;AmplifyShaderEditor.VertexColorNode;86;-3245.507,-1217.1;Float;False;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;35;-2894.304,-446.8003;Float;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.AppendNode;89;-3014.708,-1018.801;Float;False;FLOAT4;0;0;0;0;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT4
Node;AmplifyShaderEditor.PannerNode;36;-2481.102,-438.8001;Float;True;1;0;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.TexturePropertyNode;75;-3580.706,-606.6005;Float;True;Property;_Texture0;Texture 0;4;0;None;False;white;Auto;0;1;SAMPLER2D
Node;AmplifyShaderEditor.PannerNode;57;-3272.505,-75.50034;Float;True;2;0.3;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.SamplerNode;51;-2270.904,-113.9001;Float;True;Property;_TextureSample1;Texture Sample 1;2;0;Assets/Textures/T_FXNoise_Soft.tga;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;64;-2906.106,-740.3002;Float;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;0.5,0.5;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;56;-2891.704,-1.900355;Float;True;Property;_TextureSample3;Texture Sample 3;1;0;Assets/Textures/T_FXNoise_Soft.tga;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;59;-2461.504,481.3995;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.2;False;1;FLOAT
Node;AmplifyShaderEditor.PannerNode;60;-2483.705,-716.3004;Float;True;-0.5;0.5;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;-1891.705,97.2998;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.3;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;83;-2383.807,173.6992;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.2;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;53;-1676.705,264.6998;Float;True;3;3;0;FLOAT;0.0;False;1;FLOAT2;0;False;2;FLOAT;0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleAddOpNode;61;-1733.304,-561.1005;Float;True;2;2;0;FLOAT2;0.0;False;1;FLOAT;0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.SamplerNode;34;-1469.702,-23.80004;Float;True;Property;_TextureSample1;Texture Sample 1;2;0;Assets/Textures/T_FXNoise_Soft.tga;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;40;-1493.602,-682.3002;Float;True;Property;_TextureSample2;Texture Sample 2;1;0;Assets/Textures/T_FXNoise_Soft.tga;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;31;-1032.232,1115.244;Float;False;Property;_Edge2;Edge2;2;0;0.001;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;17;-1078.3,516.6996;Float;False;Property;_Edge;Edge;3;0;0.001;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.LerpOp;39;-1084.902,-334.1001;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.5;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;30;-1034.832,1007.444;Float;False;Property;_Width2;Width2;0;0;0.2303568;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;16;-1080.9,408.8997;Float;False;Property;_Width;Width;1;0;0.2303568;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.DistanceOpNode;11;-632.7001,191.0999;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.5;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;23;-663.8323,1023.644;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;24;-672.8323,1136.644;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;19;-709.8999,425.0998;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.DistanceOpNode;25;-586.6324,789.6443;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.5;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;18;-718.8999,538.0998;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;12;-304,238.5998;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;27;-257.9325,837.1443;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;20;-284.8999,539.0998;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;26;-238.8324,1137.644;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;28;-31.83249,966.3442;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;13;-77.90002,367.7997;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;80;549.0968,1060.799;Float;True;Property;_TextureSample0;Texture Sample 0;5;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;29;206.1677,969.1443;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;21;160.1001,370.5998;Float;True;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;84;982.8934,983.8997;Float;False;5;5;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;32;861.6996,704.0999;Float;True;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;79;1121.997,868.1992;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;33;1378.297,677.1998;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;87;1434.994,450.7998;Float;False;4;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;88;1666.293,570.3998;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;85;1636.993,310.3994;Float;False;2;2;0;COLOR;0.0;False;1;FLOAT;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1949.7,266.2;Float;False;True;2;Float;ASEMaterialInspector;0;Unlit;New AmplifyShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;Off;0;0;False;0;0;Transparent;0.5;True;True;0;False;Transparent;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;Relative;0;;-1;-1;-1;-1;0;14;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;OBJECT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;89;0;86;1
WireConnection;89;1;86;2
WireConnection;36;0;35;0
WireConnection;51;0;75;0
WireConnection;51;1;36;0
WireConnection;64;1;89;0
WireConnection;56;0;75;0
WireConnection;56;1;57;0
WireConnection;59;0;56;1
WireConnection;60;0;64;0
WireConnection;54;0;51;3
WireConnection;83;0;56;2
WireConnection;53;0;54;0
WireConnection;53;1;35;0
WireConnection;53;2;59;0
WireConnection;61;0;60;0
WireConnection;61;1;83;0
WireConnection;34;0;75;0
WireConnection;34;1;53;0
WireConnection;40;0;75;0
WireConnection;40;1;61;0
WireConnection;39;0;40;1
WireConnection;39;1;34;1
WireConnection;11;0;39;0
WireConnection;23;0;30;0
WireConnection;23;1;31;0
WireConnection;24;0;30;0
WireConnection;24;1;31;0
WireConnection;19;0;16;0
WireConnection;19;1;17;0
WireConnection;25;0;39;0
WireConnection;18;0;16;0
WireConnection;18;1;17;0
WireConnection;12;0;11;0
WireConnection;12;1;19;0
WireConnection;27;0;25;0
WireConnection;27;1;23;0
WireConnection;20;0;18;0
WireConnection;20;1;19;0
WireConnection;26;0;24;0
WireConnection;26;1;23;0
WireConnection;28;0;27;0
WireConnection;28;1;26;0
WireConnection;13;0;12;0
WireConnection;13;1;20;0
WireConnection;29;0;28;0
WireConnection;21;0;13;0
WireConnection;84;0;80;1
WireConnection;84;1;80;1
WireConnection;84;2;80;1
WireConnection;84;3;80;1
WireConnection;84;4;80;1
WireConnection;32;0;21;0
WireConnection;32;1;29;0
WireConnection;79;0;32;0
WireConnection;79;1;84;0
WireConnection;33;0;79;0
WireConnection;87;0;33;0
WireConnection;87;1;33;0
WireConnection;87;2;33;0
WireConnection;87;3;33;0
WireConnection;88;0;33;0
WireConnection;88;1;86;4
WireConnection;85;0;86;0
WireConnection;85;1;87;0
WireConnection;0;2;85;0
WireConnection;0;9;88;0
ASEEND*/
//CHKSM=CF369A09C16C625E0EF712365E2E69718BA22900