﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RechargingConsumables : MonoBehaviour
{

    public float tinderRechargeTime;
    public float bombRechargeTime;
    float tinderTimer;
    float pushTimer;
    float bombTimer;

    public int maxTinder;

    public int maxBombs;

    private PlayerStats stats;

	public HUDManager hud;

    // Use this for initialization
    void Start()
    {
		stats = GetComponent<PlayerStats>();
		stats.numberOfBombs = maxBombs;
		stats.numberOfTinder = maxTinder;
		hud.UpdateTinderCounter();
		hud.UpdateBombCounter();
		hud.bombCooldown.fillAmount = 1;
		hud.bombCooldown.fillAmount = 1;
    }

    // Update is called once per frame
    void Update()
    {

        if (stats.numberOfTinder < maxTinder)
        {
            tinderTimer += (1 /tinderRechargeTime)  * Time.deltaTime;
			hud.tinderCooldown.fillAmount = tinderTimer;
            if (tinderTimer >= 1)
            {
                stats.numberOfTinder += 1;
                tinderTimer = 0;
				hud.UpdateTinderCounter();
            }
        }
        if (stats.numberOfBombs < maxBombs)
        {
            bombTimer += (1 /bombRechargeTime) * Time.deltaTime;
			hud.bombCooldown.fillAmount = bombTimer;
            if (bombTimer >= 1)
            {
                stats.numberOfBombs += 1;
                bombTimer = 0;
				hud.UpdateBombCounter();
            }
        }

        if (stats.pushTimer < 1);
        {
            stats.pushTimer += (Time.deltaTime/stats.pushCooldown);
			hud.pushCooldown.fillAmount = stats.pushTimer;
            if (stats.pushTimer >= stats.pushCooldown)
            {
                stats.pushTimer = stats.pushCooldown;
            }
        }



    }
}
