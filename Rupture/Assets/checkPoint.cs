﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkPoint : MonoBehaviour
{

    private Vector3 dirPosition;
    private Quaternion dirRotation;

    private void Start()
    {
        dirPosition = transform.GetChild(0).position;
        dirRotation = transform.GetChild(0).rotation;
    }

    private void OnTriggerEnter(Collider other)
    {
		if (other.name == "PlayerMesh") {
            CheckPoints.Instance.checkPoint = dirPosition;
			CheckPoints.Instance.checkPointRotation = dirRotation;
			CheckPoints.Instance.camRotation = dirRotation;
			Destroy(this.gameObject);
		}
    }
}
